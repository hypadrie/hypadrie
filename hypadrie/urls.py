# -*- coding: utf-8 -*-
#
# Copyright (c) 2012 Rodolphe Quiedeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from django.conf.urls.defaults import *
from django.contrib.sitemaps import FlatPageSitemap, GenericSitemap
from django.views.generic.simple import redirect_to
from resa.models import Performance
from time import strftime, localtime
from tastypie.api import Api
from resa.api import ShowResource
from resa.api import EventResource
from resa.api import PerformanceResource
from resa.api import BookingResource
from resa.api_auth import ShowAuthResource
from resa.api_auth import EventAuthResource
from resa.api_auth import PerformanceAuthResource
from resa.api_auth import BookingAuthResource

# Implement API version 1
v1_api = Api(api_name='v1')
# Register Shows
v1_api.register(ShowResource())
v1_api.register(EventResource())
v1_api.register(PerformanceResource())
v1_api.register(BookingResource())
v1_api.register(ShowAuthResource())
v1_api.register(EventAuthResource())
v1_api.register(PerformanceAuthResource())
v1_api.register(BookingAuthResource())

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

info_dict = {
    'queryset': Performance.objects.filter(published='True',date__gt=strftime("%Y-%m-%d %H:%M:%S", localtime())).order_by('date'),
    'date_field': 'date',
}

sitemaps = {
    'flatpages': FlatPageSitemap,
    'blog': GenericSitemap(info_dict, priority=0.5),
}

#
urlpatterns = patterns(
    '',
    (r'^api/', include(v1_api.urls)),
    (r'^$', 'resa.views.performances'),
    (r'^accounts/profile/', 'resa.views.profile'),
    (r'^accounts/', include('registration.urls')),
    (r'^events/', include('events.urls')),
    (r'^logout$', 'resa.views.logout_view'),
    (r'^carte$', 'resa.views.themap'),
    (r'^about/$', 'resa.views.about'),
    (r'^help/?$', 'resa.views.helpview'),
    (r'^contact/$', 'resa.views.contact'),
    (r'^spectacles/', 'resa.views.spectacles'),
    (r'^accounts/api/', 'resa.views.api'),
    (r'^accounts/spectacles/', 'resa.views.spectacle_list'),
    (r'^accounts/spectacle/nouveau/', 'resa.views.spectacle_new'),
    (r'^accounts/spectacle/(?P<show_id>\d+)$', 'resa.views.spectacle_file'),
    (r'^accounts/spectacle/(?P<show_id>\d+)/resa/$', 'resa.views.spectacle_resa'),
    (r'^spectacle/(?P<show_id>\d+)$', 'resa.views.spectacle'),
    (r'^place/(?P<place_id>\d+)/?$', 'resa.views.place'),
    (r'^resalist/(?P<performance_id>\d+)/delete/(?P<unid1>\w+)-(?P<unid2>\w+)-(?P<unid3>\w+)-(?P<unid4>\w+)-(?P<unid5>\w+)$', 'resa.views.booking_delete'),
    (r'^resalist/(?P<performance_id>\d+)/(?P<flag>\d?)', 'resa.views.booking_list'),
    (r'^resalist/email/(?P<email>.*)/', 'resa.views.booking_user_list'),
    (r'^resa/', 'resa.views.bookings'),
    (r'^representations/', 'resa.views.performances'),
    (r'^representation/(?P<performance_id>\d+)$', 'resa.views.performance'),
    (r'^representation/(?P<performance_id>\d+)/resa/$', 'resa.views.performance_resa'),
    (r'^representation/(?P<performance_id>\d+)/annulation/(?P<unid1>\w+)-(?P<unid2>\w+)-(?P<unid3>\w+)-(?P<unid4>\w+)-(?P<unid5>\w+)$', 'resa.views.performance_resa_cancel'),
    (r'^representation/(?P<performance_id>\d+)/pdf/$', 'resa.views.list_resa_pdf'),
    (r'^representation/(?P<performance_id>\d+)/csv/$', 'resa.views.list_resa_csv'),
    (r'^p/(?P<performance_id>\d+)/c/(?P<unid1>\w+)-(?P<unid2>\w+)-(?P<unid3>\w+)-(?P<unid4>\w+)-(?P<unid5>\w+)$', 'resa.views.performance_resa_cancel'),
    # Uncomment the admin/doc line below to enable admin documentation:
    (r'^admin/doc/', include('django.contrib.admindocs.urls')),   
    # Uncomment the next line to enable the admin:
    (r'^admin/', include(admin.site.urls)),
    (r'^search/', include('haystack.urls')),
    # Festivals
    (r'^(?P<festival_slug>\w+)/$', 'resa.views.festival'),
    (r'^(?P<festival_slug>\w+)/representations/$', 'resa.views.festival_performances'),
    (r'^(?P<festival_slug>\w+)/representation/(?P<performance_id>\d+)$', 'resa.views.performance'),
    (r'^(?P<festival_slug>\w+)/representation/(?P<performance_id>\d+)/resa/$', 'resa.views.performance_resa'),
    (r'^(?P<festival_slug>\w+)/representation/(?P<performance_id>\d+)/annulation/(?P<unid1>\w+)-(?P<unid2>\w+)-(?P<unid3>\w+)-(?P<unid4>\w+)-(?P<unid5>\w+)$', 'resa.views.performance_resa_cancel'),
    (r'^(?P<festival_slug>\w+)/representation/(?P<performance_id>\d+)/pdf/$', 'resa.views.list_resa_pdf'),
    (r'^(?P<festival_slug>\w+)/representation/(?P<performance_id>\d+)/csv/$', 'resa.views.list_resa_csv'),
    (r'^(?P<festival_slug>\w+)/representation/(?P<festival_id>\d+)/festival/csv/$', 'resa.views.list_resa_festival_csv'),
    (r'^(?P<festival_slug>\w+)/logout$', 'resa.views.logout_view'),
    (r'^(?P<festival_slug>\w+)/carte$', 'resa.views.themap'),
    (r'^(?P<festival_slug>\w+)/about/$', 'resa.views.about'),
    (r'^(?P<festival_slug>\w+)/help/?$', 'resa.views.helpview'),
    (r'^(?P<festival_slug>\w+)/contact/$', 'resa.views.contact'),
    (r'^(?P<festival_slug>\w+)/spectacles/', 'resa.views.spectacles'),
    (r'^(?P<festival_slug>\w+)/spectacle/(?P<show_id>\d+)$', 'resa.views.spectacle'),
    (r'^(?P<festival_slug>\w+)/resalist/(?P<performance_id>\d+)/delete/(?P<unid1>\w+)-(?P<unid2>\w+)-(?P<unid3>\w+)-(?P<unid4>\w+)-(?P<unid5>\w+)$', 'resa.views.booking_delete'),
    (r'^(?P<festival_slug>\w+)/resalist/(?P<performance_id>\d+)/(?P<flag>\d?)', 'resa.views.booking_list'),
    (r'^(?P<festival_slug>\w+)/resa/', 'resa.views.bookings'),
    (r'^(?P<festival_slug>\w+)/p/(?P<performance_id>\d+)/c/(?P<unid1>\w+)-(?P<unid2>\w+)-(?P<unid3>\w+)-(?P<unid4>\w+)-(?P<unid5>\w+)$', 'resa.views.performance_resa_cancel'),
    #
    #
    (r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps})
    #(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap', HypadrieSitemap())
)
