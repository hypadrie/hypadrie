��    Y      �     �      �  �   �     \     v     �     �     �     �     �     �     �     �     �     �     �     	     	      	     (	     F	     K	     R	     c	     o	     �	  5   �	     �	     �	     �	     �	  	   �	     �	  
   �	     
     
     
     
     '
     5
  
   <
     G
     T
  
   Y
     d
     q
     z
     �
     �
     �
     �
     �
     �
     �
     �
     �
                  	   5     ?  
   H     S     \     k     �  	   �     �  	   �     �     �     �  O   �            <     M   T     �      �     �     �     �  	   �  "   �  .     {   G  (   �     �     �     �  K  �    K     j     �     �     �     �     �     �     �     �                .  	   6     @     `     y  +   �     �  	   �     �     �     �     �  P   �     M     Y     e     r     {     �     �     �     �     �     �     �     �  
   �     �                     ?     Q  	   b     l     p  &   �  %   �     �     �     �     �     �     �  ,        0     8     F     O     n  '   �  
   �     �     �     �     �  	   �  
   �  b   �     _     g  :   k  U   �     �  &        B     H     U     [  /   g  X   �  �   �  (   �     �     �     �     3       <   "      R   %   I             E              B   ,       5   +   8   (   7      X   	      #   =         W   @      &   >           -          O   
   '      F       0       /   ;   H   C   T                  G   D          ?              $   P      Y                     9   )      :           U             *                 Q   A              L   M      6   !       2       S   1   V                            J                           .   N      4   K        
	You've lost your confirmation's email and you can't remember
	how many places you've booked recently ? Fill the form below
	and we'll send you an email with all informations.
	 Account activation failed Account activation on Account successfully activated Adult places All Amount Book Booking Cancel Change password Child places Closed Company Company's URL Confirm delete Contact Copy selected representations Date Delete Delete confirmed Description Django site admin Email Email with password reset instructions has been sent. End date Event Events Festival Firstname Forgot password FreePlaces FullyBooked Help Hour Incoming events Last register Log in Logged out Lost booking Name New events New password New show No results found. Number : PDF Password changed Password reset failed Password reset successfully PerformanceSold Performances Place Places Price Publish Publish selected representations Published Register Registered Reset it Reset password Reset password at %(site_name)s Results RoomPlace Save Search In Send Show Shows Sorry but we can't find any booking with this number, maybe it does not exists  Submit Tel Thanks for your booking, please find here your book number : The email address was found, an email with your last booking was sent to you. The email sender is : This email address was not found Title ToBook Town Unpublish Unpublish selected representations You are now registered. Activation email sent. You'll receive an email with your booking number, remind to check your spam folder if you don't see the email in you inbox. Your booking is now confirmed, thank you email email : name Project-Id-Version: 0.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-12-04 13:46+0100
PO-Revision-Date: 2012-12-04 15:12+0100
Last-Translator: Rodolphe Quiédeville <rodolphe@quiedeville.org>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 
Vous avez perdu le mail de confirmation que nous vous avons envoyé et
vous ne vous rappellez plus combien de places vous aviez reservées ?
Indiquez votre adresse email dans le formulaire ci-dessous et nous
vous renverrons les informations de votre dernière réservation sur le site. Activation du compte échouée Activation du compte échouée Compte activé avec succés Places adulte Tous Montant Réservation Réservation Annuler Réinitialiser le mot de passe Places enfant Fermée Compagnie Adresse du site de la compagnie Confirmer la suppression Contact Copier les représentations sélectionnées Date Supprimer Réservation supprimée Description Django administration Email Un email avec le processus de modification du mot de passe vous a été envoyé. Date de fin Évènement Évènements Festival Prénom Mot de passe oublié Places libres Complet Aide Heure Prochains évènements Derniers inscrits Log in Logged out Réservation perdue Nom Nouveal évènement Réinitialiser le mot de passe Nouveau spectacle Aucun résultats Numéro : PDF Mot de passe modifié Erreur de modification du mot de passe Modification du mot de passe réussit Complet Représentations Lieu Lieu Prix Publié Publier les représentations sélectionnées Publié S'enregistrer Inscrits Réinitialiser le mot de passe Réinitialiser le mot de passe Mot de passe modifié sur %(site_name)s Résultats Lieu Enregistrer Chercher dans Envoyer Spectacle Spectacles Désolé mais nous ne trouvons pas de réservation pour ce numéro, peut-être n'existe-t'elle pas Envoyer Tel Merci pour votre réservation, celle-ci porte le numéro : L'adresse email est valide, nous vous avons envoyé un email avec votre
réservation. L'expéditeur des emails est : L'adresse email n'a pas été trouvée Titre Réservation Ville Non publié Dépublier les représentations sélectionnées Vous êtes maintenant enregistré, un email d'activation de compte vous
a été envoyé. Vous allez reçevoir un email contenant les détails de votre réservation, pensez à vérifier votre dossier SPAM si vous ne voyez pas le mail dans votre boîte de réception. Votre réservation est confirmée, merci email email nom 