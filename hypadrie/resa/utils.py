# -*- coding: utf-8 -*-
#
# Copyright (c) 2012 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Make PDF file listing all bookings on a performance
"""
from reportlab.pdfgen import canvas
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from time import time
from datetime import datetime
from django.template.defaultfilters import date as django_date
from django.template.loader import render_to_string
from django.contrib.sites.models import Site


class Hypapdf():
    """
    Main pdf generator class
    """
    def __init__(self, performance, date, town, datas):
        self.datas = datas
        self.perf = performance
        self.date = date
        self.town = town
        self.pdf = None
        #  Columns width order from left to right
        self.colsw = (20, 336, 16, 16, 16, 16, 16, 44, 40)

    def lines(self, nline, wcol, i):
        """
        Draw a line with data in pdf file

        """
        top = 55
        left = 16
        vstep = 18
        data = self.datas[i - 1]
        string_top_space = 3
        self.pdf.drawString(left + 4,
                            top + string_top_space + (i * vstep),
                            nline)
        self.pdf.drawString(left + 4 + wcol[0],
                            top + string_top_space + (i * vstep),
                            "%s %s" % (data[1], data[0]))

        self.pdf.drawString(left + 4 + wcol[0] + wcol[1],
                            top + string_top_space + (i * vstep),
                            data[2])
        self.pdf.drawString(left + 4 + sum(wcol[0:3]),
                            top + string_top_space + (i * vstep),
                            data[3])

        self.pdf.drawString(left + 4 + sum(wcol[0:4]),
                            top + string_top_space + (i * vstep),
                            data[4])

        self.pdf.drawString(left + 4 + sum(wcol[0:5]),
                            top + string_top_space + (i * vstep),
                            data[5])

        self.pdf.drawString(left + 4 + sum(wcol[0:6]),
                            top + string_top_space + (i * vstep),
                            data[6])
        self.pdf.drawString(left + 4 + sum(wcol[0:7]),
                            top + string_top_space + (i * vstep),
                            data[7])

        for inc in range(len(wcol) - 1):
            self.drawfield(inc, left, top - 10 + (i * vstep), vstep)

    def drawfield(self, field, left, top, vstep):
        """
        Draw the rectangle for field X and line i
        """
        j = 0
        width = left
        while j <= field:
            width = width + self.colsw[j]
            j = j + 1

        self.pdf.rect(width, top, self.colsw[field + 1], vstep)

    def list_resa_pdf(self, fpath):
        """
        Public function called in model
        """
        fontname = 'DejaVu'
        ttf = '/usr/share/fonts/truetype/ttf-dejavu/DejaVuSans.ttf'
        pdfmetrics.registerFont(TTFont(fontname, ttf))

        #  Create the PDF object, using the response object as its "file."
        self.pdf = canvas.Canvas(fpath, bottomup=0)
        self.pdf.setFont(fontname, 10)
        self.pdf.setLineWidth(.3)

        top = 15
        left = 12
        vstep = 15
        cols = self.colsw
        # Draw things on the PDF. Here's where the PDF generation happens.
        # See the ReportLab documentation for the full list of functionality.
        total = len(self.datas)
        j = 1
        pages = round(total // 42)
        if (total % 42) > 0:
            pages = pages + 1
        page = 0

        while j <= total:
            i = 1
            page = page + 1
            datei = django_date(datetime.fromtimestamp(time()), "d F Y")
            title = u'Imprimé le %s' % (datei)
            self.pdf.drawString(10, top, title)

            self.pdf.drawString(
                10,
                top + vstep,
                u'Réservations pour : %s du %s' % (self.perf, self.date))
            self.pdf.drawString(
                10,
                top + (2 * vstep),
                u'Salle : %s' % (self.town))
            self.pdf.drawString(510, top, "Page %d/%d" % (page, pages))
            self.pdf.setFont(fontname, 8)
            self.pdf.drawString(left + 4 + cols[0], 57, u'Nom Prénom')
            self.drws(left, cols, 1, 57, u'TA')
            self.drws(left, cols, 2, 57, u'TB')
            self.drws(left, cols, 3, 57, u'TC')
            self.drws(left, cols, 4, 57, u'TD')
            self.drws(left, cols, 5, 57, u'TE')
            self.pdf.drawString(left + 4 + sum(cols[0:7]), 57, u'A régler')
            self.pdf.setFont(fontname, 10)
            while i <= 42 and j <= total:
                self.lines(str(j), cols, i)
                i = i + 1
                j = j + 1
            self.pdf.showPage()
        # Close the PDF object cleanly, and we're done.
        self.pdf.save()

    def drws(self, left, cols, position, increment, string):
        dec = 0
        i = 0
        while (i <= position):
            dec = dec + cols[i]
            i = i + 1

        self.pdf.drawString(left + dec + 2, increment, string)


class Messages():
    """
    The Messages class is used to send email in models
    """
    def __init__(self):
        """
        The init
        """
        self.booking = None
        self.server_name = None
        self.performance = None
        self.current_site = Site.objects.get_current()

    def mail_public(self, template='messages/fr_FR/booking_email_public.txt'):
        """
        Message send to whom booked

        """
        msg = render_to_string(template,
                               {'site': self.current_site,
                                'booking': self.booking})

        return msg

    def mail_cancel_public(self):
        """
        Message send to whom booked

        """
        msg = render_to_string('messages/fr_FR/cancel_email_public.txt',
                               {'site': self.current_site,
                                'booking': self.booking})

        return msg

    def mail_admin(self):
        """
        Message to performance's contact
        """

        current_site = Site.objects.get_current()
        msg = render_to_string('messages/fr_FR/booking_email_orga.txt',
                               {'site': current_site,
                                'booking': self.booking})

        return msg


def plural(qty, string):
    """
    Return the plural form if qty > 1
    """
    if qty > 1:
        string = u"%ss" % string
    return string
