# -*- coding: utf-8 -*-  pylint: disable-msg=R0801
#
"""
Admin part for Performance object

Actions availables in admin part

 - copy

 - publish

 - unpublish

"""
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from resa.models import Performance


class PerformanceAdmin(admin.ModelAdmin):  # pylint: disable-msg=R0904
    """
    Performance admin parts actions
    """
    def copy_published(self, request, queryset):
        """
        Limit on the 10 first objects for security reason
        """
        for obj in queryset[:10]:
            Performance(show=obj.show,
                        place=obj.place,
                        date=obj.date,
                        published=False,
                        nb_places_available=obj.nb_places_available,
                        price_a=obj.price_a,
                        price_b=obj.price_b,
                        price_c=obj.price_c,
                        price_d=obj.price_d,
                        price_e=obj.price_e,
                        open_booking=obj.open_booking,
                        information=obj.information,
                        festival=obj.festival,
                        bkng_send_email=obj.bkng_send_email,
                        bkng_contact_email=obj.bkng_contact_email).save()

    def make_published(self, request, queryset):
        """
        Publish the selected objects
        """
        queryset.update(published=True)

    def make_unpublished(self, request, queryset):
        """
        Unpublish the selected objects
        """
        queryset.update(published=False)

    def format_date(self, obj):
        """
        Format the datetime in long format
        """
        return obj.date.strftime('%A %d %b %Y %H:%M')

    make_published.short_description = _('Publish selected representations')
    unpub = _('Unpublish selected representations')
    make_unpublished.short_description = unpub
    copy_published.short_description = _('Copy selected representations')

    verbose_name = 'Représentations'
    list_display = ('date',
                    'show',
                    'place',
                    'nb_places_available',
                    'published',
                    'open_booking')
    list_filter = ['festival', 'date', 'published']
    date_hierarchy = 'date'
    ordering = ['-date']
    actions = [copy_published, make_published, make_unpublished]

    fieldsets = [
        ('Spectacle', {'fields': ['date', 'show', 'place', 'published']}),
        ('Réservations', {'fields': ['open_booking',
                                     'nb_places_available',
                                     'open_booking_from',
                                     'open_booking_until']}),
        ('Tarifs', {'fields': ['price_a', 'label_a',
                               'price_b', 'label_b',
                               'price_c', 'label_c',
                               'price_d', 'label_d',
                               'price_e', 'label_e',
                               'information', 'festival']}),
        ('Notifications', {'fields': ['bkng_send_email',
                                      'bkng_contact_email']}),
    ]

    format_date.short_description = 'Date'
