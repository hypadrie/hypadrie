# -*- coding: utf-8 -*-  pylint: disable-msg=R0801
#
# Copyright (c) 2012 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
The django views
"""
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, EmptyPage, InvalidPage
from django.contrib.sites.models import get_current_site
from django.shortcuts import get_object_or_404, redirect
from django.http import HttpResponse
from django.template import RequestContext, loader
from django.conf import settings
from django.shortcuts import render
from resa.models import Booking, Festival, Performance, Place, Show
from resa.utils import Hypapdf
from resa.forms import BookingForm, HelpForm
from resa.forms import ShowForm
from time import strftime, localtime
from uuid import uuid4
from cStringIO import StringIO
import csv
from tastypie.models import ApiKey

def themap(request):
    """
    The map
    """
    return render(request,
                  'map.html',
                  {'onglet': 'map'})


def about(request, festival_slug=None):
    """
    About static view

    The classical ``about`` view with information about hypadrie authors,
    source code and whatever.

    **Required arguments**

    None.

    **Optional arguments**

    None.

    """
    return render(request,
                  'about.html',
                  {'onglet': 'about',
                   'hypadrie_version': settings.HYPADRIE_VERSION,
                   'prefix_url': urlprefix(festival_slug)
                   }
                  )


def helpview(request, festival_slug=None):
    """
    Help view

    An help view that contains some off basic action to help our clients
    **Required arguments**

    None.

    **Optional arguments**

    None.

    """
    if request.method == 'POST':
        form = HelpForm(request.POST)
        if form.is_valid():
            try:
                data = form.cleaned_data['email']
                last_booking = Booking.objects.filter(
                    email=data).order_by('-id')[0:1].get()
                last_booking.mail_public(request.META['SERVER_NAME'],
                                         settings.EMAIL_FROM)
                result = 'found'
            except ObjectDoesNotExist:
                result = 'notfound'

            return render(request,
                          'help.html',
                          {'onglet': 'help',
                           'form': form,
                           'helpform': result,
                           'prefix_url': urlprefix(festival_slug)})
        else:
            return render(request,
                          'help.html',
                          {'onglet': 'help',
                           'form': form,
                           'prefix_url': urlprefix(festival_slug),
                           'helpform': 'error'})
    else:
        form = HelpForm()
        return render(request,
                      'help.html',
                      {'onglet': 'help', 'form': form,
                       'helpform': 'new',
                       'prefix_url': urlprefix(festival_slug)})


def contact(request, festival_slug=None):
    """
    Contact : static view

    Sometimes people wants to ``contact`` webmaster, the want to speak
    with them, it's an strange habit but we have to suits people
    needs.

    **Required arguments**

    None.

    **Optional arguments**

    None.

    """
    return render(request,
                  'contact.html',
                  {'onglet': 'contact',
                   'prefix_url': urlprefix(festival_slug)
                   })


@login_required
def api(request):
    """
    Profile
    """
    current_site = get_current_site(request)
    apikey = get_object_or_404(ApiKey, user=request.user)

    return render(request, 'api.html', {'apikey': apikey,
                                        'site': current_site})

@login_required
def profile(request):
    """
    Profile
    """
    current_site = get_current_site(request)
    festivals = Festival.objects.filter(owner__in=[request.user.id],
                                        site__in=[current_site.id]).order_by('-id')[:7]
    shows = Show.objects.filter(owner__in=[request.user.id]).order_by('-id')[:7]

    return render(request, 'profile.html', {'festivals': festivals,
                                            'shows': shows})


def place(request, place_id):
    """
    Place
    """
    pobj = get_object_or_404(Place, pk=place_id)
    return render(request, 'place.html', {'place': pobj,
                                          'lonstr': str(pobj.lon),
                                          'latstr': str(pobj.lat)}
                  )


def festival(request, festival_slug):
    """
    Single festival

    """
    fest = get_object_or_404(Festival, url=festival_slug)
    perf_list = Performance.objects.filter(published='True',
                                           festival=fest.pk)
    return render(request,
                  'festival.html',
                  {'onglet': 'festival',
                   'festival': fest,
                   'perf_list': perf_list,
                   'title': fest.title,
                   'prefix_url': urlprefix(festival_slug)}
                  )


def spectacles(request, festival_slug=None):
    """
    Listing all spectacle

    """
    current_site = get_current_site(request)
    latest_show_list = Show.objects.filter(published='True',
                                           site__in=[current_site.id])

    return render(request,
                  'shows.html',
                  {'onglet': 'shows',
                   'latest_show_list': latest_show_list,
                   'prefix_url': urlprefix(festival_slug)
                   }
                  )


def spectacle(request, show_id, festival_slug=None):
    """
    The show view
    """
    current_site = get_current_site(request)
    datenow = strftime("%Y-%m-%d %H:%M:%S", localtime())
    show = get_object_or_404(Show, pk=show_id,site__in=[current_site.id])

    perf_list = Performance.objects.filter(published='True',
                                           date__gt=datenow,
                                           show=show_id
                                           ).order_by('date')[:10]
    prev_list = Performance.objects.filter(published='True',
                                           date__lt=datenow,
                                           show=show_id
                                           ).order_by('date')[:3]

    return render(request,
                  'show.html',
                  {'show': show,
                   'next_perf_list': perf_list,
                   'prev_perf_list': prev_list,
                   'prefix_url': urlprefix(festival_slug)
                   },
                  )


@login_required
def spectacle_resa(request, show_id, festival_slug=None):
    """
    View show's bookings
    """
    show = get_object_or_404(Show, pk=show_id, owner__pk=request.user.id)

    blist = Booking.objects.filter(
        performance__show__pk=show_id).order_by('name', 'firstname')

    paginator = Paginator(blist, 10)

    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1

    try:
        contacts = paginator.page(page)
    except (EmptyPage, InvalidPage):
        contacts = paginator.page(paginator.num_pages)

    return render(request,
                  'show/bookings.html',
                  {'show': show,

                   'latest_perf_list': contacts,
                   'prefix_url': urlprefix(festival_slug)})


@login_required
def spectacle_file(request, show_id, festival_slug=None):
    """
    View a show in privileged part
    """
    show = get_object_or_404(Show, pk=show_id, owner__pk=request.user.id)

    return render(request,
                  'show/view.html',
                  {'show': show, }
                  )


@login_required
def spectacle_list(request, festival_slug=None):
    """
    View all shows owned by the logged in account
    """
    show_list = Show.objects.filter(
        owner__in=[request.user.id]).order_by('-pk')

    paginator = Paginator(show_list, 10)

    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1

    try:
        shows = paginator.page(page)
    except (EmptyPage, InvalidPage):
        shows = paginator.page(paginator.num_pages)

    return render(request,
                  'show/list.html',
                  {'shows': shows,
                   'prefix_url': urlprefix(festival_slug)})


@login_required
def spectacle_new(request, festival_slug=None):
    """
    Create a new show
    """
    if request.method == 'POST':
        form = ShowForm(request.POST)
        if form.is_valid():
            shw = Show(title=form.cleaned_data['title'],
                       company=form.cleaned_data['company'],
                       description=form.cleaned_data['description'],
                       contact_name=form.cleaned_data['contact_name'],
                       contact_tel=form.cleaned_data['contact_tel'],
                       contact_email=form.cleaned_data['contact_email'],
                       company_url=form.cleaned_data['company_url'],
                       show_url=form.cleaned_data['show_url'],
                       published=form.cleaned_data['published'],
                       created_by=request.user)
            shw.save()
            shw.owner.add(request.user.id)
            shw.site.add(settings.SITE_ID)

            return render(request,
                          'show/view.html',
                          {'show': shw, }
                          )
        else:
            return render(request,
                          'show/new.html',
                          {'form': form, })
    else:
        form = ShowForm()
        return render(request,
                      'show/new.html',
                      {'form': form, }
                      )


def festival_performances(request, festival_slug):
    """
    Performances for a festival
    """
    fest = get_object_or_404(Festival, url=festival_slug)
    perf_list = Performance.objects.filter(
        published='True',
        festival=fest.id,
        date__gt=strftime("%Y-%m-%d %H:%M:%S", localtime())).order_by('date')
    return render(request,
                  'performances.html',
                  {'onglet': 'performances',
                   'today': strftime("%Y%m%d", localtime()),
                   'next_perf_list': perf_list,
                   'prefix_url': urlprefix(festival_slug)}
                  )


def performances(request, festival_slug=None):
    """
    Display performances published

    """
    current_site = get_current_site(request)
    dategt = strftime("%Y-%m-%d %H:%M:%S", localtime())
    prf_list = Performance.objects.filter(
        published='True',
        show__site__in=[current_site.id],
        date__gt=dategt).order_by('date')
    return render(request,
                  'performances.html',
                  {'onglet': 'performances',
                   'today': strftime("%Y%m%d", localtime()),
                   'next_perf_list': prf_list,
                   'prefix_url': urlprefix(festival_slug)}
                  )


def performance(request, performance_id, festival_slug=None):
    """
    Performance
    """
    perf = get_object_or_404(Performance, pk=performance_id)
    i = 0
    place_list = []
    maxplaces = min(settings.MAX_BOOKABLE_PLACE, perf.nb_places_free)
    while (i <= maxplaces):
        place_list.append((i, i))
        i = i + 1
    form = BookingForm()
    form.fields['nb_place_a'].choices = place_list
    form.fields['nb_place_b'].choices = place_list
    form.fields['nb_place_c'].choices = place_list
    form.fields['nb_place_d'].choices = place_list
    form.fields['nb_place_e'].choices = place_list
    return render(request,
                  'performance.html',
                  {'perf': perf,
                   'form': form,
                   'nb_place_list': place_list,
                   'prefix_url': urlprefix(festival_slug)})


def performance_resa(request, performance_id, festival_slug=None):
    """
    Book a performance
    """

    perf = get_object_or_404(Performance, pk=performance_id)
    place_list = []
    i = 0
    maxplaces = min(settings.MAX_BOOKABLE_PLACE, perf.nb_places_free)
    maxbookable = min(2 * int(settings.MAX_BOOKABLE_PLACE),
                      perf.nb_places_free)
    while (i <= maxplaces):
        place_list.append((i, i))
        i = i + 1
    if request.method == 'POST':
        #  If the form has been submitted...
        form = BookingForm(request.POST, maxplaces=maxbookable)
        form.fields['nb_place_a'].choices = place_list
        form.fields['nb_place_b'].choices = place_list
        form.fields['nb_place_c'].choices = place_list
        form.fields['nb_place_d'].choices = place_list
        form.fields['nb_place_e'].choices = place_list
        if form.is_valid():
            #  All validation rules pass
            bkn = Booking(performance_id=performance_id,
                          firstname=form.cleaned_data['firstname'],
                          name=form.cleaned_data['name'],
                          places_a=form.cleaned_data['nb_place_a'],
                          places_b=form.cleaned_data['nb_place_b'],
                          places_c=form.cleaned_data['nb_place_c'],
                          places_d=form.cleaned_data['nb_place_d'],
                          places_e=form.cleaned_data['nb_place_e'],
                          email=form.cleaned_data['email'],
                          uuid=str(uuid4()))
            bkn.save()
            bkn.mail_public(request.META['SERVER_NAME'], settings.EMAIL_FROM)
            if perf.bkng_send_email and len(perf.bkng_contact_email) > 0:
                bkn.mail_admin(request.META['SERVER_NAME'],
                               settings.EMAIL_FROM,
                               perf.bkng_contact_email)
            return render(request,
                          'performance_resa.html',
                          {'perf': perf,
                           'booking': bkn,
                           'form': form,
                           'email_from': settings.EMAIL_FROM,
                           'prefix_url': urlprefix(festival_slug)})
        else:
            return render(request,
                          'performance.html',
                          {'perf': perf,
                           'form': form,
                           'prefix_url': urlprefix(festival_slug)})
    else:
        #  the form was requested by GET
        form = BookingForm()
        form.fields['nb_place_a'].choices = place_list
        form.fields['nb_place_b'].choices = place_list
        form.fields['nb_place_c'].choices = place_list
        form.fields['nb_place_d'].choices = place_list
        form.fields['nb_place_e'].choices = place_list
        return render(request, 'performance_resa.html', {'perf': perf,
                                                         'form': form,
                                                         'prefix_url': urlprefix(festival_slug)})


def performance_resa_cancel(request, performance_id, unid1, unid2, unid3,
                            unid4, unid5, festival_slug=None):
    """
    Cancel a booking
    """
    uuid = "%s-%s-%s-%s-%s" % (unid1, unid2, unid3, unid4, unid5)
    perf = get_object_or_404(Performance, pk=performance_id)
    tpl = loader.get_template('performance_resa_cancel.html')
    try:
        #Delete a Booking object
        bkg = Booking.objects.get(uuid=uuid)
        bkg.mail_cancel_public(request.META['SERVER_NAME'],
                               settings.EMAIL_FROM)
        bkg.delete()
        ctx = RequestContext(request, {'perf': perf,
                                       'uuid': uuid,
                                       'result': 'success',
                                       'prefix_url': urlprefix(festival_slug)
                                       })
    except ObjectDoesNotExist:
        ctx = RequestContext(request, {'perf': perf,
                                       'uuid': uuid,
                                       'result': 'notexist',
                                       'prefix_url': urlprefix(festival_slug)
                                       })
    return HttpResponse(tpl.render(ctx))


@login_required
def list_resa_pdf(request, performance_id, festival_slug=None):
    """
    Printing bookings on pdf
    """
    perf = get_object_or_404(Performance, pk=performance_id)

    datas = []

    qrstr = '''
SELECT id, firstname, name, places_a, places_b, places_c, places_d, places_e, price_total
FROM resa_booking
WHERE performance_id = %s
ORDER BY name ASC, firstname ASC
'''
    query = qrstr % performance_id
    rawdatas = Booking.objects.raw(query)
    for prf in rawdatas:
        datas.append((prf.firstname,
                      prf.name,
                      str(prf.places_a),
                      str(prf.places_b),
                      str(prf.places_c),
                      str(prf.places_d),
                      str(prf.places_e),
                      str(prf.price_total)))

    hypa = Hypapdf(perf.show.title,
                   perf.date,
                   perf.place.title,
                   datas)

    fname = 'resa-' + strftime("%d%m%y-%H%M", localtime())

    # Create the HttpResponse object with the appropriate PDF headers.
    response = HttpResponse(mimetype='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=%s.pdf' % (fname)

    bufpdf = StringIO()
    hypa.list_resa_pdf(bufpdf)
    pdf = bufpdf.getvalue()
    bufpdf.close()
    response.write(pdf)
    return response


@login_required
def list_resa_csv(request, performance_id, festival_slug=None):
    """
    Export bookings in CSV
    """
    perf = get_object_or_404(Performance, pk=performance_id)

    opts = Booking.objects.filter(performance__pk=perf.id).order_by('name')

    response = HttpResponse(mimetype='text/csv; charset=utf-8')
    response['Content-Disposition'] = 'attachment; filename=%s.csv' % (perf.show.title)
    writer = csv.writer(response)
    field_names = ['Nom', 'Prénom', 'email', 'Montant', 'places_a', 'places_b', 'places_c', 'places_d', 'places_e', 'Date résa']
    # Write a first row with header information
    writer.writerow(field_names)
    # Write data rows
    for obj in opts:
        writer.writerow([obj.name, obj.firstname, obj.email,
                         unicode(obj.price_total).replace('.', ','), obj.places_a, obj.places_b, obj.places_c, obj.places_d, obj.places_e,
                         obj.booking_date])
    return response


@login_required
def list_resa_festival_csv(request, festival_id, festival_slug=None):
    """
    Export bookings in CSV
    """
    fest = get_object_or_404(Festival, pk=festival_id)

    opts = Booking.objects.filter(performance__festival__pk=fest.id).order_by('name')

    response = HttpResponse(mimetype='text/csv; charset=utf-8')
    response['Content-Disposition'] = 'attachment; filename=%s.csv' % (fest.title)
    writer = csv.writer(response)
    field_names = ['Spectacle',  'Nom', 'Prénom', 'email', 'Montant', 'places_a', 'places_b', 'places_c', 'places_d', 'places_e', 'Date résa']
    # Write a first row with header information
    writer.writerow(field_names)
    # Write data rows
    for obj in opts:
        writer.writerow([obj.performance.show.title.encode('utf-8'),
                         obj.name.encode('utf-8'),
                         obj.firstname.encode('utf-8'),
                         obj.email.encode('utf-8'),
                         unicode(obj.price_total).replace('.', ','), obj.places_a, obj.places_b, obj.places_c, obj.places_d, obj.places_e, obj.booking_date])
    return response


@login_required
def bookings(request, festival_slug=None):
    """
    View limited to auth only
    """

    if festival_slug is not None:
        fest = get_object_or_404(Festival, url=festival_slug)
        perf_list = Performance.objects.filter(published='True',
                                               festival__pk=fest.id).order_by('date')
    else:
        fest = None
        perf_list = Performance.objects.filter(published='True').order_by('date')
    return render(request,
                  'booking_summary.html',
                  {'onglet': 'bookings',
                   'latest_perf_list': perf_list,
                   'festival': fest,
                   'prefix_url': urlprefix(festival_slug)}
                  )


@login_required
def booking_list(request, performance_id, flag, festival_slug=None):
    """
    View limited to auth only

    - perfomance_id : the performance id

    - flag : delete confirmed flag (used in templates)
    """
    prf_id = performance_id
    perf = Performance.objects.filter(pk=prf_id)[0]
    blist = Booking.objects.filter(
        performance=prf_id).order_by('name', 'firstname')

    paginator = Paginator(blist, 10)

    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1

    try:
        contacts = paginator.page(page)
    except (EmptyPage, InvalidPage):
        contacts = paginator.page(paginator.num_pages)

    return render(request,
                  'bookings.html',
                  {'onglet': 'bookings',
                   'perf': perf,
                   'dc': flag,
                   'bookings': contacts,
                   'prefix_url': urlprefix(festival_slug)}
                  )


@login_required
def booking_user_list(request, email, festival_slug=None):
    """
    View limited to auth only

    - perfomance_id : the performance id

    - flag : delete confirmed flag (used in templates)
    """
    blist = Booking.objects.filter(
        email=email).order_by('name', 'firstname')

    paginator = Paginator(blist, 10)

    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1

    try:
        contacts = paginator.page(page)
    except (EmptyPage, InvalidPage):
        contacts = paginator.page(paginator.num_pages)

    return render(request,
                  'bookings_users.html',
                  {'onglet': 'bookings',
                   'bookings': contacts,
                   'email_qry': email,
                   'nb_bookings': len(contacts),
                   'prefix_url': urlprefix(festival_slug)}
                  )


@login_required
def booking_delete(request, performance_id, unid1, unid2, unid3, unid4, unid5, festival_slug=None):
    """
    View limited to auth only
    """
    uuid = "%s-%s-%s-%s-%s" % (unid1, unid2, unid3, unid4, unid5)
    booking = Booking.objects.filter(uuid=uuid)
    booking.delete()

    return redirect('/resalist/%s/1' % performance_id)


def logout_view(request):
    """
    Logout a previously logged in user
    """
    logout(request)
    return redirect('/')


def urlprefix(festival_slug):
    if (festival_slug != None):
        prefixurl = "/%s" % (festival_slug)
    else:
        prefixurl = ""
    return prefixurl
