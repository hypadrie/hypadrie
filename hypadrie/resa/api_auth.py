# -*- coding: utf-8 -*-  pylint: disable-msg=R0801
#
# Copyright (c) 2012 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
API for hypadrie
"""

from tastypie.resources import ModelResource
from tastypie import fields
from resa.models import Booking
from resa.models import Performance
from resa.models import Show
from tastypie.throttle import CacheThrottle
from tastypie.authentication import ApiKeyAuthentication
from tastypie.authorization import DjangoAuthorization


class ShowAuthResource(ModelResource):  # pylint: disable-msg=R0904
    class Meta:
        # auth with api key
        authentication = ApiKeyAuthentication()
        authorization = DjangoAuthorization()
        #
        list_allowed_methods = ['get']
        queryset = Show.objects.filter(
            events=False).order_by('-pk')
        resource_name = 'auth/show'
        excludes = ['events']
        # limit to 100 req per hour
        throttle = CacheThrottle(throttle_at=100, timeframe=3600)

    def apply_authorization_limits(self, request, object_list):
        return object_list.filter(owner__in=[request.user.id])


class PerformanceAuthResource(ModelResource):  # pylint: disable-msg=R0904
    show = fields.ForeignKey(ShowAuthResource, 'show')

    class Meta:
        # auth with api key
        authentication = ApiKeyAuthentication()
        authorization = DjangoAuthorization()
        #
        queryset = Performance.objects.filter().order_by('-pk')
        resource_name = 'auth/performance'
        # limit to 100 req per hour
        throttle = CacheThrottle(throttle_at=100, timeframe=3600)

    def apply_authorization_limits(self, request, object_list):
        return object_list.filter(show__owner__in=[request.user.id])


class EventAuthResource(ModelResource):  # pylint: disable-msg=R0904
    # An event is a special performance/show
    show = fields.ForeignKey(ShowAuthResource, 'show')

    class Meta:
        # auth with api key
        authentication = ApiKeyAuthentication()
        authorization = DjangoAuthorization()
        #
        queryset = Performance.objects.filter().order_by('-pk')
        resource_name = 'auth/event'
        excludes = ['published']
        # limit to 100 req per hour
        throttle = CacheThrottle(throttle_at=100, timeframe=3600)

    def apply_authorization_limits(self, request, object_list):
        return object_list.filter(show__owner__in=[request.user.id])


class BookingAuthResource(ModelResource):  # pylint: disable-msg=R0904
    performance = fields.ForeignKey(PerformanceAuthResource, 'performance')

    class Meta:
        # auth with api key
        authentication = ApiKeyAuthentication()
        authorization = DjangoAuthorization()
        #
        queryset = Booking.objects.all()
        resource_name = 'auth/booking'
        # limit to 100 req per hour
        throttle = CacheThrottle(throttle_at=100, timeframe=3600)

    def apply_authorization_limits(self, request, object_list):
        return object_list.filter(
            performance__show__owner__in=[request.user.id])
