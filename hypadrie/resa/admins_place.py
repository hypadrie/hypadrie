# -*- coding: utf-8 -*-
# Custom admin for places
from django.contrib import admin


class PlaceAdmin(admin.ModelAdmin):
    description = u'Salles de concerts'
    fieldsets = [(None,  {'fields': ['title',
                                     'address',
                                     'town']
                          }),
                 ]
    list_display = ('title', 'town')
    list_filter = ('town',)
    search_fields = ['title', 'town']
