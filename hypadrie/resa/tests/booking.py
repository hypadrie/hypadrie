# -*- coding: utf-8 -*-
#
# Copyright (c) 2012 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Unit tests for model bookings - Hypadrie

"""
from django.test import TestCase
from django.test import Client
from django.core import mail
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from datetime import datetime
from hypadrie.resa.models import Show
from hypadrie.resa.models import Place
from hypadrie.resa.models import Booking
from hypadrie.resa.models import Performance


class BookingTests(TestCase):  # pylint: disable-msg=R0904
    """
    Tests for the model-oriented functionality of hypadrie-resa,
    including ``Booking``.
    """
    def setUp(self):
        """
        Init
        """
        self.site = Site.objects.create(domain='sample_site')
        self.user = User.objects.create(username='admin_bookings')

        place_located = Place.objects.create(title='foo_place_l')

        sample_show = Show.objects.create(title='foo_show',
                                          company='foobar',
                                          created_by=self.user)

        self.sample_perf_empty = Performance.objects.create(
            show=sample_show,
            place=place_located,
            date=datetime(2012, 6, 7, 20, 10, 2),
            nb_places_available=120,
            price_a=1,
            price_b=2,
            price_c=3,
            price_d=4,
            price_e=5)

        self.smp_perf_non_empty = Performance.objects.create(
            show=sample_show,
            place=place_located,
            date='2012-06-06 23:20:20',
            nb_places_available=120,
            price_a=1,
            price_b=2,
            price_c=3,
            price_d=4,
            price_e=5)

        self.sample_booking_a = Booking.objects.create(
            performance=self.smp_perf_non_empty,
            firstname='foobar',
            places_a=1,
            places_b=2,
            places_c=3,
            places_d=4,
            places_e=5)

        self.smp_perf_full = Performance.objects.create(
            show=sample_show,
            place=place_located,
            date='2012-06-06 23:20:20',
            nb_places_available=18,
            price_a=1,
            price_b=2,
            price_c=3,
            price_d=4,
            price_e=5)

        self.sample_booking_b = Booking.objects.create(
            performance=self.smp_perf_full,
            firstname='lorem 5 places',
            name='ipsum',
            places_a=0,
            places_b=2,
            places_c=3,
            places_d=0,
            places_e=0)

        Booking.objects.create(
            performance=self.smp_perf_full,
            firstname='foobar 13 places',
            places_a=10,
            places_b=0,
            places_c=1,
            places_d=1,
            places_e=1)

    def test_booking(self):
        """
        Test a performance without any free places

        """
        self.assertEqual(str(self.sample_booking_a), ' foobar')
        self.assertEqual(self.sample_booking_b.price_total, 13.0)
        self.assertEqual(self.sample_booking_b.nb_places(), 5)

    def test_bookingtwo(self):
        """
        Test a performance without any free places

        """
        self.assertEqual(self.sample_booking_b.nb_places_str, "5")

    def test_booking_save(self):
        """
        Test save method
        """
        foo_booking = Booking.objects.create(performance=self.smp_perf_full,
                                             firstname='foobar',
                                             name='lorem',
                                             uuid=None,
                                             places_a=0,
                                             places_b=0,
                                             places_c=0,
                                             places_d=0,
                                             places_e=5)
        foo_booking.save()
        self.assertEqual(foo_booking.name, 'lorem')
        self.assertIsNotNone(foo_booking.uuid)

    def test_booking_savezero(self):
        """
        Test save method with 0 places

        Do not save with 0 places
        """
        foo_booking = Booking.objects.create(performance=self.smp_perf_full,
                                             firstname='foobar',
                                             name='lorem',
                                             uuid=None,
                                             places_a=0,
                                             places_b=0,
                                             places_c=0,
                                             places_d=0,
                                             places_e=0)
        foo_booking.save()
        self.assertIsNone(foo_booking.uuid)

    def test_booking_mailpublic(self):
        """
        Test email sent to whom booked a place

        """
        self.sample_booking_a.mail_public('foo.hypadrie.eu',
                                          'from_public@foo.fr')

        self.assertEqual(len(mail.outbox), 1)

    def test_booking_mailadmin(self):
        """
        Test emails sent to show's contact
        """

        self.sample_booking_a.mail_admin('foo.hypadrie.eu',
                                         'from_admin@foo.fr',
                                         'to@foo.fr')

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].to, ['to@foo.fr'])

    def test_booking_userlist(self):
        """
        Listing of bookings by email
        """
        User.objects.create_user('footest', 'foo@bar.com', 'admintest')
        email = 'foobar@hypadrie.eu'
        Booking.objects.all().delete()

        Booking.objects.create(performance=self.smp_perf_full,
                               firstname='foobar',
                               name='lorem',
                               uuid=None,
                               email='foobar@hypadrie.eu',
                               places_a=1,
                               places_b=0,
                               places_c=0,
                               places_d=0,
                               places_e=0)

        Booking.objects.create(performance=self.smp_perf_full,
                               firstname='foobar',
                               name='lorem',
                               uuid=None,
                               email=email,
                               places_a=1,
                               places_b=0,
                               places_c=0,
                               places_d=0,
                               places_e=0)

        client = Client()
        client.login(username='footest', password='admintest')

        response = client.get('/resalist/email/%s/' % email)
        self.assertContains(response, email, status_code=200)
        # number of bookings
        self.assertContains(response, '(2)', status_code=200)
