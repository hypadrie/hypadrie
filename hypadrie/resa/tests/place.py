# -*- coding: utf-8 -*-  pylint: disable-msg=R0801
"""

 Copyright (c) 2012 Rodolphe Quiédeville <rodolphe@quiedeville.org>

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Test cases for place

"""
from django.test import TestCase
from resa.models import Place


class PlaceTests(TestCase):  # pylint: disable-msg=R0904
    """
    Tests for the model-oriented functionality of hypadrie-resa,
    including ``Place``.
    """
    def setUp(self):
        """
        Init
        """
        self.sample_place_located = Place.objects.create(title='foo_place_l')

        self.sample_place_unlocated = Place.objects.create(title='foo_place_u',
                                                           lon=9.23,
                                                           lat=34.434)

    def test_isnot_located(self):
        """
        Test that a newly-created place is not located

        """
        self.failUnless(self.sample_place_unlocated.lon)
        self.failUnless(self.sample_place_unlocated.lat)
        self.assertEqual(self.sample_place_unlocated.title,
                         'foo_place_u')

    def test_is_located(self):
        """
        Test that a newly-created user is inactive.

        """
        self.failIf(self.sample_place_located.lon)
        self.failIf(self.sample_place_located.lat)
        self.assertEqual(str(self.sample_place_located), 'foo_place_l')

    def test_url(self):
        """
        Absolute URL
        """
        attend = self.sample_place_located.get_absolute_url()
        self.assertEqual(attend, '/place/1')
