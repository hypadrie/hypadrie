# -*- coding: utf-8 -*-
#
# Copyright (c) 2012 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Unit tests for api v1 with authentification

"""
from django.test import TestCase
from django.test import Client
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.shortcuts import get_object_or_404
from datetime import datetime
from hypadrie.resa.models import Show
from hypadrie.resa.models import Place
from hypadrie.resa.models import Booking
from hypadrie.resa.models import Performance
from tastypie.models import ApiKey


class ApiAuthTests(TestCase):  # pylint: disable-msg=R0904
    """
    Test the API witth authentification
    """
    def setUp(self):
        """
        Init
        """
        self.site = Site.objects.create(domain='sample_site')

        self.usera = User.objects.create(username='apiuserA')
        self.userb = User.objects.create(username='apiuserB')

        self.place_located = Place.objects.create(title='foo_place_l')

        sample_show = Show.objects.create(title='foo_show',
                                          company='foobar',
                                          created_by=self.usera)
        sample_show.owner.add(self.usera.id)

        sample_showb = Show.objects.create(title='foo',
                                           company='foobar',
                                           created_by=self.usera,
                                           published=True)
        sample_showb.owner.add(self.userb.id)

        self.smp_perf = Performance.objects.create(
            show=sample_show,
            place=self.place_located,
            date=datetime(2012, 6, 7, 20, 10, 2),
            nb_places_available=144,  # we use this values in assert
            price_a=1,
            price_b=2,
            price_c=3,
            price_d=4,
            price_e=5)

        self.smp_perf_non_empty = Performance.objects.create(
            show=sample_showb,
            place=self.place_located,
            date='2012-06-06 23:20:20',
            nb_places_available=166,  # we use this values in assert
            price_a=1,
            price_b=2,
            price_c=3,
            price_d=4,
            price_e=5)

        Booking.objects.create(
            performance=self.smp_perf,
            firstname='bookoobar',
            places_a=1,
            places_b=2,
            places_c=3,
            places_d=4,
            places_e=5)

        Booking.objects.create(
            performance=self.smp_perf,
            firstname='lorem 5 places',
            name='ipsum',
            places_a=0,
            places_b=2,
            places_c=3,
            places_d=0,
            places_e=0)

        Booking.objects.create(
            performance=self.smp_perf_non_empty,
            firstname='showb foobar 13 places',
            places_a=10,
            places_b=0,
            places_c=1,
            places_d=1,
            places_e=1)

    def test_shows(self):
        """
        List shows

        - in auth part shows are filtered on user owner
        """
        sha = Show.objects.create(title='alphabeta',
                                  company='foobar',
                                  published=False,
                                  created_by=self.usera)
        sha.owner.add(self.usera.id)

        shb = Show.objects.create(title='teta',
                                  company='foobar',
                                  published=False,
                                  created_by=self.userb)
        shb.owner.add(self.userb.id)

        client = Client()
        apikey = get_object_or_404(ApiKey, user=self.usera)
        url = '/api/v1/auth/show/'
        params = {'format': 'json',
                  'username': self.usera.username,
                  'api_key': apikey.key}
        response = client.get(url, params)
        self.assertEqual(sha.owner.all()[0].id, self.usera.id)
        self.assertContains(response, 'alphabeta', status_code=200)
        self.assertNotContains(response, 'teta', status_code=200)

    def test_wrongkey(self):
        """
        Check that a wrong key return a 401
        """
        Show.objects.create(title='alphabravo',
                            company='foobar',
                            published=True,
                            created_by=self.usera)

        client = Client()
        url = '/api/v1/auth/show/'
        params = {'format': 'json',
                  'username': self.usera.username,
                  'api_key': 'wrongkey'}
        resp = client.get(url, params)
        self.assertEqual(resp.status_code, 401)  # pylint: disable-msg=E1103

    def test_booking_protected(self):
        """
        Booking is protected
        """
        client = Client()
        url = '/api/v1/auth/booking/'
        params = {'format': 'json'}
        resp = client.get(url, params)
        self.assertEqual(resp.status_code, 401)  # pylint: disable-msg=E1103

    def test_show_protected(self):
        """
        Show part is protected
        """
        client = Client()
        url = '/api/v1/auth/show/'
        params = {'format': 'json'}
        resp = client.get(url, params)
        self.assertEqual(resp.status_code, 401)  # pylint: disable-msg=E1103

    def test_bookings(self):
        """
        Bookings in auth context
        """
        client = Client()
        apikey = get_object_or_404(ApiKey, user=self.usera)
        url = '/api/v1/auth/booking/'
        params = {'format': 'json',
                  'username': self.usera.username,
                  'api_key': apikey.key}
        resp = client.get(url, params)

        self.assertContains(resp, 'bookoobar', status_code=200)
        self.assertNotContains(resp, 'showb', status_code=200)

    def test_performances(self):
        """
        Performance in auth context
        """
        client = Client()
        apikey = get_object_or_404(ApiKey, user=self.usera)
        url = '/api/v1/auth/performance/'
        params = {'format': 'json',
                  'username': self.usera.username,
                  'api_key': apikey.key}
        resp = client.get(url, params)

        self.assertContains(resp, '144', status_code=200)
        self.assertNotContains(resp, '166', status_code=200)

    def test_events_list(self):
        """
        Events in auth context
        """
        show = Show.objects.create(title='event1',
                                   company='foobar',
                                   created_by=self.usera,
                                   events=True,
                                   published=True)
        show.owner.add(self.usera.id)

        Performance.objects.create(
            show=show,
            place=self.place_located,
            date=datetime(2012, 6, 7, 20, 10, 2),
            nb_places_available=144,  # we use this values in assert
            information='BiHo2mae',
            price_a=1,
            price_b=2,
            price_c=3,
            price_d=4,
            price_e=5)

        client = Client()
        apikey = get_object_or_404(ApiKey, user=self.usera)
        url = '/api/v1/auth/event/'
        params = {'format': 'json',
                  'username': self.usera.username,
                  'api_key': apikey.key}
        resp = client.get(url, params)

        self.assertContains(resp, 'BiHo2mae', status_code=200)
        self.assertNotContains(resp, 'showb', status_code=200)
