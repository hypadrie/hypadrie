# -*- coding: utf-8 -*-
"""

 Copyright (c) 2012 Rodolphe Quiédeville <rodolphe@quiedeville.org>

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Test cases for performance admin
"""
from django.test import TestCase
from django.contrib.auth.models import User
from django.contrib.admin.sites import AdminSite
from datetime import datetime, timedelta
from resa.admins_performance import PerformanceAdmin
from resa.models import Performance, Place, Show


class PerformanceAdminTests(TestCase):  # pylint: disable-msg=R0904
    """
    Performance admin panel
    """
    def setUp(self):
        """
        Init
        """
        self.user = User.objects.create(username='admin_pfadmin')

        self.place = Place.objects.create(title='foo_place_l')

        self.show = Show.objects.create(title='foo_show',
                                        company='foobar',
                                        created_by=self.user,)

        self.perf = Performance.objects.create(
            show=self.show,
            place=self.place,
            date=datetime(2012, 6, 7, 20, 10, 2),
            nb_places_available=42,
            price_a=10,
            price_b=3,
            price_c=2,
            price_d=4,
            price_e=5)

        self.site = AdminSite()

    def test_make_published(self):
        """
        Test the unpublished method
        """
        mad = PerformanceAdmin(Performance, self.site)

        Performance.objects.create(show=self.show,
                                   place=self.place,
                                   date=datetime(2012, 6, 7, 21, 10, 2),
                                   nb_places_available=42,
                                   price_a=10,
                                   price_b=5,
                                   price_c=5,
                                   price_d=5,
                                   price_e=5,
                                   published=False)

        qry = Performance.objects.filter(nb_places_available=42)

        self.assertEqual(qry[0].published, False)
        mad.make_published(None, qry)
        self.assertEqual(qry[0].published, True)

    def test_make_unpublished(self):
        """
        Test the unpublished method
        """
        mad = PerformanceAdmin(Performance, self.site)
        date_filter = datetime(2012, 6, 7, 14, 42, 13)
        Performance.objects.create(show=self.show,
                                   place=self.place,
                                   date=date_filter,
                                   nb_places_available=42,
                                   price_a=10,
                                   price_b=5,
                                   price_c=5,
                                   price_d=5,
                                   price_e=5,
                                   published=True)

        qry = Performance.objects.filter(date=date_filter)
        #  Be sure to check the right object
        self.assertEqual(len(qry), 1)
        self.assertEqual(qry[0].published, True)
        mad.make_unpublished(None, qry)
        self.assertEqual(qry[0].published, False)

    def test_format_date(self):
        """
        Test the format date
        """
        perf = Performance.objects.create(show=self.show,
                                          place=self.place,
                                          date=datetime(2012, 6, 7, 21, 10, 2),
                                          nb_places_available=42,
                                          price_a=10,
                                          price_b=5,
                                          price_c=5,
                                          price_d=5,
                                          price_e=5,
                                          published=True)

        mad = PerformanceAdmin(Performance, self.site)

        attend = datetime(2012, 6, 7, 21, 10, 2).strftime('%A %d %b %Y %H:%M')
        date = mad.format_date(perf)

        self.assertEqual(date, attend)

    def test_copy_published(self):
        """
        Test the copy action on performance objects in admin panel
        """
        mad = PerformanceAdmin(Performance, self.site)
        date_perf = datetime(2012, 9, 1, 12, 12, 2)
        Performance.objects.create(show=self.show,
                                   place=self.place,
                                   date=date_perf,
                                   nb_places_available=42,
                                   price_a=10,
                                   price_b=5,
                                   price_c=5,
                                   price_d=5,
                                   price_e=5,
                                   published=False)

        before = len(Performance.objects.all())

        self.assertNotEqual(before, 0)
        qry = Performance.objects.filter(date=date_perf)
        mad.copy_published(None, qry)
        after = len(Performance.objects.all())
        self.failUnless(after > before)

    def test_copy_published10(self):
        """
        Test the copy action on performance objects in admin panel
        Check the max number of 10 copies
        """
        mad = PerformanceAdmin(Performance, self.site)
        date_perf = datetime(2012, 9, 1, 12, 12, 2)
        #  Create 12 objects with same date
        while len(Performance.objects.all()) < 12:
            Performance.objects.create(show=self.show,
                                       place=self.place,
                                       date=date_perf,
                                       nb_places_available=42,
                                       price_a=10,
                                       price_b=5,
                                       price_c=5,
                                       price_d=5,
                                       price_e=5,
                                       published=False)

        before = len(Performance.objects.all())
        qry = Performance.objects.filter(date=date_perf)
        mad.copy_published(None, qry)
        after = len(Performance.objects.all())
        self.assertEqual(after, before + 10)

    def test_isopenbooking(self):
        """
        Test if booking is open, with available dates
        The booking is open manually and dates are compliant.
        """
        past = datetime.now() - timedelta(hours=2)
        future = datetime.now() + timedelta(days=42)
        perf = Performance.objects.create(show=self.show,
                                          place=self.place,
                                          date=datetime.now(),
                                          nb_places_available=42,
                                          price_a=10,
                                          published=False,
                                          open_booking=True,
                                          open_booking_from=past,
                                          open_booking_until=future)

        self.assertEqual(perf.open_booking, True)
        self.assertEqual(perf.booking_is_open, True)

    def test_isopenbooking2(self):
        """
        Test if booking is open
        The booking is open manually and dates are not set
        """
        perf = Performance.objects.create(show=self.show,
                                          place=self.place,
                                          date=datetime.now(),
                                          nb_places_available=42,
                                          price_a=10,
                                          published=False,
                                          open_booking=True)

        self.assertEqual(perf.open_booking, True)
        self.assertEqual(perf.booking_is_open, True)

    def test_isclosed(self):
        """
        Test if booking is closed
         - available dates
         - booking is closed manually

         The booking must be closed
        """
        past = datetime.now() - timedelta(hours=2)
        future = datetime.now() + timedelta(days=42)
        perf = Performance.objects.create(show=self.show,
                                          place=self.place,
                                          date=datetime.now(),
                                          nb_places_available=42,
                                          price_a=10,
                                          published=False,
                                          open_booking=False,
                                          open_booking_from=past,
                                          open_booking_until=future)

        self.assertEqual(perf.open_booking, False)
        self.assertEqual(perf.booking_is_open, False)

    def test_isclosed2(self):
        """
        Test if booking is closed, with available dates
        It'll open in 2 days
        """
        future = datetime.now() + timedelta(days=2)
        perf = Performance.objects.create(show=self.show,
                                          place=self.place,
                                          date=datetime.now(),
                                          nb_places_available=42,
                                          price_a=10,
                                          published=True,
                                          open_booking=True,
                                          open_booking_from=future)

        self.assertEqual(perf.open_booking, True)
        self.assertEqual(perf.booking_is_open, False)

    def test_isclosed3(self):
        """
        Test if booking is closed, with available dates
         - closing date was 3 hours in past

        Must be closed
        """
        past = datetime.now() - timedelta(hours=3)
        perf = Performance.objects.create(show=self.show,
                                          place=self.place,
                                          date=datetime.now(),
                                          nb_places_available=42,
                                          price_a=10,
                                          published=True,
                                          open_booking=True,
                                          open_booking_until=past)

        self.assertEqual(perf.open_booking, True)
        self.assertEqual(perf.booking_is_open, False)

    def test_isclosed4(self):
        """
        Test if booking is closed, with available dates
        """
        past = datetime.now() - timedelta(hours=3)
        future = datetime.now() + timedelta(hours=3)
        perf = Performance.objects.create(show=self.show,
                                          place=self.place,
                                          date=datetime.now(),
                                          nb_places_available=42,
                                          price_a=10,
                                          published=True,
                                          open_booking=False,
                                          open_booking_from=past,
                                          open_booking_until=future)

        self.assertEqual(perf.open_booking, False)
        self.assertEqual(perf.booking_is_open, False)
