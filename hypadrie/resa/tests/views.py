# -*- coding: utf-8 -*-
#
# Copyright (c) 2012 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Unit tests for views in resa
"""
from django.conf import settings
from django.contrib.sites.models import Site
from django.core import mail
from django.test import TestCase, Client
from django.http import HttpRequest
from django.contrib.auth.models import User
from datetime import datetime
from resa.models import Booking, Show, Place, Performance, Festival
from resa import views


class ViewsTests(TestCase):  # pylint: disable-msg=R0904
    """
    Test all the views (or try to do)
    """
    def setUp(self):
        """
        Init
        """
        self.site = Site.objects.create(domain='sample_site')
        user = User.objects.create(username='sample_admin')

        self.sample_place_located = Place.objects.create(title='foo_place_l')
        self.sample_show = Show.objects.create(title='foo_show',
                                               company='foobar',
                                               created_by=user)
        self.sample_show.site.add(self.site.id)

        self.smp_perf_non_empty = Performance.objects.create(
            show=self.sample_show,
            place=self.sample_place_located,
            date='2012-06-06 23:20:20',
            nb_places_available=120,
            price_a=10,
            price_b=0,
            price_c=0,
            price_d=5,
            price_e=0)

        self.sample_booking_a = Booking.objects.create(
            performance=self.smp_perf_non_empty,
            firstname='foobar',
            places_a=1,
            places_b=2,
            places_c=1,
            places_d=1,
            places_e=1)

        settings.CACHES = {
            'default': {
                'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
                'LOCATION': 'unique-snowflake'}}

    def test_views_map(self):
        """
        The map view
        """
        result = views.themap(None)

        self.assertEqual(result.status_code, 200)  # pylint: disable-msg=E1103
        self.failUnless('<body' in result.content)
        self.failUnless('</body>' in result.content)

    def test_views_about(self):
        """
        The simple about view returns a 200 http code
        """
        result = views.about(None)

        self.assertEqual(result.status_code, 200)  # pylint: disable-msg=E1103
        self.failUnless('<body' in result.content)
        self.failUnless('</body>' in result.content)

    def test_helpview_get(self):
        """
        The help view in GET
        """
        requ = HttpRequest()
        requ.method = 'GET'
        result = views.helpview(requ)

        self.assertEqual(result.status_code, 200)  # pylint: disable-msg=E1103
        self.failUnless('<body' in result.content)
        self.failUnless('</body>' in result.content)

    def test_helpview_postnobooking(self):
        """
        The help view in POST with no booking in database
        """
        # prepare datas
        requ = HttpRequest()
        requ.method = 'POST'
        requ.META = {'SERVER_NAME': 'FOO_SERVER_NAME'}
        requ.POST = {'email': 'foo@foo.bar'}
        Booking.objects.all().delete()
        #  action
        result = views.helpview(requ)
        #  asserts
        self.assertEqual(result.status_code, 200)  # pylint: disable-msg=E1103
        self.failUnless('alert-error' in result.content)

    def test_helpview_invalidemail(self):
        """
        The help view in POST with invalid email
        """
        requ = HttpRequest()
        requ.method = 'POST'
        requ.META = {}
        requ.META['SERVER_NAME'] = 'FOO_SERVER_NAME'
        #  invalid email
        requ.POST = {'email': 'foo@foo'}
        result = views.helpview(requ)

        self.assertEqual(result.status_code, 200)  # pylint: disable-msg=E1103
        self.failUnless('<body' in result.content)
        self.failUnless('control-label' in result.content)
        self.failUnless('</body>' in result.content)

    def test_helpview_validemail(self):
        """
        The help view in POST with valid email
        """
        requ = HttpRequest()
        requ.method = 'POST'
        requ.META = {}
        requ.META['SERVER_NAME'] = 'FOO_SERVER_NAME'
        requ.POST = {'email': 'foo@foobar.com'}

        Booking.objects.create(performance=self.smp_perf_non_empty,
                               firstname='foobar',
                               email='foo@foobar.com',
                               places_a=1,
                               places_b=1,
                               places_c=2,
                               places_d=1,
                               places_e=1)
        #  action
        result = views.helpview(requ)
        #  results
        self.assertEqual(result.status_code, 200)  # pylint: disable-msg=E1103
        self.failUnless('<body' in result.content)
        self.failUnless('</body>' in result.content)
        self.failUnless('alert-success' in result.content)

    def test_views_contact(self):
        """
        The contact view
        """
        requ = HttpRequest()
        result = views.contact(requ)

        self.assertEqual(result.status_code, 200)  # pylint: disable-msg=E1103
        self.failUnless('<body' in result.content)
        self.failUnless('</body>' in result.content)

    def test_views_festival(self):
        """
        The festival view
        """
        beg = datetime(2012, 6, 7, 20, 10, 2)
        end = datetime(2012, 9, 2, 20, 10, 2)
        Festival.objects.create(title='Lorem ipsum',
                                published=True,
                                date_begin=beg,
                                date_end=end,
                                description='Lorem ipsum sic',
                                url='foo')
        result = views.festival(HttpRequest(), 'foo')

        self.assertEqual(result.status_code, 200)  # pylint: disable-msg=E1103
        self.failUnless('<body' in result.content)
        self.failUnless('</body>' in result.content)
        self.failUnless('Lorem ipsum' in result.content)

    def test_views_spectacles(self):
        """
        The show view
        """
        client = Client()
        resp = client.get('/spectacles/')
        self.assertEqual(resp.status_code, 200)  # pylint: disable-msg=E1103

    def test_views_place(self):
        """
        The place view by URL
        """
        client = Client()
        resp = client.get('/place/1')
        self.assertEqual(resp.status_code, 200)  # pylint: disable-msg=E1103

    def test_views_festivalspectacles(self):
        """
        The place view by URL
        """
        beg = datetime(2012, 4, 7, 20, 10, 2)
        end = datetime(2012, 9, 2, 20, 10, 2)
        Festival.objects.all().delete()
        Festival.objects.create(title='Lorem ipsum',
                                published=True,
                                date_begin=beg,
                                date_end=end,
                                description='Lorem ipsum sic',
                                url='foo')

        client = Client()
        resp = client.get('/foo/spectacles/')
        self.assertEqual(resp.status_code, 200)  # pylint: disable-msg=E1103

    def test_views_festivalabout(self):
        """
        The place view by URL
        """
        beg = datetime(2012, 4, 7, 20, 10, 2)
        end = datetime(2012, 9, 2, 20, 10, 2)
        Festival.objects.all().delete()
        Festival.objects.create(title='Lorem ipsum',
                                published=True,
                                date_begin=beg,
                                date_end=end,
                                description='Lorem ipsum sic',
                                url='foo')

        client = Client()
        resp = client.get('/foo/about/')
        self.assertEqual(resp.status_code, 200)  # pylint: disable-msg=E1103

    def test_views_festivalcontact(self):
        """
        The place view by URL
        """
        beg = datetime(2012, 4, 7, 20, 10, 2)
        end = datetime(2012, 9, 2, 20, 10, 2)
        Festival.objects.all().delete()
        Festival.objects.create(title='Lorem ipsum',
                                published=True,
                                date_begin=beg,
                                date_end=end,
                                description='Lorem ipsum sic',
                                url='foo')

        client = Client()
        resp = client.get('/foo/contact/')
        self.assertEqual(resp.status_code, 200)  # pylint: disable-msg=E1103

    def test_views_place_get(self):
        """
        The place view
        """
        result = views.place(HttpRequest(), 1)

        self.assertEqual(result.status_code, 200)  # pylint: disable-msg=E1103
        self.failUnless('<body' in result.content)
        self.failUnless('</body>' in result.content)

    def test_views_spectacle(self):
        """
        The place view
        """
        result = views.spectacle(HttpRequest(), 1)

        self.assertEqual(result.status_code, 200)  # pylint: disable-msg=E1103
        self.failUnless('<body' in result.content)
        self.failUnless('</body>' in result.content)

    def test_views_performances(self):
        """
        The place view
        """
        result = views.performances(HttpRequest())

        self.assertEqual(result.status_code, 200)  # pylint: disable-msg=E1103
        self.failUnless('<body' in result.content)
        self.failUnless('</body>' in result.content)

    def test_festival_performances(self):
        """
        The festival performance view
        """
        beg = datetime(2012, 4, 7, 20, 10, 2)
        end = datetime(2012, 9, 2, 20, 10, 2)
        Festival.objects.create(title='Lorem ipsum',
                                published=True,
                                date_begin=beg,
                                date_end=end,
                                description='Lorem ipsum sic',
                                url='foo')

        result = views.festival_performances(HttpRequest(), 'foo')

        self.assertEqual(result.status_code, 200)  # pylint: disable-msg=E1103
        self.failUnless('<body' in result.content)
        self.failUnless('</body>' in result.content)

    def test_performance(self):
        """
        The performance view with real performance id
        """
        result = views.performance(HttpRequest(), 1)

        self.assertEqual(result.status_code, 200)  # pylint: disable-msg=E1103
        self.failUnless('<body' in result.content)
        self.failUnless('</body>' in result.content)

    def test_performance_resa_get(self):
        """
        The performance_resa view in GET
        """
        requ = HttpRequest()
        requ.method = 'GET'

        result = views.performance_resa(requ, 1)
        self.assertEqual(result.status_code, 200)  # pylint: disable-msg=E1103

    def test_performance_resapost(self):
        """
        The performance_resa view in POST, valid datas
        """
        settings.MAX_BOOKABLE_PLACE = 10
        requ = HttpRequest()
        requ.method = 'POST'
        requ.META = {'SERVER_NAME': 'www.foo.bar'}
        requ.POST = {'firstname': 'foobar',
                     'name': '',
                     'email': '',
                     'nb_place_a': '2',
                     'nb_place_b': '2',
                     'nb_place_c': '0',
                     'nb_place_d': '2',
                     'nb_place_e': '2',
                     'csrfmiddlewaretoken': '88c25fbe6ed4871bf9e8e83820a4e001'}

        s_perf = Performance.objects.create(show=self.sample_show,
                                            place=self.sample_place_located,
                                            date='2012-06-07 23:20:20',
                                            open_booking=True,
                                            published=True,
                                            nb_places_available=20,
                                            price_a=10,
                                            price_b=10,
                                            price_c=10,
                                            price_d=10,
                                            price_e=5)

        result = views.performance_resa(requ, s_perf.pk)
        self.assertEqual(result.status_code, 200)  # pylint: disable-msg=E1103
        self.failUnless('alert-info' in result.content)
        self.failUnless('alert-success' in result.content)

    def test_performance_resapostwmail(self):
        """
        The performance_resa view in POST, valid datas
        """
        settings.MAX_BOOKABLE_PLACE = 10
        requ = HttpRequest()
        requ.method = 'POST'
        requ.META = {'SERVER_NAME': 'www.foo.bar'}
        requ.POST = {'firstname': 'foobar',
                     'name': '',
                     'email': '',
                     'nb_place_a': '2',
                     'nb_place_b': '2',
                     'nb_place_c': '0',
                     'nb_place_d': '2',
                     'nb_place_e': '2',
                     'csrfmiddlewaretoken': '88c25fbe6ed4871bf9e8e83820a4e001'}

        s_perf = Performance.objects.create(show=self.sample_show,
                                            place=self.sample_place_located,
                                            date='2012-06-07 23:20:20',
                                            open_booking=True,
                                            published=True,
                                            bkng_send_email=True,
                                            bkng_contact_email='foo@bar.com',
                                            nb_places_available=20,
                                            price_a=10,
                                            price_b=10,
                                            price_c=10,
                                            price_d=10,
                                            price_e=5)

        views.performance_resa(requ, s_perf.pk)

        self.assertEqual(len(mail.outbox), 2)

    def test_perf_resapostinvalid(self):
        """
        The performance_resa view in POST, invalid datas
        """
        settings.MAX_BOOKABLE_PLACE = 10
        requ = HttpRequest()
        requ.method = 'POST'
        requ.POST = {'firstname': 'foobar',
                     'name': '',
                     'email': '',
                     'nb_place_a': '20',
                     'nb_place_b': '20',
                     'nb_place_c': '20',
                     'nb_place_d': '2',
                     'nb_place_e': '2'}

        s_perf = Performance.objects.create(show=self.sample_show,
                                            place=self.sample_place_located,
                                            date='2012-06-07 23:20:20',
                                            open_booking=True,
                                            published=True,
                                            nb_places_available=20,
                                            price_a=10,
                                            price_b=10,
                                            price_c=10,
                                            price_d=10,
                                            price_e=5)

        result = views.performance_resa(requ, s_perf.pk)
        self.assertEqual(result.status_code, 200)  # pylint: disable-msg=E1103
        self.failUnless('alert-error' in result.content)

    def test_perf_resacancel(self):
        """
        Cancel a booking
        """
        settings.MAX_BOOKABLE_PLACE = 10

        s_perf = Performance.objects.create(show=self.sample_show,
                                            place=self.sample_place_located,
                                            date='2012-06-07 23:20:20',
                                            open_booking=True,
                                            published=True,
                                            nb_places_available=20,
                                            price_a=10,
                                            price_b=10,
                                            price_c=10,
                                            price_d=10,
                                            price_e=5)

        self.assertEqual(s_perf.nb_places_a_booked, 0)
        self.assertEqual(s_perf.nb_places_c_booked, 0)

        s_booking = Booking.objects.create(
            performance=s_perf,
            firstname='foobar',
            places_a=2,
            places_b=2,
            places_c=2,
            places_d=2,
            places_e=2,
            uuid='49675bbf-5b3b-4ee1-9bf2-1749ef841295')

        self.assertEqual(s_perf.nb_places_a_booked, 2)
        self.assertEqual(s_perf.nb_places_c_booked, 2)

        requ = HttpRequest()
        requ.method = 'POST'
        requ.META = {'SERVER_NAME': 'FOO_SERVER_NAME'}
        requ.POST = {'firstname': 'foobar',
                     'name': '',
                     'email': '',
                     'nb_place_a': '20', 'nb_place_c': '20'}

        uuid = s_booking.uuid.split('-')

        views.performance_resa_cancel(requ,
                                      s_perf.pk,
                                      uuid[0],
                                      uuid[1],
                                      uuid[2],
                                      uuid[3],
                                      uuid[4])

        self.assertEqual(s_perf.nb_places_booked, 0)

    def test_perf_resacancelfailed(self):
        """
        Cancel a not exists booking
        """
        settings.MAX_BOOKABLE_PLACE = 10

        s_perf = Performance.objects.create(show=self.sample_show,
                                            place=self.sample_place_located,
                                            date='2012-06-07 23:20:20',
                                            open_booking=True,
                                            published=True,
                                            nb_places_available=20,
                                            price_a=10,
                                            price_b=10,
                                            price_c=10,
                                            price_d=10,
                                            price_e=5)

        self.assertEqual(s_perf.nb_places_a_booked, 0)
        self.assertEqual(s_perf.nb_places_c_booked, 0)

        Booking.objects.create(performance=s_perf,
                               firstname='foobar',
                               places_a=2,
                               places_b=0,
                               places_c=2,
                               places_d=0,
                               places_e=0,
                               uuid='49675bbf-5b3b-4ee1-9bf2-1749ef841295')

        self.assertEqual(s_perf.nb_places_a_booked, 2)
        self.assertEqual(s_perf.nb_places_c_booked, 2)

        requ = HttpRequest()
        requ.method = 'POST'
        requ.POST = {'firstname': 'foobar',
                     'name': '',
                     'email': '',
                     'nb_place_a': '20',
                     'nb_place_b': '20',
                     'nb_place_c': '20',
                     'nb_place_d': '20',
                     'nb_place_e': '20'}

        views.performance_resa_cancel(requ,
                                      s_perf.pk,
                                      'foo', 'foo', 'foo', 'foo', 'foo')

        self.assertEqual(s_perf.nb_places_booked, 4)

    def test_listresapdf_anonymous(self):
        """
        The list_resa_pdf view by URL in anonymous
        """
        client = Client()
        resp = client.get('/representation/1/pdf/')
        self.assertEqual(resp.status_code, 302)

    def test_listresapdf_auth(self):
        """
        The list_resa_pdf view by URL with authenticated account
        """
        User.objects.create_user('footest', 'foo@bar.com', 'admintest')
        client = Client()
        client.login(username='footest', password='admintest')

        s_perf = Performance.objects.create(show=self.sample_show,
                                            place=self.sample_place_located,
                                            date='2012-06-07 23:20:20',
                                            open_booking=True,
                                            published=True,
                                            nb_places_available=20,
                                            price_a=10,
                                            price_b=5,
                                            price_c=10,
                                            price_d=5,
                                            price_e=5)

        Booking.objects.create(performance=s_perf,
                               firstname='foobar',
                               places_a=2,
                               places_b=2,
                               places_c=2,
                               places_d=2,
                               places_e=2,
                               uuid='49675bbf-5b3b-4ee1-9bf2-1749ef841295')

        resp = client.get('/representation/%s/pdf/' % (s_perf.id))
        self.assertEqual(resp.status_code, 200)  # pylint: disable-msg=E1103

    def test_bookinglist_anonymous(self):
        """
        The booking list in anonymous
        """
        client = Client()
        resp = client.get('/resalist/1/')
        self.assertEqual(resp.status_code, 302)

    def test_bookinglist_auth(self):
        """
        The booking list auth
        """
        User.objects.create_user('footest', 'foo@bar.com', 'foopwd')
        client = Client()
        client.login(username='footest', password='foopwd')

        resp = client.get('/resalist/1/')
        self.assertEqual(resp.status_code, 200)  # pylint: disable-msg=E1103

    def test_bookinglist_badpagenumber(self):
        """
        The booking list auth with a bad number page
        """
        User.objects.create_user('footest', 'foo@bar.com', 'foopwd')
        client = Client()
        client.login(username='footest', password='foopwd')

        resp = client.get('/resalist/1/?page=a')
        self.assertEqual(resp.status_code, 200)  # pylint: disable-msg=E1103

    def test_bookinglist_invalidpagenb(self):
        """
        The booking list auth with invalid page number
        """
        User.objects.create_user('footest', 'foo@bar.com', 'foopwd')
        client = Client()
        client.login(username='footest', password='foopwd')

        resp = client.get('/resalist/1/?page=42')
        self.assertEqual(resp.status_code, 200)  # pylint: disable-msg=E1103

    def test_bookingdelete_anonymous(self):
        """
        The booking deletion url in anonymous

        Must be redirected to login page
        """
        client = Client()
        url = '/resalist/1/delete/49675bbf-5b3b-4ee1-9bf2-4249ef841242'
        resp = client.get(url)
        location = resp.__getitem__('Location')
        self.assertEqual(resp.status_code, 302)
        self.failUnless('login/?next=/resalist/1/' in location)

    def test_bookingdelete_auth(self):
        """
        The booking deletion process in auth

        Do a redirect on the same page
        """
        User.objects.create_user('footest', 'foo@bar.com', 'foopwd')
        client = Client()
        client.login(username='footest', password='foopwd')
        Booking.objects.create(performance=self.smp_perf_non_empty,
                               firstname='foobar',
                               places_a=2,
                               places_b=2,
                               places_c=2,
                               places_d=2,
                               places_e=2,
                               uuid='49675bbf-5b3b-4ee1-9bf2-4349ef841242')
        before = self.smp_perf_non_empty.nb_places_a_booked
        #
        url = '/resalist/1/delete/49675bbf-5b3b-4ee1-9bf2-4349ef841242'
        resp = client.get(url)
        #
        after = self.smp_perf_non_empty.nb_places_a_booked
        location = resp.__getitem__('Location')
        self.assertEqual(resp.status_code, 302)
        self.failUnless('/resalist/1/' in location)
        self.failIf('login/?next=/resalist/1/' in location)
        self.assertEqual(before, after + 2)

    def test_logout_view(self):
        """
        The logout page
        """
        client = Client()
        resp = client.get('/logout')
        self.assertEqual(resp.status_code, 302)

    def test_auth_view(self):
        """
        The authentification process
        """
        User.objects.create_user('footest', 'foo@bar.com', 'foopwd')

        client = Client()
        token = '88c25fbe6ed4871bf9e8e83820a4e001'
        resp = client.post('/accounts/login/',
                           {'username': 'footest',
                            'password': 'foopwd',
                            'next': '',
                            'csrfmiddlewaretoken': token}
                           )

        self.assertEqual(resp.status_code, 302)
        self.failUnless('/accounts/profile/' in resp.__getitem__('Location'))

    def test_auth_wrongcredentials(self):
        """
        The authentification process with wrong credentials
        """
        User.objects.create_user('footest', 'foo@bar.com', 'foopwd')

        client = Client()
        token = '88c25fbe6ed4871bf9e8e83820a4e001'
        resp = client.post('/accounts/login/',
                           {'username': 'footest',
                            'password': 'WHATEVER',
                            'next': '',
                            'csrfmiddlewaretoken': token}
                           )

        self.assertEqual(resp.status_code, 200)  # pylint: disable-msg=E1103

    def test_bookings_anonymous(self):
        """
        The bookings list in anonymous

        Auth is required
        """
        client = Client()
        resp = client.get('/resa/')
        self.assertRedirects(resp, '/accounts/login/?next=/resa/')

    def test_bookings_view_auth(self):
        """
        The bookings list with an authorized user

        Auth is required
        """
        User.objects.create_user('foouser', 'foo@bar.com', 'foopwd')
        client = Client()
        client.login(username='foouser', password='foopwd')
        resp = client.get('/resa/')
        self.assertEqual(resp.status_code, 200)  # pylint: disable-msg=E1103
