# -*- coding: utf-8 -*-  pylint: disable-msg=R0801
#
# Copyright (c) 2012 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Unit tests for resa - Hypadrie about full text indexing

"""
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.test import TestCase, Client
from resa.models import Show


class ProfileTests(TestCase):  # pylint: disable-msg=R0904
    """
    These tests concern full-text indexing

    """
    def setUp(self):
        """
        Init
        """
        self.site = Site.objects.create(domain='sample_site')
        self.user = User.objects.create_user('admin_search',
                                             'admin_search@bar.com',
                                             'admintest')
        settings.SITE_ID = 1

    def test_viewshows(self):
        """
        View owner shows
        """
        Show.objects.all().delete()

        show = Show.objects.create(title='Ibo2iaem',
                                   company='ipsum',
                                   description='happy hacking',
                                   published=True,
                                   created_by=self.user)
        show.site.add(1)
        show.owner.add(self.user.id)

        client = Client()
        client.login(username='admin_search', password='admintest')

        # lookup on description
        response = client.get('/accounts/profile/')
        self.assertContains(response, show.title, status_code=200)

    def test_hideshows(self):
        """
        View owner shows
        """
        Show.objects.all().delete()

        show = Show.objects.create(title='Gecoegh0',
                                   company='ipsum',
                                   description='happy hacking',
                                   published=True,
                                   created_by=self.user)
        show.site.add(1)
        show.owner.add(self.user.id + 1)

        client = Client()
        client.login(username='admin_search', password='admintest')

        # lookup on description
        response = client.get('/accounts/profile/')
        self.assertNotContains(response, show.title, status_code=200)

    def test_viewoneshow(self):
        """
        View owner shows
        """
        Show.objects.all().delete()

        show = Show.objects.create(title='Ibo2iaem',
                                   company='ipsum',
                                   description='happy hacking',
                                   published=True,
                                   created_by=self.user)
        show.site.add(1)
        show.owner.add(self.user.id)

        client = Client()
        client.login(username='admin_search', password='admintest')

        # lookup on description
        response = client.get('/accounts/spectacle/%s' % show.id)
        self.assertContains(response, show.title, status_code=200)

    def test_createshow(self):
        """
        Create a new show in privileged part
        """
        client = Client()
        client.login(username='admin_search', password='admintest')

        token = '88c25fbe6ed4871bf9e8e83820a4e001'
        result = client.post('/accounts/spectacle/nouveau/',
                             {'title': 'footest',
                              'company': 'foopwd',
                              'published': 'false',
                              'csrfmiddlewaretoken': token}
                             )
        self.assertNotContains(result, 'alert-error', status_code=200)

    def test_createshow_missingdatas(self):
        """
        Create a new show in privileged part
        """
        client = Client()
        client.login(username='admin_search', password='admintest')

        token = '88c25fbe6ed4871bf9e8e83820a4e001'
        result = client.post('/accounts/spectacle/nouveau/',
                             {'title': 'footest',
                              'csrfmiddlewaretoken': token}
                             )
        self.assertContains(result, 'errorlist', status_code=200)

    def test_shows(self):
        """
        View all owner's shows
        """
        Show.objects.all().delete()

        show_a = Show.objects.create(title='mbu-Ibo2iaem',
                                     company='ipsum',
                                     description='happy hacking',
                                     published=True,
                                     created_by=self.user)
        show_a.site.add(1)
        show_a.owner.add(self.user.id)

        show_b = Show.objects.create(title='mbu-quohSh0jm',
                                     company='ipsum',
                                     description='happy hacking',
                                     published=True,
                                     created_by=self.user)
        show_b.site.add(1)
        show_b.owner.add(self.user.id)

        show_c = Show.objects.create(title='mbu-tooNo7oo',
                                     company='ipsum',
                                     description='happy hacking',
                                     published=True,
                                     created_by=self.user)
        show_c.site.add(1)
        show_c.owner.add(self.user.id)

        client = Client()
        client.login(username='admin_search', password='admintest')
        response = client.get('/accounts/spectacles/')
        self.assertContains(response, show_a.title, status_code=200)
        self.assertContains(response, show_b.title, status_code=200)
        self.assertContains(response, show_c.title, status_code=200)
