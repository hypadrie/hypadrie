# -*- coding: utf-8 -*-  pylint: disable-msg=R0801
"""

 Copyright (c) 2012 Rodolphe Quiédeville <rodolphe@quiedeville.org>

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Test cases for place

"""
from django.test import TestCase
from resa import utils


class UtilsTests(TestCase):  # pylint: disable-msg=R0904
    """
    utils parts
    """
    def test_hypapdf(self):
        """
        Pdf generation class
        """
        datas = (('firstname', 'name', '2', '3', '2', '4', '5', '40'),
                 ('firstname', 'name', '2', '3', '2', '4', '5', '40'))

        pdf = utils.Hypapdf("title", "date", "place", datas)
        pdf.list_resa_pdf('/dev/null')
        self.assertEqual(len(pdf.datas), 2)
        self.assertEqual(pdf.pdf.getPageNumber(), 2)

    def test_hypapdf_big(self):
        """
        Pdf generation class with more that one page
        """
        bkng = ('firstname', 'name', '2', '3', '2', '4', '5', '40')
        datas = [bkng]
        inc = 0
        while inc < 100:
            datas.append(bkng)
            inc += 1

        pdf = utils.Hypapdf("title", "date", "place", datas)
        pdf.list_resa_pdf('/dev/null')
        self.assertEqual(len(pdf.datas), 101)
        self.assertGreater(pdf.pdf.getPageNumber(), 1)

    def test_hypapdf_2pages(self):
        """
        Pdf generation class with more exactly 2 pages

        There is 42 elements in one page
        """
        bkng = ('firstname', 'name', '2', '3', '2', '4', '5', '40')
        datas = [bkng]
        inc = 0
        while inc < 41:
            datas.append(bkng)
            inc += 1
        pdf = utils.Hypapdf("title", "date", "place", datas)
        pdf.list_resa_pdf('/dev/null')
        self.assertEqual(len(pdf.datas), 42)
        self.assertEqual(pdf.pdf.getPageNumber(), 2)
