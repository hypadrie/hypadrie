# -*- coding: utf-8 -*-
#
# Copyright (c) 2012 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Unit tests for resa - Hypadrie

"""
from django.test import TestCase
from resa import forms


class BookingFormTests(TestCase):  # pylint: disable-msg=R0904
    """
    Booking objects class tests
    """
    def test_bookingform_womaxplaces(self):
        """
        Absolute URL
        """
        bform = forms.BookingForm()
        self.assertIsNone(bform.maxplaces)

    def test_bookingform_wimaxplaces(self):
        """
        Absolute URL
        """
        bform = forms.BookingForm(maxplaces=42)
        self.assertEqual(bform.maxplaces, 42)

    def test_bookingform_cleaned(self):
        """
        Booking form
        """
        token = '88c25fbe6ed4871bf9e8e83820a4e000'
        request_post = {'name': '', 'firstname': 'eee',
                        'nb_place_a': '3',
                        'nb_place_b': '3',
                        'nb_place_c': '3',
                        'nb_place_d': '3',
                        'nb_place_e': '3',
                        'csrfmiddlewaretoken': token, 'email': ''}
        place_list = ((0, 0), (1, 1), (2, 2), (3, 3), (4, 4))

        bform = forms.BookingForm(request_post, maxplaces=42)
        bform.fields['nb_place_a'].choices = place_list
        bform.fields['nb_place_b'].choices = place_list
        bform.fields['nb_place_c'].choices = place_list
        bform.fields['nb_place_d'].choices = place_list
        bform.fields['nb_place_e'].choices = place_list
        self.failUnless(bform.is_valid())
        self.failUnless('id_firstname' in bform.as_p())

    def test_bookingform_nomaxplaces(self):
        """
        Booking form is invalid, maxplaces are not set correctly
        """
        token = '88c25be6edf1271bf9e8e83820a4e001'
        request_post = {'name': 'ipsum', 'firstname': 'lorem',
                        'nb_place_a': '3',
                        'nb_place_b': '3',
                        'nb_place_c': '3',
                        'nb_place_d': '3',
                        'nb_place_e': '3',
                        'csrfmiddlewaretoken': token, 'email': ''}
        place_list = ((0, 0), (1, 1), (2, 2), (3, 3), (4, 4))

        bform = forms.BookingForm(request_post, maxplaces=None)
        bform.fields['nb_place_a'].choices = place_list
        bform.fields['nb_place_b'].choices = place_list
        bform.fields['nb_place_c'].choices = place_list
        bform.fields['nb_place_d'].choices = place_list
        bform.fields['nb_place_e'].choices = place_list
        self.failIf(bform.is_valid())

    def test_bookingform_toomanyplaces(self):
        """
        Booking form is invalid, too many places are requested
        """
        token = '88c25be6edf1271bf9e8e83820a4e001'
        request_post = {'name': 'ipsum', 'firstname': 'lorem',
                        'nb_place_a': '3',
                        'nb_place_b': '3',
                        'nb_place_c': '3',
                        'nb_place_d': '3',
                        'nb_place_e': '3',
                        'csrfmiddlewaretoken': token, 'email': ''}
        place_list = ((0, 0), (1, 1), (2, 2), (3, 3), (4, 4))

        bform = forms.BookingForm(request_post, maxplaces=4)
        bform.fields['nb_place_a'].choices = place_list
        bform.fields['nb_place_b'].choices = place_list
        bform.fields['nb_place_c'].choices = place_list
        bform.fields['nb_place_d'].choices = place_list
        bform.fields['nb_place_e'].choices = place_list
        self.failIf(bform.is_valid())

    def test_bookingform_invalidadult(self):
        """
        Booking form is invalide, to many places booked for adult
        """
        token = '88c25be6edf1271bf9e8e83820a4e001'
        request_post = {'name': 'ipsum', 'firstname': 'lorem',
                        'nb_place_child': '0',
                        'nb_place_adult': '9',
                        'csrfmiddlewaretoken': token, 'email': ''}
        place_list = ((0, 0), (1, 1), (2, 2), (3, 3), (4, 4))

        bform = forms.BookingForm(request_post, maxplaces='42')
        bform.fields['nb_place_a'].choices = place_list
        bform.fields['nb_place_b'].choices = place_list
        bform.fields['nb_place_c'].choices = place_list
        bform.fields['nb_place_d'].choices = place_list
        bform.fields['nb_place_e'].choices = place_list
        self.failIf(bform.is_valid())

    def test_bookingform_invalidzero(self):
        """
        Booking form is invalide, to many places booked for adult
        """
        token = '88c25be6edf1271bf9e8e83820a4e002'
        request_post = {'name': 'ipsum', 'firstname': 'lorem',
                        'nb_place_child': '0',
                        'nb_place_adult': '0',
                        'csrfmiddlewaretoken': token, 'email': ''}
        place_list = ((0, 0), (1, 1), (2, 2), (3, 3), (4, 4))

        bform = forms.BookingForm(request_post, maxplaces='42')
        bform.fields['nb_place_a'].choices = place_list
        bform.fields['nb_place_b'].choices = place_list
        bform.fields['nb_place_c'].choices = place_list
        bform.fields['nb_place_d'].choices = place_list
        bform.fields['nb_place_e'].choices = place_list
        self.failIf(bform.is_valid())
