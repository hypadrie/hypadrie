# -*- coding: utf-8 -*-
#
# Copyright (c) 2012 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Unit tests for resa - Hypadrie

"""
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.test import TestCase
from resa import forms


class FormsTests(TestCase):  # pylint: disable-msg=R0904
    """
    Load initial datas
    """
    def setUp(self):
        """
        Init
        """
        self.site = Site.objects.create(domain='sample_site')
        self.user = User.objects.create(username='admin_misc')

    def test_showform(self):
        """
        Create a new show

        Mandatory fields : title and company
        """
        fshow = forms.ShowForm({'title': 'ha7yhde4',
                                'company': 'hgbdc32e'})
        self.failUnless(fshow.is_valid())

    def test_showform_companymissing(self):
        """
        Create a new show

        Mandatory fields : title and company
        """
        fshow = forms.ShowForm({'title': 'ha7yhde4'})
        self.failIf(fshow.is_valid())
