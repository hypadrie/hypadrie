# -*- coding: utf-8 -*-  pylint: disable-msg=R0801
#
# Copyright (c) 2012 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Unit tests for resa - Hypadrie about full text indexing

"""
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.test import TestCase, Client
from resa.models import Booking
from resa.models import Place
from resa.models import Performance
from resa.models import Show


class SearchTests(TestCase):  # pylint: disable-msg=R0904
    """
    These tests concern full-text indexing

    """
    def setUp(self):
        """
        Init
        """
        self.site = Site.objects.create(domain='sample_site')
        settings.SITE_ID = self.site.id
        self.user = User.objects.create_user('admin_search',
                                             'admin_search@bar.com',
                                             'admintest')

    def test_showbydescription(self):
        """
        Lookup by querying data in description field
        """
        Show.objects.all().delete()

        show = Show.objects.create(title='Ibao2iem',
                                   company='ipsum',
                                   description='happy hacking',
                                   published=False,
                                   created_by=self.user)
        show.site.add(self.site.id)

        show_published = Show.objects.create(title='foo',
                                             company='foobar',
                                             description='happy hacking',
                                             published=True,
                                             created_by=self.user)
        show_published.site.add(self.site.id)

        client = Client()
        # lookup on description
        response = client.get('/search/', {'q': 'happy', })
        self.assertContains(response, show_published.title, status_code=200)
        self.assertNotContains(response, show.title, status_code=200)

    def test_showonanothersite(self):
        """
        Create a show on another SITE, it must not appears in results
        """
        Show.objects.all().delete()

        show = Show.objects.create(title='Ahth7ohgShaf6aih',
                                   company='ipsum',
                                   description='happy hacking',
                                   published=True,
                                   created_by=self.user)
        show.site.remove(self.site.id)
        show.site.add(self.site.id + 1)

        client = Client()
        # lookup on a word present in description
        response = client.get('/search/', {'q': 'happy', })
        self.assertNotContains(response, show.title, status_code=200)

    def test_showbytitle(self):
        """
        Lookup show by querying data in title field
        """
        Show.objects.all().delete()

        show = Show.objects.create(title='lorem',
                                   company='ipsum',
                                   description='lorem ipsum',
                                   published=False,
                                   created_by=self.user)
        show.site.add(self.site.id)

        showa = Show.objects.create(title='bravo',
                                    company='foobar',
                                    description='happy hacking',
                                    published=True,
                                    created_by=self.user)
        showa.site.add(self.site.id)

        showb = Show.objects.create(title='whisky bravo',
                                    company='foobar',
                                    description='happy hacking',
                                    published=True,
                                    created_by=self.user)
        showb.site.add(self.site.id)

        client = Client()
        response = client.get('/search/', {'q': 'bravo'})
        self.assertContains(response, showa.title, status_code=200)
        self.assertContains(response, showb.title, status_code=200)
        self.assertNotContains(response, 'foobar', status_code=200)

    def test_showbycompany(self):
        """
        Lookup show by querying data in company field
        """
        Show.objects.all().delete()

        show = Show.objects.create(title='lorem',
                                   company='ipsum',
                                   description='lorem ipsum',
                                   published=False,
                                   created_by=self.user)
        show.site.add(self.site.id)

        show_published = Show.objects.create(title='bravo',
                                             company='delta',
                                             description='happy hacking',
                                             published=True,
                                             created_by=self.user)
        show_published.site.add(self.site.id)

        client = Client()
        response = client.get('/search/', {'q': 'delta'})
        self.assertContains(response, 'hacking', status_code=200)
        self.assertNotContains(response, 'foobar', status_code=200)

    def test_bookingbyname(self):
        """
        Lookup booking by name field
        """
        tplace = Place.objects.create(title='foo_place_l')
        tshow = Show.objects.create(title='lorem',
                                    company='ipsum',
                                    description='lorem ipsum',
                                    published=True,
                                    created_by=self.user)
        tshow.site.add(self.site.id)

        tperf = Performance.objects.create(show=tshow,
                                           place=tplace,
                                           date='2012-06-06 23:20:20',
                                           nb_places_available=120,
                                           price_a=1,
                                           price_b=2,
                                           price_c=3,
                                           price_d=4,
                                           price_e=5)

        tbook = Booking.objects.create(performance=tperf,
                                       name='alpha',
                                       firstname='foxtrot',
                                       places_a=1,
                                       places_b=2,
                                       places_c=3,
                                       places_d=4,
                                       places_e=5)

        client = Client()
        response = client.get('/accounts/bookings/', {'q': 'delta'})
        client.login(username='admin_search', password='admintest')
        #self.assertContains(response, tbook.name, status_code=200)
        #self.assertNotContains(response, 'foobar', status_code=200)
        self.assertNotContains(response, 'foobar', status_code=404)
