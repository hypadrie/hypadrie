# -*- coding: utf-8 -*-  pylint: disable-msg=R0801
#
# Copyright (c) 2012 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Testcase for Show
"""
from django.test import TestCase
from django.contrib.auth.models import User
from hypadrie.resa.models import Show


class ShowTests(TestCase):  # pylint: disable-msg=R0904
    """
    Tests for ``Show`` objects.
    """
    def setUp(self):

        self.user = User.objects.create(username='sample_admin_show')

        self.sample_show = Show.objects.create(title='foo_show',
                                               company='foobar',
                                               created_by=self.user)

        self.sample_show_published = Show.objects.create(title='foo',
                                                         company='foobar',
                                                         published=True,
                                                         created_by=self.user)

    def test_new_show(self):
        """
        Test that a newly-created show is not published by default.
        """
        self.assertEqual(str(self.sample_show), 'foo_show')
        self.assertEqual(self.sample_show.title, 'foo_show')
        self.failIf(self.sample_show.published)

    def test_new_show_url(self):
        """
        Absolute URL
        """
        self.assertEqual(self.sample_show.get_absolute_url(), '/spectacle/1')
