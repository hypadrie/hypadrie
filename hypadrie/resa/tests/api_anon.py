# -*- coding: utf-8 -*-
#
# Copyright (c) 2012 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Unit tests for API in anonymous

"""
from django.test import TestCase
from django.test import Client
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from datetime import datetime
from hypadrie.resa.models import Show
from hypadrie.resa.models import Place
from hypadrie.resa.models import Booking
from hypadrie.resa.models import Performance


class ApiAnonTests(TestCase):  # pylint: disable-msg=R0904
    """
    Tests the API in anonymous
    """
    def setUp(self):
        """
        Init
        """
        self.site = Site.objects.create(domain='sample_site')
        self.user = User.objects.create(username='admin_bookings')

        self.place = Place.objects.create(title='foo_place_l')

        sample_show = Show.objects.create(title='foo_show',
                                          company='foobar',
                                          created_by=self.user)

        self.sample_perf_empty = Performance.objects.create(
            show=sample_show,
            place=self.place,
            date=datetime(2012, 6, 7, 20, 10, 2),
            nb_places_available=120,
            price_a=1,
            price_b=2,
            price_c=3,
            price_d=4,
            price_e=5)

        self.smp_perf_non_empty = Performance.objects.create(
            show=sample_show,
            place=self.place,
            date='2012-06-06 23:20:20',
            nb_places_available=120,
            price_a=1,
            price_b=2,
            price_c=3,
            price_d=4,
            price_e=5)

        self.smp_perf_full = Performance.objects.create(
            show=sample_show,
            place=self.place,
            date='2012-06-06 23:20:20',
            nb_places_available=18,
            price_a=1,
            price_b=2,
            price_c=3,
            price_d=4,
            price_e=5)

        Booking.objects.create(
            performance=self.smp_perf_non_empty,
            firstname='foobar',
            places_a=1,
            places_b=2,
            places_c=3,
            places_d=4,
            places_e=5)

        Booking.objects.create(
            performance=self.smp_perf_full,
            firstname='lorem 5 places',
            name='ipsum',
            places_a=0,
            places_b=2,
            places_c=3,
            places_d=0,
            places_e=0)

        Booking.objects.create(
            performance=self.smp_perf_full,
            firstname='foobar 13 places',
            places_a=10,
            places_b=0,
            places_c=1,
            places_d=1,
            places_e=1)

    def test_shows_anon(self):
        """
        Show list anonymous
        """
        Show.objects.create(title='alphabravo',
                            company='foobar',
                            published=True,
                            created_by=self.user)

        Show.objects.create(title='hidden',
                            company='foobar',
                            published=False,
                            created_by=self.user)

        client = Client()
        response = client.get('/api/v1/show/?format=json')
        self.assertContains(response, 'alphabravo', status_code=200)
        self.assertNotContains(response, 'hidden', status_code=200)

    def test_shows_hiddenfields(self):
        """
        Check taht hidden fields are not displayed
        """
        Show.objects.create(title='alpha bravo',
                            company='foobar',
                            published=False,
                            created_by=self.user)

        client = Client()
        response = client.get('/api/v1/show/?format=json')
        self.assertNotContains(response, '"events":', status_code=200)
        self.assertNotContains(response, '"published":', status_code=200)
        self.assertNotContains(response, '"template_file":', status_code=200)

    def test_performance_list(self):
        """
        Performance list with no filters
        """
        assertstring = 'EePahsh8'
        sample_show = Show.objects.create(title='foo_show',
                                          company='foobar',
                                          published=True,
                                          created_by=self.user)

        Performance.objects.create(
            show=sample_show,
            published=True,
            place=self.place,
            date=datetime(2012, 6, 7, 20, 10, 2),
            nb_places_available=120,
            information=assertstring,
            price_a=1,
            price_b=2,
            price_c=3,
            price_d=4,
            price_e=5)

        client = Client()
        response = client.get('/api/v1/performance/?format=json')
        self.assertContains(response, assertstring, status_code=200)

    def test_performance_notpublished(self):
        """
        Performance not published are not shown
        """
        assertstring = 'zaeK7aejahTh3eis6oh4iich'
        sample_show = Show.objects.create(title='foo_show',
                                          company='foobar',
                                          published=True,
                                          created_by=self.user)

        Performance.objects.create(
            show=sample_show,
            published=False,
            place=self.place,
            date=datetime(2012, 6, 7, 20, 10, 2),
            nb_places_available=120,
            information=assertstring,
            price_a=1,
            price_b=2,
            price_c=3,
            price_d=4,
            price_e=5)

        client = Client()
        response = client.get('/api/v1/performance/?format=json')
        self.assertNotContains(response, assertstring, status_code=200)

    def test_performance_showfilter(self):
        """
        Performance filtered on show
        """
        assertstringa = 'ohmoHupoish7hoopaJ2eekeo'
        assertstringb = 'Xai4Ju5ahXo1aaj4Tiul0uco'
        showa = Show.objects.create(title='foo_show',
                                    company='foobar',
                                    published=True,
                                    created_by=self.user)

        showb = Show.objects.create(title='foo_show',
                                    company='foobar',
                                    published=True,
                                    created_by=self.user)

        Performance.objects.create(
            show=showa,
            published=True,
            place=self.place,
            date=datetime(2012, 6, 7, 20, 10, 2),
            nb_places_available=120,
            information=assertstringa,
            price_a=1,
            price_b=2,
            price_c=3,
            price_d=4,
            price_e=5)

        Performance.objects.create(
            show=showb,
            published=True,
            place=self.place,
            date=datetime(2012, 6, 7, 20, 10, 2),
            nb_places_available=120,
            information=assertstringb,
            price_a=1,
            price_b=2,
            price_c=3,
            price_d=4,
            price_e=5)

        client = Client()
        url = '/api/v1/performance/?format=json&show=%s' % showa.id
        response = client.get(url)
        self.assertContains(response, assertstringa, status_code=200)
        self.assertNotContains(response, assertstringb, status_code=200)
