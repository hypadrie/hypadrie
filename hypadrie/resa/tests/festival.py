# -*- coding: utf-8 -*-
#
# Copyright (c) 2012 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Unit tests for resa - Hypadrie

"""
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.test import Client
from django.test import TestCase
from datetime import datetime
from resa.models import Festival
from resa.models import Place
from resa.models import Show
from resa.models import Performance
from resa.models import Booking


class FestivalTests(TestCase):  # pylint: disable-msg=R0904
    """
    Tests for non independants tests on the model-oriented ``Festival``.
    """
    def setUp(self):
        """
        Init
        """
        beg = datetime(2012, 6, 7, 20, 10, 2)
        end = datetime(2012, 8, 2, 20, 10, 2)
        self.user = User.objects.create(username='admin_festival')
        self.site = Site.objects.create(domain='sample_site')

        tplace = Place.objects.create(title='foo_place_l')

        self.afest = Festival.objects.create(title='foo_festival',
                                             published=True,
                                             date_begin=beg,
                                             date_end=end,
                                             description='Lorem ipsum sic',
                                             url='rthgbe3s3e2d')

        fshow = Show.objects.create(title='lorem',
                                    company='ipsum',
                                    description='lorem ipsum',
                                    published=True,
                                    created_by=self.user)
        fshow.site.add(self.site.id)

        eshow = Show.objects.create(title='lorem',
                                    company='ipsum',
                                    description='lorem ipsum',
                                    published=True,
                                    created_by=self.user)
        eshow.site.add(self.site.id)

        fperf = Performance.objects.create(show=fshow,
                                           place=tplace,
                                           date='2012-06-06 23:20:20',
                                           nb_places_available=120,
                                           price_a=1,
                                           price_b=2,
                                           price_c=3,
                                           price_d=4,
                                           price_e=5,
                                           festival=self.afest)

        eperf = Performance.objects.create(show=eshow,
                                           place=tplace,
                                           date='2012-06-06 23:20:20',
                                           nb_places_available=120,
                                           price_a=1,
                                           price_b=2,
                                           price_c=3,
                                           price_d=4,
                                           price_e=5,
                                           festival=self.afest)

        Booking.objects.create(performance=fperf,
                               name='alpha',
                               firstname='foxtrot',
                               places_a=1,
                               places_b=2,
                               places_c=0,
                               places_d=0,
                               places_e=0)

        Booking.objects.create(performance=eperf,
                               name='alpha',
                               firstname='foxtrot',
                               places_a=4,
                               places_b=0,
                               places_c=0,
                               places_d=0,
                               places_e=0)

    def test_festival_nbbooked(self):
        """
        Festival object

        Result :
         - sum of booked places for self.eperf and self.fperf
        """

        self.assertEqual(self.afest.nb_places_booked, 7)

    def test_festival(self):
        """
        Festival object
        """
        beg = datetime(2012, 6, 7, 20, 10, 2)
        end = datetime(2012, 8, 2, 20, 10, 2)
        s_fest = Festival.objects.create(title='foo_festival',
                                         published=False,
                                         date_begin=beg,
                                         date_end=end,
                                         description='Lorem ipsum sic')

        self.assertEqual(str(s_fest), 'foo_festival')

    def test_festival_absolute_url(self):
        """
        The Festival absolute URL
        """
        beg = datetime(2012, 6, 7, 20, 10, 2)
        end = datetime(2012, 8, 2, 20, 10, 2)
        s_festival = Festival.objects.create(title='foo_festival',
                                             published=False,
                                             url='festival_for_fun',
                                             date_begin=beg,
                                             date_end=end,
                                             description='Lorem ipsum sic')

        self.assertEqual(s_festival.get_absolute_url(), '/festival_for_fun')

    def test_festival_urlcall(self):
        """
        The Festival absolute URL
        """
        beg = datetime(2012, 6, 7, 20, 10, 2)
        end = datetime(2012, 8, 2, 20, 10, 2)
        fest = Festival.objects.create(title='foo_festival',
                                       published=True,
                                       url='festifoo',
                                       date_begin=beg,
                                       date_end=end,
                                       description='Lorem ipsum sic')
        fest.site.add(self.site.id)

        client = Client()
        response = client.get('/festifoo/')
        self.assertTemplateUsed(response, 'festival.html')
