# -*- coding: utf-8 -*-
#
# Copyright (c) 2012 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Unit tests for resa - Hypadrie

"""
from django.contrib.sites.models import Site
from django.test import TestCase
from django.conf import settings
from django.core.cache import cache
from resa import context_processors
from resa.models import Information


class ContextProcessorsTests(TestCase):  # pylint: disable-msg=R0904
    """
    Test the context processors
    """
    def setUp(self):
        """
        Init
        """
        self.site = Site.objects.create(domain='sample_site')
        settings.SITE_ID = self.site.id
        settings.CACHES = {
            'default': {
                'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
                'LOCATION': 'unique-snowflake'}}

    def test_piwik_set(self):
        """
        Piwik is set and right defined
        """
        settings.PIWIK_ID = 42
        settings.PIWIK_FQDN = 'www.foo.bar'
        attend = {'PIWIK_ID': 42, 'PIWIK_FQDN': 'www.foo.bar'}

        self.assertEqual(context_processors.piwik({}), attend)

    def test_piwik_nonset(self):
        """
        Piwik is not set
        """
        try:
            del settings.PIWIK_ID
        except:
            pass
        settings.PIWIK_FQDN = 'www.bar.foo'
        attend = {'PIWIK_ID': None, 'PIWIK_FQDN': None}
        self.assertEqual(context_processors.piwik({}), attend)

    def test_piwik_demoset(self):
        """
        Demo mode is activated in settings
        """
        settings.DEMO = True
        attend = {'DEMO': True}
        self.assertEqual(context_processors.demo({}), attend)

    def test_infobox_none(self):
        """
        Infobox
        """
        self.assertEqual(settings.CACHES['default']['BACKEND'],
                         'django.core.cache.backends.locmem.LocMemCache')

        attend = {'info_title': None,
                  'info_information': None,
                  'info_url': None}
        self.assertEqual(context_processors.infobox({}), attend)

    def test_infobox_nocache(self):
        """
        Infobox with no cache defined
        """

        settings.CACHES = None

        attend = {'info_title': None,
                  'info_information': None,
                  'info_url': None}
        self.assertEqual(context_processors.infobox({}), attend)

    def test_infobox_wmemcached(self):
        """
        Infobox with cache defined on memcached
        """
        memcache = 'django.core.cache.backends.memcached.MemcachedCache'
        settings.CACHES = {
            'default': {
                'BACKEND': memcache,
                'LOCATION': '127.0.0.1:11211',
                'KEY_PREFIX': 'hpdr'}}

        Information.objects.all().delete()
        cache.clear()
        info = Information.objects.create(title='footitle',
                                          information='lorem ipsum',
                                          url='http://www.foo.bar',
                                          published=True)
        info.site.add(self.site.id)

        attend = {'info_title': 'footitle',
                  'info_information': 'lorem ipsum',
                  'info_url': 'http://www.foo.bar'}

        self.assertEqual(context_processors.infobox({}), attend)

    def test_infobox_badcache(self):
        """
        Infobox with cache defined on a bad host
        """
        memcache = 'django.core.cache.backends.memcached.MemcachedCache'
        settings.CACHES = {
            'default': {
                'BACKEND': memcache,
                'LOCATION': '127.0.0.42:4242',
                'KEY_PREFIX': 'hpdr'}}

        attend = {'info_title': None,
                  'info_information': None,
                  'info_url': None}
        self.assertEqual(context_processors.infobox({}), attend)

    def test_infobox_woinformation(self):
        """
        Test infobox without any informations in database
        Return a dict with None values
        """
        Information.objects.all().delete()
        cache.clear()
        attend = {'info_title': None,
                  'info_information': None,
                  'info_url': None}
        result = context_processors.infobox({})
        self.failUnless(type(result) is dict)
        self.assertEqual(result, attend)

    def test_cache(self):
        """
        Test infobox without any informations in database
        Return a dict with None values
        """
        cache.clear()
        cache_content = cache.get('foo')
        self.assertEqual(cache_content, None)
