# -*- coding: utf-8 -*-
#
# Copyright (c) 2012 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Unit tests for resa - Hypadrie

"""
from django.conf import settings
from django.core.cache import cache
from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.test import TestCase, Client
from datetime import datetime
from resa import admin
from resa import forms
from resa import utils
from resa.models import Booking, Information
from resa.models import Performance, Place, Show


class MiscTests(TestCase):  # pylint: disable-msg=R0904
    """
    Load initial datas
    """
    def setUp(self):
        """
        Init
        """
        self.site = Site.objects.create(domain='sample_site')
        self.user = User.objects.create(username='admin_misc')

        self.sample_place_located = Place.objects.create(title='foo_place_l')

        self.sample_show = Show.objects.create(title='foo_show',
                                               company='foobar',
                                               created_by=self.user)
        self.sample_show.site.add(self.site.id)

        self.sample_perf_empty = Performance.objects.create(
            show=self.sample_show,
            place=self.sample_place_located,
            date=datetime(2012, 6, 7, 20, 10, 2),
            nb_places_available=120,
            price_a=1,
            price_b=2,
            price_c=3,
            price_d=4,
            price_e=5)

        self.smp_perf_non_empty = Performance.objects.create(
            show=self.sample_show,
            place=self.sample_place_located,
            date='2012-06-06 23:20:20',
            nb_places_available=120,
            price_a=1,
            price_b=2,
            price_c=3,
            price_d=4,
            price_e=5)

        self.smp_perf_full = Performance.objects.create(
            show=self.sample_show,
            place=self.sample_place_located,
            date='2012-06-06 23:20:20',
            nb_places_available=18,
            price_a=1,
            price_b=2,
            price_c=3,
            price_d=4,
            price_e=5)

        Booking.objects.create(
            performance=self.smp_perf_non_empty,
            firstname='foobar',
            places_a=1,
            places_b=2,
            places_c=3,
            places_d=4,
            places_e=5)

        Booking.objects.create(
            performance=self.smp_perf_full,
            firstname='lorem 5 places',
            name='ipsum',
            places_a=0,
            places_b=2,
            places_c=3,
            places_d=0,
            places_e=0)

        Booking.objects.create(
            performance=self.smp_perf_full,
            firstname='foobar 13 places',
            places_a=10,
            places_b=0,
            places_c=1,
            places_d=1,
            places_e=1)

        settings.CACHES = {
            'default': {
                'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
                'LOCATION': 'unique-snowflake'}}


class ResaTests(MiscTests):  # pylint: disable-msg=R0904
    """
    Tests for resa
    """
    def test_performance_empty_prices(self):
        """
        Test a performance with no booking

        """
        p_empty = Performance.objects.create(
            show=self.sample_show,
            place=self.sample_place_located,
            date=datetime(2012, 6, 7, 20, 10, 2),
            nb_places_available=120,
            price_a=0.0,
            price_c=0.0)

        self.assertEqual(p_empty.price_c_booked, 0)
        self.assertEqual(p_empty.price_a_booked, 0)
        self.assertEqual(p_empty.price_booked, 0)

    def test_performance_full_prices(self):
        """
        Test a performance without any free places

        """
        self.assertEqual(self.sample_show.nb_perf, 3)

    def test_shownb(self):
        """
        Test a performance without any free places

        """
        self.assertEqual(self.smp_perf_full.nb_places_a_booked, 10)
        self.assertEqual(self.smp_perf_full.nb_places_b_booked, 2)
        self.assertEqual(self.smp_perf_full.nb_places_c_booked, 4)
        self.assertEqual(self.smp_perf_full.nb_places_d_booked, 1)
        self.assertEqual(self.smp_perf_full.nb_places_e_booked, 1)
        self.assertEqual(self.smp_perf_full.nb_places_booked, 18)
        self.failUnless(self.smp_perf_full.is_complete())

    def test_performance_full(self):
        """
        Test a performance without any free places

        """
        self.assertEqual(self.smp_perf_full.nb_places_a_booked, 10)
        self.assertEqual(self.smp_perf_full.nb_places_b_booked, 2)
        self.assertEqual(self.smp_perf_full.nb_places_c_booked, 4)
        self.assertEqual(self.smp_perf_full.nb_places_d_booked, 1)
        self.assertEqual(self.smp_perf_full.nb_places_e_booked, 1)
        self.assertEqual(self.smp_perf_full.nb_places_booked, 18)
        self.failUnless(self.smp_perf_full.is_complete())

    def test_performance_nbplacesbooked(self):
        """
        Test a performance without any free places

        """
        self.assertEqual(self.smp_perf_full.nb_places_booked, 18)

    def test_performance_non_empty(self):
        """
        Test that a newly-created user is inactive.

        """
        self.failIf(self.smp_perf_non_empty.is_complete())
        self.assertNotEqual(self.smp_perf_non_empty.nb_places_a_booked, 0)
        self.assertNotEqual(self.smp_perf_non_empty.nb_places_c_booked, 0)

    def test_new_performance_empty(self):
        """
        Test that a newly-created user is inactive.

        """
        self.failIf(self.sample_perf_empty.is_complete())
        self.assertEqual(self.sample_perf_empty.nb_places_a_booked, 0)
        self.assertEqual(self.sample_perf_empty.nb_places_c_booked, 0)
        self.assertEqual(self.sample_perf_empty.nb_places_dispo, 120)


class PerformanceTests(MiscTests):  # pylint: disable-msg=R0904
    """
    Tests for non independants tests on the model-oriented ``Performance``.
    """
    def test_new_performance(self):
        """
        The Performance object creation
        """
        self.assertEqual(str(self.sample_perf_empty),
                         '07/06/12 20:10 - foo_show - foo_place_l')

    def test_new_performance_dates(self):
        """
        Performance dates
        """
        self.assertEqual(self.sample_perf_empty._format_date(),
                         'Thursday 07 Jun 2012 20:10')
        self.assertEqual(self.sample_perf_empty._format_date_long(),
                         'Thursday 07 Jun 2012 20:10')

    def test_new_performance_url(self):
        """
        Performance Absolute URL
        """
        self.assertEqual(self.sample_perf_empty.get_absolute_url(),
                         '/representation/1')
        self.assertEqual(self.smp_perf_non_empty.get_absolute_url(),
                         '/representation/2')


class InformationTests(MiscTests):  # pylint: disable-msg=R0904
    """
    Tests for ``Information`` objects.

    """
    def test_information(self):
        """
        Test that a newly-created is not published by default.
        """
        info = Information.objects.create(title='foo',
                                          information='lorem ipsum')

        self.assertEqual(str(info), 'foo')
        self.assertEqual(info.title, 'foo')
        self.failIf(info.published)

    def test_information_random(self):
        """
        roll-up information
        """
        Information.objects.all().delete()
        cache.clear()
        info1 = Information.objects.create(title='info1',
                                           information='lorem ipsum',
                                           published=True)
        info1.site.add(self.site.id)

        info2 = Information.objects.create(title='info1',
                                           information='lorem ipsum',
                                           published=True)
        info2.site.add(self.site.id)

        client = Client()
        response = client.get('/')
        self.assertContains(response, 'info1', status_code=200)

    def test_information_nodatas(self):
        """
        roll-up information
        """
        Information.objects.all().delete()

        client = Client()
        response = client.get('/')
        self.assertContains(response, 'None', status_code=200)


class MessagesTests(MiscTests):  # pylint: disable-msg=R0904
    """
    Tests for the class message
    """
    def test_messages_admin(self):
        """
        Test that a newly-created is not published by default.
        """
        msg = utils.Messages()
        self.failUnless(len(msg.mail_admin()) > 1)

    def test_messages_public(self):
        """
        Test that a newly-created is not published by default.
        """
        msg = utils.Messages()
        self.failUnless(len(msg.mail_public()) > 1)

    def test_messages_plural(self):
        """
        Test the plural function for french plurals
        """
        self.assertEqual(utils.plural(1, 'hour'), 'hour')
        self.assertEqual(utils.plural(2, 'hour'), 'hours')


class LonLatFormTests(MiscTests):  # pylint: disable-msg=R0904
    """
    Check the geographical coordinate form
    """
    def test_lonlatform(self):
        """
        Absolute URL
        """
        lonlatform = forms.LonLatForm({'lon': '21', 'lat': '42'})
        self.failUnless(lonlatform.is_valid())

    def test_lonlatform_wrongcoord(self):
        """
        Wrong values for LonLat
        """
        self.failIf(forms.LonLatForm({'lon': '2.1',  'lat': '420'}).is_valid())
        self.failIf(forms.LonLatForm({'lon': '210', 'lat': '42'}).is_valid())
        self.failIf(forms.LonLatForm({'lon': '210', 'lat': '420'}).is_valid())
        self.failIf(forms.LonLatForm({'lon': '-210', 'lat': '4'}).is_valid())
        self.failIf(forms.LonLatForm({'lon': '-2',  'lat': '-181'}).is_valid())


class AdminTests(MiscTests):  # pylint: disable-msg=R0904
    """
    django administration
    """
    def test_make_published(self):
        """
        Publish selected elements
        """
        show = Show.objects.create(title='3804cced1484e6c68e3c172bdb128',
                                   company='foobar',
                                   created_by=self.user,
                                   published=False)
        show.site.add(self.site.id)

        qry = Show.objects.filter(title='3804cced1484e6c68e3c172bdb128')

        self.assertEqual(qry[0].published, False)
        admin.make_published(None, None, qry)
        self.assertEqual(qry[0].published, True)
        admin.make_unpublished(None, None, qry)
        self.assertEqual(qry[0].published, False)
