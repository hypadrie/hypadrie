from resa.tests.api_anon import ApiAnonTests
from resa.tests.api_auth import ApiAuthTests
from resa.tests.booking import BookingTests
from resa.tests.booking_forms import BookingFormTests
from resa.tests.festival import FestivalTests
from resa.tests.forms import FormsTests
from resa.tests.context_processors import ContextProcessorsTests
from resa.tests.misc import ResaTests
from resa.tests.misc import MiscTests
from resa.tests.misc import PerformanceTests
from resa.tests.misc import InformationTests
from resa.tests.misc import MessagesTests
from resa.tests.misc import LonLatFormTests
from resa.tests.misc import AdminTests
from resa.tests.performance_admin import PerformanceAdminTests
from resa.tests.place import PlaceTests
from resa.tests.search import SearchTests
from resa.tests.show import ShowTests
from resa.tests.utils import UtilsTests
from resa.tests.views import ViewsTests
from resa.tests.profile import ProfileTests
