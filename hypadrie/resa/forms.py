# -*- coding: utf-8 -*-
#
# Copyright (c) 2012 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from django import forms
from django.forms.widgets import Select, TextInput, Textarea
from django.forms.widgets import CheckboxInput


class HelpForm(forms.Form):  # pylint: disable=R0924

    email = forms.EmailField(required=False,
                             max_length=150,
                             widget=TextInput(attrs={'class': 'input-xlarge'})
                             )


class LonLatForm(forms.Form):  # pylint: disable=R0924

    lon = forms.FloatField(required=True,
                           max_value=180.0,
                           min_value=-180.0)

    lat = forms.FloatField(required=True,
                           max_value=180.0,
                           min_value=-180.0)


class ShowForm(forms.Form):  # pylint: disable=R0924

    title = forms.CharField(
        required=True,
        max_length=120,
        widget=TextInput(attrs={'class': 'input-xlarge focused'}))

    company = forms.CharField(
        required=True,
        max_length=200,
        widget=TextInput(attrs={'class': '.span4'}))

    description = forms.CharField(
        required=False,
        widget=Textarea())

    published = forms.BooleanField(
        required=False,
        widget=CheckboxInput())

    contact_name = forms.CharField(
        required=False,
        max_length=200,
        widget=TextInput(attrs={'class': 'input-large'}))

    contact_tel = forms.CharField(
        required=False,
        max_length=200,
        widget=TextInput(attrs={'class': 'input-medium'}))

    contact_email = forms.EmailField(
        required=False,
        max_length=75,
        widget=TextInput(attrs={'class': 'input-xlarge'}))

    show_url = forms.URLField(
        required=False,
        widget=TextInput(attrs={'class': 'input-xlarge'}))

    company_url = forms.URLField(
        required=False,
        widget=TextInput(attrs={'class': 'input-xlarge'}))

    def clean(self):
        cleaned_data = self.cleaned_data
        return cleaned_data


class BookingForm(forms.Form):  # pylint: disable=R0924

    def __init__(self, *args, **kwargs):
        self.maxplaces = kwargs.pop('maxplaces', None)
        super(BookingForm, self).__init__(*args, **kwargs)

    firstname = forms.CharField(
        required=True,
        max_length=25,
        widget=TextInput(attrs={'onchange': 'activate_or_not()',
                                'class': 'input-large focused'}))

    name = forms.CharField(
        required=False,
        max_length=40,
        widget=TextInput(attrs={'onchange': 'activate_or_not()',
                                'class': 'input-large'}))

    email = forms.EmailField(
        required=False,
        max_length=150,
        widget=TextInput(attrs={'onchange': 'activate_or_not()',
                                'class': 'input-xlarge'}))

    nb_place_a = forms.IntegerField()
    nb_place_b = forms.IntegerField()
    nb_place_c = forms.IntegerField()
    nb_place_d = forms.IntegerField()
    nb_place_e = forms.IntegerField()

    nb_place_a = forms.ChoiceField(
        required=True,
        widget=Select(attrs={'onchange': 'activate_or_not()',
                             'class': 'span1'}))
    nb_place_b = forms.ChoiceField(
        required=True,
        widget=Select(attrs={'onchange': 'activate_or_not()',
                             'class': 'span1'}))
    nb_place_c = forms.ChoiceField(
        required=True,
        widget=Select(attrs={'onchange': 'activate_or_not()',
                             'class': 'span1'}))
    nb_place_d = forms.ChoiceField(
        required=True,
        widget=Select(attrs={'onchange': 'activate_or_not()',
                             'class': 'span1'}))
    nb_place_e = forms.ChoiceField(
        required=True,
        widget=Select(attrs={'onchange': 'activate_or_not()',
                             'class': 'span1'}))

    def clean(self):
        cleaned_data = self.cleaned_data

        try:
            nb_a = cleaned_data['nb_place_a']
            nb_b = cleaned_data['nb_place_b']
            nb_c = cleaned_data['nb_place_c']
            nb_d = cleaned_data['nb_place_d']
            nb_e = cleaned_data['nb_place_e']
        except KeyError:
            nb_a = '0'
            nb_b = '0'
            nb_c = '0'
            nb_d = '0'
            nb_e = '0'

        total = int(nb_a) + int(nb_b) + int(nb_c) + int(nb_d) + int(nb_e)

        msg_tot0 = """
Aucune place réservée, indiquez le nb de place que vous souhaitez réserver.
"""

        text = """
Pas assez de place disponible, il ne reste que %s places,
vous en avez demandé %s.
"""

        if self.maxplaces is None:
            msg = "Impossible d'enregistrer votre réservation"
            raise forms.ValidationError(msg)
        else:
            if (total > int(self.maxplaces)):
                msg = text % (self.maxplaces, total)
                raise forms.ValidationError(msg)
            if (total == 0):
                raise forms.ValidationError(msg_tot0)

        return cleaned_data
