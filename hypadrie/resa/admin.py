# -*- coding: utf-8 -*-
"""
 Copyright (c) 2012 Rodolphe Quiédeville <rodolphe@quiedeville.org>

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from resa.models import Booking
from resa.models import Festival
from resa.models import Information
from resa.models import Performance
from resa.models import Place
from resa.models import Show
from resa.admins_performance import PerformanceAdmin
from resa.admins_place import PlaceAdmin
from django.contrib import admin


def make_published(modeladmin, request, queryset):
    """
    Published an entry, make it available on public parts
    """
    queryset.update(published=True)


def make_unpublished(modeladmin, request, queryset):
    """
    Unpublished an entry, hide it on public parts
    """
    queryset.update(published=False)

make_published.short_description = u'Publier les entrées sélectionnées'
make_unpublished.short_description = u'Dé-Publier les entrées sélectionnées'


class ShowAdmin(admin.ModelAdmin):
    """
    Custom Admin for show
    """
    list_display = ('title', 'company', 'published')
    list_filter = ['company', 'published']
    actions = [make_published, make_unpublished]


class InformationAdmin(admin.ModelAdmin):
    """
    Custom admin class for information
    """
    list_display = ('title', 'published')
    search_fields = ['title']
    list_filter = ['published']
    actions = [make_published, make_unpublished]


class FestivalAdmin(admin.ModelAdmin):
    """
    Custom admin for festival
    """
    list_display = ('title', 'published', 'date_begin')
    list_filter = ['published']
    ordering = ['-date_begin']
    actions = [make_published, make_unpublished]


class BookingAdmin(admin.ModelAdmin):
    """
    Custom admin class for booking
    """
    description = 'Les reservations'

    list_display = ('performance', 'name', 'firstname',
                    'places_a', 'places_b', 'places_c', 'places_d', 'places_e',
                    'email')

    list_filter = ['performance']
    search_fields = ['name', 'firstname', 'email']
    readonly_fields = ['uuid']

admin.site.register(Festival, FestivalAdmin)
admin.site.register(Show, ShowAdmin)
admin.site.register(Place, PlaceAdmin)
admin.site.register(Performance, PerformanceAdmin)
admin.site.register(Booking, BookingAdmin)
admin.site.register(Information, InformationAdmin)
