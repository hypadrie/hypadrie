# -*- coding: utf-8 -*-
#
# Copyright (c) 2012 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Fulltext indexing with haystack
"""
from django.conf import settings
from haystack import indexes
from resa.models import Show
from resa.models import Booking


class ShowIndex(indexes.RealTimeSearchIndex, indexes.Indexable):
    """
    Fulltext indexing for objects Show
    """
    text = indexes.CharField(document=True, use_template=True)
    title = indexes.CharField(model_attr='title')
    family = indexes.CharField(model_attr='family',null=True)
    events = indexes.BooleanField(model_attr='events',default='false')

    def get_model(self):
        return Show

    def index_queryset(self):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.filter(
            published=True,
            site__in=[settings.SITE_ID]).order_by("-pk")


class BookingIndex(indexes.RealTimeSearchIndex, indexes.Indexable):
    """
    Fulltext indexing for objects Booking
    """
    text = indexes.CharField(document=True, use_template=True)
    name = indexes.CharField(model_attr='name')
    firstname = indexes.CharField(model_attr='firstname')

    def get_model(self):
        return Booking

    def index_queryset(self):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.filter(
            performance__show__site__in=[settings.SITE_ID]).order_by("-pk")
