# -*- coding: utf-8 -*-  pylint: disable-msg=R0801
#
# Copyright (c) 2012 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
API for hypadrie
"""

from tastypie.resources import ModelResource, ALL_WITH_RELATIONS
from tastypie import fields
from resa.models import Booking
from resa.models import Performance
from resa.models import Show
from tastypie.throttle import CacheThrottle


class ShowResource(ModelResource):  # pylint: disable-msg=R0904
    class Meta:
        list_allowed_methods = ['get']
        queryset = Show.objects.filter(
            published=True,
            events=False).order_by('-pk')
        excludes = ['events', 'published', 'template_file']
        # limit to 100 req per hour
        throttle = CacheThrottle(throttle_at=100, timeframe=3600)


class PerformanceResource(ModelResource):  # pylint: disable-msg=R0904
    show = fields.ForeignKey(ShowResource, 'show')

    class Meta:
        queryset = Performance.objects.filter(published=True).order_by('-pk')
        list_allowed_methods = ['get']
        excludes = ['published']
        # limit to 100 req per hour
        throttle = CacheThrottle(throttle_at=100, timeframe=3600)

        filtering = {"show": ALL_WITH_RELATIONS}


class EventResource(ModelResource):  # pylint: disable-msg=R0904
    # An event is a special performance/show
    show = fields.ForeignKey(ShowResource, 'show')

    class Meta:
        queryset = Performance.objects.filter(published=True).order_by('-pk')
        excludes = ['published']
        # limit to 100 req per hour
        throttle = CacheThrottle(throttle_at=100, timeframe=3600)


class BookingResource(ModelResource):  # pylint: disable-msg=R0904
    performance = fields.ForeignKey(PerformanceResource, 'performance')

    class Meta:
        queryset = Booking.objects.filter(
            performance__published=True,
            performance__open_booking=True).order_by('-pk')
        fields = ['firstname', 'name' 'confirmed', 'booking_date']
        max_limit = 100
        # limit to 100 req per hour
        throttle = CacheThrottle(throttle_at=100, timeframe=3600)
        filtering = {'performance': ALL_WITH_RELATIONS}
