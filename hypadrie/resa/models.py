# -*- coding: utf-8 -*-  pylint: disable-msg=R0801
#
# Copyright (c) 2012 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
The models describe all the object manipulate by resa
"""

from django.db import models
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.core.mail import send_mail
from django.db.models import Sum, Count
from django.utils.translation import ugettext_lazy as _
from uuid import uuid4
from datetime import datetime
from hypadrie.resa.utils import Messages
from tastypie.models import create_api_key


class Information(models.Model):
    """
    Model for ``Information message``.

    An information message is a message displayed on some pages

    """
    title = models.CharField(max_length=120, verbose_name='titre', blank=True)
    information = models.TextField(verbose_name='texte', blank=True)
    published = models.BooleanField(verbose_name='publié en ligne')
    url = models.URLField(verbose_name=u'''url pour plus d'infos''',
                          blank=True, null=True)
    site = models.ManyToManyField(Site)

    def __unicode__(self):
        """
        The unicode method
        """
        return self.title


class Show(models.Model):
    """
    Model for ``Shows``.

    A show is defined by

    """
    title = models.CharField(max_length=120, verbose_name='titre')
    company = models.CharField(max_length=200, verbose_name='compagnie')
    # hack: whoosh is bugged in filtering on boolean=field
    family = models.CharField(max_length=200, blank=True, null=True)
    company.help_text = 'Nom de la compagnie (requis)'
    published = models.BooleanField(verbose_name='publié en ligne')
    events = models.BooleanField(verbose_name='Evènement')
    contact_name = models.CharField(max_length=200, blank=True)
    contact_tel = models.CharField(max_length=200, blank=True)
    contact_email = models.EmailField(max_length=75, blank=True)
    description = models.TextField(blank=True)
    company_url = models.URLField(verbose_name='site web de la compagnie',
                                  blank=True)
    show_url = models.URLField(verbose_name='site web du spectacle',
                               blank=True)
    owner = models.ManyToManyField(User,
                                   related_name='owner',
                                   verbose_name='propriétaires')
    created_by = models.ForeignKey(User,
                                   related_name='created_by')
    site = models.ManyToManyField(Site)
    template_file = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        """
        Meta class
        """
        verbose_name = 'spectacle'
        verbose_name_plural = 'spectacles'

    def __unicode__(self):
        """
        The unicode method
        """
        return self.title

    def get_absolute_url(self):
        """
        Return the absolute url
        """
        return "/spectacle/%s" % self.pk

    @property
    def nb_perf(self):
        """
        Total place booked
        """
        nb_perf = Booking.objects.filter(
            performance__show__pk=self.id).aggregate(Count('pk'))
        return int(nb_perf['pk__count'])

    @property
    def nb_places_booked(self):
        """
        Total place booked
        """
        pbooked = Booking.objects.filter(
            performance__show__pk=self.id).aggregate(Sum('places_a'),
                                                     Sum('places_b'),
                                                     Sum('places_c'),
                                                     Sum('places_d'),
                                                     Sum('places_e'))

        try:
            tpa = int(pbooked['places_a__sum'])
        except:
            tpa = 0

        try:
            tpb = int(pbooked['places_b__sum'])
        except:
            tpb = 0

        try:
            tpc = int(pbooked['places_c__sum'])
        except:
            tpc = 0

        try:
            tpd = int(pbooked['places_d__sum'])
        except:
            tpd = 0

        try:
            tpe = int(pbooked['places_e__sum'])
        except:
            tpe = 0

        return tpa + tpb + tpc + tpd + tpe


class Place(models.Model):
    """
    Model for ``Place``.

    A place is defined as where a show could be shown.

    """
    title = models.CharField(max_length=200, verbose_name=_('name'))
    address = models.CharField(max_length=200, verbose_name='adresse',
                               null=True,
                               blank=True)
    town = models.CharField(max_length=200,
                            verbose_name='ville',
                            blank=True)
    title.help_text = 'Ce nom est visible pour les internautes'
    lon = models.FloatField(null=True)
    lat = models.FloatField(null=True)
    owner = models.ManyToManyField(User, verbose_name='propriétaires')

    class Meta:
        """
        Meta class
        """
        verbose_name = 'salle de spectacle'
        verbose_name_plural = 'salles de spectacle'

    def __unicode__(self):
        """
        The unicode method
        """
        return self.title

    def get_absolute_url(self):
        """
        Return the absolute url
        """
        return "/place/%s" % self.pk


class Festival(models.Model):
    """
    Model for ``Festival``.

    A place is defined as where a show could be shown.
    """

    title = models.CharField(max_length=200, verbose_name='nom')
    url = models.SlugField(max_length=50, unique=True)
    published = models.BooleanField(verbose_name='publié en ligne')
    date_begin = models.DateField(verbose_name='Date de début')
    date_end = models.DateField(verbose_name=_('End date'))
    description = models.TextField(blank=True)
    external_url = models.URLField(verbose_name=u'''url externe''',
                                   blank=True, null=True)
    media_url = models.URLField(verbose_name=u'''url de media''',
                                blank=True, null=True)

    media_url.help_text = '''Url d'une affiche, flyer, ...'''
    external_url.help_text = '''Url du site officiel du festival'''
    title.help_text = 'Titre public du festival'
    url.help_text = 'Ce nom est unique pour tout le site'
    owner = models.ManyToManyField(User, verbose_name='propriétaires')
    site = models.ManyToManyField(Site)

    def __unicode__(self):
        """
        The unicode method
        """
        return self.title

    def get_absolute_url(self):
        """
        Return the absolute url
        """
        return "/%s" % self.url

    @property
    def nb_places_booked(self):
        """
        Total place booked
        """
        pbooked = Booking.objects.filter(
            performance__festival__pk=self.id).aggregate(Sum('places_a'),
                                                         Sum('places_b'),
                                                         Sum('places_c'),
                                                         Sum('places_d'),
                                                         Sum('places_e'))

        try:
            total = pbooked['places_a__sum'] + pbooked['places_b__sum'] + pbooked['places_c__sum'] + pbooked['places_d__sum'] + pbooked['places_e__sum']
        except TypeError:
            total = 0
        return total


class Performance(models.Model):
    """
    A performance is a show play
    """
    show = models.ForeignKey(Show, verbose_name='spectacle')
    place = models.ForeignKey(Place, verbose_name='salle')
    date = models.DateTimeField()
    published = models.BooleanField(verbose_name='publié en ligne')
    open_booking = models.BooleanField(verbose_name='résa ouverte')
    require_confirmation = models.BooleanField(
        verbose_name='confirmation par email requise')
    public_booking = models.BooleanField(
        verbose_name='afficher les réservations en ligne')
    open_booking_from = models.DateTimeField(null=True, blank=True)
    open_booking_until = models.DateTimeField(null=True, blank=True)
    max_bookable_places = models.PositiveIntegerField(
        verbose_name='nb place réservable',
        null=True,
        default=10)
    nb_places_available = models.PositiveIntegerField(
        verbose_name='nb place')
    nb_places_onspot = models.PositiveIntegerField(
        verbose_name='nb place sur site',
        default=0)
    price_a = models.FloatField(verbose_name='prix A')
    price_b = models.FloatField(verbose_name='prix B', null=True, blank=True)
    price_c = models.FloatField(verbose_name='prix C', null=True, blank=True)
    price_d = models.FloatField(verbose_name='prix D', null=True, blank=True)
    price_e = models.FloatField(verbose_name='prix E', null=True, blank=True)

    label_a = models.CharField(max_length=40, verbose_name='descriptif prix A')
    label_b = models.CharField(max_length=40,
                               verbose_name='descriptif prix B',
                               null=True,
                               blank=True)
    label_c = models.CharField(max_length=40,
                               verbose_name='descriptif prix C',
                               null=True,
                               blank=True)
    label_d = models.CharField(max_length=40,
                               verbose_name='descriptif prix D',
                               null=True,
                               blank=True)
    label_e = models.CharField(max_length=40,
                               verbose_name='descriptif prix E',
                               null=True,
                               blank=True)
    information = models.TextField(
        verbose_name='information',
        blank=True)
    festival = models.ForeignKey(
        Festival,
        verbose_name='festival',
        blank=True,
        null=True)
    bkng_send_email = models.BooleanField(
        verbose_name='envoi des emails')
    bkng_contact_email = models.EmailField(max_length=75,
                                           blank=True,
                                           verbose_name='email',
                                           null=True)

    nb_places_available.help_text = '''
Nombre de places disponibles à la réservation sur internet
'''
    nb_places_onspot.help_text = '''
Nombre de places en vente sur place
'''
    festival.help_text = '''
A renseigner si la représention à lieu au sein d''un festival
'''
    bkng_send_email.help_text = '''
Envoyer un email lors d'une nouvelle réservation
'''

    class Meta:
        """
        Meta class
        """
        ordering = ('date',)
        verbose_name = 'représentation'
        verbose_name_plural = 'représentations'

    def __unicode__(self):
        """
        The unicode method
        """
        uncd = '%s - %s - %s' % (self.date.strftime('%d/%m/%y %H:%M'),
                                 self.show.title,
                                 self.place.title)
        return uncd

    def get_absolute_url(self):
        """
        Return the absolute url
        """
        return "/representation/%s" % self.pk

    def _format_date(self):
        """
        Format the date on shot form
        """
        return self.date.strftime('%A %d %b %Y %H:%M')

    def _format_date_long(self):
        """
        Format the date on long form
        """
        return self.date.strftime('%A %d %b %Y %H:%M')

    def _nb_places_booked(self):
        """
        Total place booked
        """
        pbooked = Booking.objects.filter(
            performance=self.id).aggregate(Sum('places_a'),
                                           Sum('places_b'),
                                           Sum('places_c'),
                                           Sum('places_d'),
                                           Sum('places_e'))
        try:
            tpa = int(pbooked['places_a__sum'])
        except:
            tpa = 0

        try:
            tpb = int(pbooked['places_b__sum'])
        except:
            tpb = 0

        try:
            tpc = int(pbooked['places_c__sum'])
        except:
            tpc = 0

        try:
            tpd = int(pbooked['places_d__sum'])
        except:
            tpd = 0

        try:
            tpe = int(pbooked['places_e__sum'])
        except:
            tpe = 0

        total = tpa + tpb + tpc + tpd + tpe

        return total

    @property
    def booking_is_open(self):

        open_from = True
        open_until = True

        if self.open_booking_from is not None:
            if self.open_booking_from > datetime.now():
                open_from = False

        if self.open_booking_until is not None:
            if self.open_booking_until < datetime.now():
                open_until = False

        return self.open_booking and open_from and open_until

    @property
    def price_a_booked(self):
        try:
            total = self.nb_places_a_booked * self.price_a
        except TypeError:
            total = 0
        return float(total)

    @property
    def price_b_booked(self):
        try:
            total = self.nb_places_b_booked * self.price_b
        except TypeError:
            total = 0
        return float(total)

    @property
    def price_c_booked(self):
        try:
            total = self.nb_places_c_booked * self.price_c
        except TypeError:
            total = 0
        return float(total)

    @property
    def price_d_booked(self):
        try:
            total = self.nb_places_d_booked * self.price_d
        except TypeError:
            total = 0
        return float(total)

    @property
    def price_e_booked(self):
        try:
            total = self.nb_places_e_booked * self.price_e
        except TypeError:
            total = 0

        return float(total)

    @property
    def price_booked(self):
        return self.price_a_booked + self.price_b_booked + self.price_c_booked + self.price_d_booked + self.price_e_booked

    @property
    def nb_places_a_booked(self):
        pbooked = Booking.objects.filter(
            performance=self.id).aggregate(Sum('places_a'))
        try:
            total = int(pbooked['places_a__sum'])
        except TypeError:
            total = 0
        return total

    @property
    def nb_places_b_booked(self):
        pbooked = Booking.objects.filter(
            performance=self.id).aggregate(Sum('places_b'))
        try:
            total = int(pbooked['places_b__sum'])
        except TypeError:
            total = 0
        return total

    @property
    def nb_places_c_booked(self):
        pbooked = Booking.objects.filter(
            performance=self.id).aggregate(Sum('places_c'))
        try:
            total = int(pbooked['places_c__sum'])
        except TypeError:
            total = 0
        return total

    @property
    def nb_places_d_booked(self):
        pbooked = Booking.objects.filter(
            performance=self.id).aggregate(Sum('places_d'))

        try:
            total = int(pbooked['places_d__sum'])
        except TypeError:
            total = 0
        return total

    @property
    def nb_places_e_booked(self):
        pbooked = Booking.objects.filter(
            performance=self.id).aggregate(Sum('places_e'))

        try:
            total = int(pbooked['places_e__sum'])
        except TypeError:
            total = 0
        return total

    def is_complete(self):
        """
        Is the performance complete, if yes this indicates that there
        is no available that can be booked.
        """
        if self.nb_places_booked >= self.nb_places_available:
            return True
        else:
            return False

    def _nb_places_dispo(self):
        places = self.nb_places_available - self.nb_places_booked
        return places

    nb_places_booked = property(_nb_places_booked)
    nb_places_dispo = property(_nb_places_dispo)
    nb_places_free = property(_nb_places_dispo)
    format_date = property(_format_date)
    format_date_long = property(_format_date_long)


class Booking(models.Model):
    """
    The booking object
    """
    performance = models.ForeignKey(Performance)
    name = models.CharField(max_length=40, verbose_name='nom')
    firstname = models.CharField(max_length=25, verbose_name='prénom')
    email = models.EmailField(max_length=150, blank=True)
    places_a = models.PositiveIntegerField(
        verbose_name='nb place cat A', null=True)
    places_b = models.PositiveIntegerField(
        verbose_name='nb place cat B', null=True)
    places_c = models.PositiveIntegerField(
        verbose_name='nb place cat C', null=True)
    places_d = models.PositiveIntegerField(
        verbose_name='nb place cat D', null=True)
    places_e = models.PositiveIntegerField(
        verbose_name='nb place cat E', null=True)
    guest = models.BooleanField(verbose_name='invité')
    confirmed = models.BooleanField(verbose_name='confirmé')
    price_total = models.FloatField(null=True)
    message = models.TextField(verbose_name='message', blank=True)
    uuid = models.SlugField(
        max_length=50, unique=False, verbose_name='Numéro de réservation')
    booking_date = models.DateTimeField(auto_now_add=True, blank=True)

    class Meta:
        """
        Meta class
        """
        verbose_name = 'réservation'
        verbose_name_plural = 'réservations'

    def __unicode__(self):
        """
        The unicode method
        """
        return u'%s %s' % (self.name, self.firstname)

    def nb_places(self):
        return self.places_a + self.places_b + self.places_c + self.places_d + self.places_e

    @property
    def nb_places_str(self):
        return u'%s' % (int(self.places_a) + int(self.places_b) + int(self.places_c) + int(self.places_d) + int(self.places_e))

    @property
    def total_price(self):
        """
        Total price for the booking
        """
        total = float(self.places_a) * self.performance.price_a
        try:
            total = total + float(self.places_b) * self.performance.price_b
        except:
            pass
        try:
            total = total + float(self.places_c) * self.performance.price_c
        except:
            pass
        try:
            total = total + float(self.places_d) * self.performance.price_d
        except:
            pass
        try:
            total = total + float(self.places_e) * self.performance.price_e
        except:
            pass
        return total

    def mail_public(self, server_name, email_from, body=None):
        """
        Send an email to whom is concerned
        """
        msg = Messages()
        msg.booking = self
        msg.server_name = server_name
        if body is None:
            body = msg.mail_public()
        subject = u'Votre réservation %s' % self.performance.show.title
        send_mail(subject,
                  body,
                  email_from,
                  [self.email],
                  fail_silently=True)

    def mail_cancel_public(self, server_name, email_from):
        """
        Send an email to whom is concerned
        """
        msg = Messages()
        msg.booking = self
        msg.server_name = server_name
        subject = u'Annulation de votre réservation %s' % self.performance.show.title
        send_mail(subject,
                  msg.mail_cancel_public(),
                  email_from,
                  [self.email],
                  fail_silently=True)

    def mail_admin(self, server_name, email_from, email_to):
        """
        Send an email to show's contact
        """
        msg = Messages()
        msg.booking = self
        msg.server_name = server_name
        title = u'Nouvelle réservation pour'
        subject = u'%s : %s' % (title, self.performance.show.title)
        send_mail(subject, msg.mail_admin(), email_from, [email_to],
                  fail_silently=True)

    def save(self, *args, **kwargs):
        """
        Save method

        Do not save empty booking
        """
        places = int(self.places_a) + int(self.places_b) + int(self.places_c) + int(self.places_d) + int(self.places_e)
        if (places) > 0:
            if self.uuid is None:
                self.uuid = str(uuid4())
            self.price_total = self.total_price
            super(Booking, self).save(*args, **kwargs)

# create apikey
models.signals.post_save.connect(create_api_key, sender=User)
