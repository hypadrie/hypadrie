# -*- coding: utf-8 -*-
"""

 Copyright (c) 2012 Rodolphe Quiédeville <rodolphe@quiedeville.org>

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Context processors definition

"""
from django.contrib.sites.models import Site
from django.conf import settings
from resa.models import Information
from django.core.cache import cache
from random import randint


def demo(request):
    """
    Define if the site is runnning in demo mode or not. Demo mode will
    slighty modify templates by adding an information div on footer.
    """
    return {'DEMO': settings.DEMO}


def piwik(request):
    """
    Piwik parameters
    """
    try:
        pwid = settings.PIWIK_ID
        pfqdn = settings.PIWIK_FQDN
    except AttributeError:
        pwid = None
        pfqdn = None

    return {'PIWIK_ID': pwid,
            'PIWIK_FQDN': pfqdn}


def infobox(request):
    """
    Fetch information that will be display in left box
    """
    curid = Site.objects.get_current().id
    sizes = cache.get('hypa_site_%s_infobox_size' % curid)
    last_index = cache.get('hypa_site_%s_infobox_last_index' % curid)

    if sizes is None or last_index is None:
        infos = Information.objects.filter(published=True,
                                           site__in=[curid])
        sizes = len(infos)
        last_index = 1
        cache.set('hypa_site_%s_infobox_size' % curid, sizes, 300)
        cache.set('hypa_site_%s_infobox_last_index' % curid, 1, 300)
        i = 0
        for info in infos:
            i = i + 1
            pref = 'hypa_site_%s_infobox_entry' % curid
            cache.set('%s_%s_title' % (pref, i), info.title, 300)
            cache.set('%s_%s_desc' % (pref, i), info.information, 300)
            cache.set('%s_%s_url' % (pref, i), info.url, 300)

    title = cache.get('hypa_site_%s_infobox_entry_%s_title' % (curid,
                                                               last_index))
    desc = cache.get('hypa_site_%s_infobox_entry_%s_desc' % (curid,
                                                             last_index))
    url = cache.get('hypa_site_%s_infobox_entry_%s_url' % (curid,
                                                           last_index))

    # Case of dummy cache
    if title is None and sizes > 0 and len(infos) > 0:
        index = randint(0, sizes - 1)
        title = infos[index].title
        url = infos[index].url
        desc = infos[index].information

    if sizes >= last_index + 1:
        cache.set('hypa_site_%s_infobox_last_index' % curid,
                  last_index + 1,
                  300)
    else:
        cache.set('hypa_site_%s_infobox_last_index' % curid,
                  1,
                  300)

    return {'info_title': title,
            'info_information': desc,
            'info_url': url}
