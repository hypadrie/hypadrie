import os, sys

project_path = os.path.dirname(os.path.abspath(__file__))
homedir, appname = os.path.split(project_path)

sys.path.insert(0, project_path)
sys.path.insert(0, homedir)

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hypadrie.settings")

# This application object is used by the development server
# as well as any WSGI server configured to use this file.
import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()

