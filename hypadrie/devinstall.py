#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2012 Rodolphe Quiedeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# NEVER USE THIS SCRIPT ON A PRODUCTION INSTANCE
#
# !! THIS SCRIPT DELETES THE DATABASES !!
#
# Only useful on a dev environnement
# 
import commands
from settings import *

if DATABASES['default']['ENGINE'] == 'django.db.backends.sqlite3':
    print "Remove : %s" % (DATABASES['default']['NAME'])
    commands.getoutput('/bin/rm %s' % DATABASES['default']['NAME'])

#
print './manage.py syncdb --noinput'

syncdb = commands.getoutput('./manage.py syncdb --noinput')

print syncdb

datafile = '../samples/sqlite3.sql'

print "Load datas from : %s " % datafile

if DATABASES['default']['ENGINE'] == 'django.db.backends.sqlite3':
    load = commands.getoutput('sqlite3 %s < %s' % (DATABASES['default']['NAME'],
                                                   datafile)
                              )

if DATABASES['default']['ENGINE'] == 'django.db.backends.postgresql_psycopg2':
    load = "No data for postgresql"

print load

update_index = commands.getoutput('./manage.py update_index')

print update_index
