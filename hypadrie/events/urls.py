"""
Events app urls

"""
from django.conf.urls.defaults import *

urlpatterns = patterns('events.views',
                       (r'^$', 'public_list'),
                       (r'^(?P<event_id>\d+)$', 'public_events'),
                       (r'^(?P<event_id>\d+)/registered$', 'public_registered'),
                       (r'^(?P<event_id>\d+)/cancel/(?P<unid1>\w+)-(?P<unid2>\w+)-(?P<unid3>\w+)-(?P<unid4>\w+)-(?P<unid5>\w+)$', 'public_cancel'),
                       (r'^(?P<event_id>\d+)/confirm/(?P<unid1>\w+)-(?P<unid2>\w+)-(?P<unid3>\w+)-(?P<unid4>\w+)-(?P<unid5>\w+)$', 'public_confirm'),
                        (r'^search/$', 'search_events'),
)
