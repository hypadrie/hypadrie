# -*- coding: utf-8 -*-  pylint: disable-msg=R0801
#
# Copyright (c) 2012 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""


"""
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.test import TestCase, Client
from datetime import datetime
from hypadrie.resa.models import Show
from hypadrie.resa.models import Performance
from hypadrie.resa.models import Place


class EventTests(TestCase):  # pylint: disable-msg=R0904
    """
    

    """
    def setUp(self):
        """
        Init
        """
        self.site = Site.objects.create(domain='sample_site')
        self.user = User.objects.create_user('admin_search',
                                             'admin_search@bar.com',
                                             'admintest')

    def test_viewshows(self):
        """
        View owner shows
        """
        Show.objects.all().delete()
        place = Place.objects.create(title='foo_place_l')

        show = Show.objects.create(title='tae0of6G',
                                   company='ipsum',
                                   description='happy hacking',
                                   published=True,
                                   events=True,
                                   created_by=self.user)
        show.site.add(1)
        show.owner.add(self.user.id)

        perf = Performance.objects.create(
            show=show,
            place=place,
            date=datetime(2016, 6, 7, 20, 10, 2),
            published=True,
            nb_places_available=42,
            price_a=10,
            price_b=3,
            price_c=2,
            price_d=4,
            price_e=5)

        client = Client()
        client.login(username='admin_search', password='admintest')

        # lookup on description
        response = client.get('/events/')
        self.assertContains(response, show.title, status_code=200)

    def test_oneevent(self):
        """
        View owner shows
        """
        Show.objects.all().delete()
        place = Place.objects.create(title='mu3yah1D')

        show = Show.objects.create(title='Aey3Odoo',
                                   company='ipsum',
                                   description='happy hacking',
                                   published=True,
                                   events=True,
                                   created_by=self.user)
        show.site.add(1)
        show.owner.add(self.user.id)

        perf = Performance.objects.create(
            show=show,
            place=place,
            date=datetime(2016, 6, 7, 20, 10, 2),
            published=True,
            nb_places_available=42,
            price_a=10,
            price_b=3,
            price_c=2,
            price_d=4,
            price_e=5)

        client = Client()
        client.login(username='admin_search', password='admintest')

        # lookup on description
        response = client.get('/events/%s' % show.id)
        self.assertContains(response, show.title, status_code=200)

