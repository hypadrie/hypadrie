# -*- coding: utf-8 -*-  pylint: disable-msg=R0801
#
# Copyright (c) 2012 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""

"""
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.test import TestCase, Client
from datetime import datetime
import hypadrie.resa.search_indexes
from hypadrie.resa.models import Show
from hypadrie.resa.models import Performance
from hypadrie.resa.models import Place
from haystack.management.commands import update_index


class EventSearchTests(TestCase):  # pylint: disable-msg=R0904
    """
    
    """
    def setUp(self):
        """
        Init
        """
        self.site = Site.objects.create(domain='sample_site')
        self.user = User.objects.create_user('admin_search',
                                             'admin_search@bar.com',
                                             'admintest')

        settings.HAYSTACK_CONNECTIONS = {
            'default': {
                'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
                'PATH': '/tmp/whoosh_index',
            },
        }
        update_index.Command().handle(using='default')

    def test_search_empty(self):
        """
        Make an empty search is working
        """
        client = Client()
        response = client.get('/events/search/')
        self.assertContains(response, 'form', status_code=200)

    def test_search_nonempty(self):
        """
        Make an non-empty search is working
        """
        client = Client()
        response = client.get('/events/search/?q=foobar')
        self.assertContains(response, 'form', status_code=200)


