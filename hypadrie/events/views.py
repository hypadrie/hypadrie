# -*- coding: utf-8 -*-  pylint: disable-msg=R0801
#
# Copyright (c) 2012 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
The django views for events
"""
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, EmptyPage, InvalidPage
from django.contrib.sites.models import get_current_site
from django.shortcuts import get_object_or_404, redirect
from django.http import HttpResponse
from django.template import RequestContext, loader
from django.conf import settings
from django.shortcuts import render
from resa.models import Show, Performance, Booking
from events.forms import EventForm
from time import strftime, localtime

def public_list(request):
    """
    The public list events, use as the main page
    """
    datenow = strftime("%Y-%m-%d %H:%M:%S", localtime())
    elist = Performance.objects.filter(
        date__gt=datenow,
        published=True,
        show__events=True,
        show__site__in=[settings.SITE_ID]).order_by('-pk')

    paginator = Paginator(elist, 10)

    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1

    try:
        events = paginator.page(page)
    except (EmptyPage, InvalidPage):
        events = paginator.page(paginator.num_pages)

    return render(request,
                  'events/events_list.html',
                  {'events': events,
                   'onglet': 'events'})

def public_events(request, event_id):
    """
    Registering process form on a particular event
    """
    evnt = Performance.objects.filter(
        pk=event_id,
        published=True,
        show__events=True,
        show__site__in=[settings.SITE_ID])[0]
    bookings = None
    if evnt is not None:
        if evnt.public_booking:
            bookings = Booking.objects.filter(
                performance__pk=evnt.id).order_by('-pk')[:3]
    i = 1
    place_list = []
    maxplaces = min(settings.MAX_BOOKABLE_PLACE, evnt.nb_places_free)
    while (i <= maxplaces):
        place_list.append((i, i))
        i = i + 1

    if request.method == 'POST':
        form = EventForm(request.POST, 
                         maxplaces=1,
                         email_required=evnt.require_confirmation)
        form.fields['nb_place_a'].choices = place_list
        if form.is_valid():
            bkn = Booking(performance_id=event_id,
                          firstname=form.cleaned_data['firstname'],
                          name=form.cleaned_data['name'],
                          places_a=1,
                          places_b=0,
                          places_c=0,
                          places_d=0,
                          places_e=0,
                          email=form.cleaned_data['email'],
                          uuid=None)
            bkn.save()
            msgbody = loader.render_to_string(
                'messages/fr_FR/event_booking_public.txt',
                {'site': get_current_site(request),
                 'booking': bkn})
            bkn.mail_public(request.META['SERVER_NAME'],
                            settings.EMAIL_FROM,
                            msgbody)
            if evnt.bkng_send_email and len(evnt.bkng_contact_email) > 0:
                bkn.mail_admin(request.META['SERVER_NAME'],
                               settings.EMAIL_FROM,
                               evnt.bkng_contact_email)
            return render(request,
                          'events/event_success.html',
                          {'event': evnt,
                           'bkn': bkn,
                           'bookings': bookings,
                           'form': form,
                           'last_events': last_events(),
                           'nb_place_list': place_list,})

    else:
        form = EventForm()                

    return render(request,
                  'events/event.html',
                  {'event': evnt,
                   'bookings': bookings,
                   'form': form,
                   'last_events': last_events(),
                   'nb_place_list': place_list,})

def public_registered(request, event_id):
    """
    List of people who registered on an particular event
    """
    evnt = get_object_or_404(Performance, pk=event_id)

    elist = Booking.objects.filter(
        performance__pk=event_id,
        performance__show__events=True).order_by('-pk')

    paginator = Paginator(elist, 40)

    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1

    try:
        bookings = paginator.page(page)
    except (EmptyPage, InvalidPage):
        bookings = paginator.page(paginator.num_pages)

    return render(request,
                  'events/registered.html',
                  {'bookings': bookings,
                   'onglet': 'events',
                   'event': evnt,
                   'last_events': last_events(),})

def public_cancel(request, event_id, unid1, unid2, unid3,
                  unid4, unid5):
    """
    Cancel a registration
    """
    evnt = get_object_or_404(Performance, pk=event_id)
    uuid = "%s-%s-%s-%s-%s" % (unid1, unid2, unid3, unid4, unid5)
    perf = get_object_or_404(Performance, pk=event_id)
    tpl = loader.get_template('events/cancel.html')
    try:
        #Delete a Booking object
        bkg = Booking.objects.get(uuid=uuid)
        bkg.mail_cancel_public(request.META['SERVER_NAME'],
                               settings.EMAIL_FROM)
        bkg.delete()
        ctx = RequestContext(request, {'event': evnt,
                                       'uuid': uuid,
                                       'result': 'success',
                                       'last_events': last_events(),
                                       })
    except ObjectDoesNotExist:
        ctx = RequestContext(request, {'event': evnt,
                                       'uuid': uuid,
                                       'result': 'notexist',
                                       'last_events': last_events(),
                                       })
    return HttpResponse(tpl.render(ctx))

def last_events():
    """
    Return the last 3 public events
    """
    return Performance.objects.filter(
        date__gt=strftime("%Y-%m-%d %H:%M:%S", localtime()),
        published=True,
        show__events=True).order_by('-pk')[:3]

def public_confirm(request, event_id, unid1, unid2, unid3,
                  unid4, unid5):
    """
    Cancel a registration
    """
    evnt = get_object_or_404(Performance, pk=event_id)
    uuid = "%s-%s-%s-%s-%s" % (unid1, unid2, unid3, unid4, unid5)
    perf = get_object_or_404(Performance, pk=event_id)
    tpl = loader.get_template('events/confirm.html')
    try:
        #Delete a Booking object
        bkg = Booking.objects.get(uuid=uuid)
        bkg.confirmed=True
        bkg.save()
        ctx = RequestContext(request, {'event': evnt,
                                       'uuid': uuid,
                                       'result': 'success',
                                       'last_events': last_events(),
                                       })
    except ObjectDoesNotExist:
        ctx = RequestContext(request, {'event': evnt,
                                       'uuid': uuid,
                                       'result': 'notexist',
                                       'last_events': last_events(),
                                       })
    return HttpResponse(tpl.render(ctx))

def search_events(request):
    from haystack.query import SearchQuerySet
    shwids = []
    results = []
    try:
        qry_str = request.GET.get('q')
    except NameError:
        qry_str = None

    # search only on Show
    if qry_str is not None:
        shows = SearchQuerySet().filter(content__contains=qry_str).filter(family='event').models(Show)
        for show in shows:
            shwids.append(show.object.id)
        results = Performance.objects.filter(show__in=shwids)

    if len(results) == 1:
        return redirect('/events/%s' % results[0].id)        
    else:
        return render(request,
                      'events/search.html',
                      {'results': results,
                       'query': qry_str})
