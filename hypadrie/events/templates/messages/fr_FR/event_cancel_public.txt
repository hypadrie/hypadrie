{% load i18n %}Bonjour, Bonsoir,

Nous vous avons bien enregistré votre annulation pour le spectacle :
                                
 Spectacle : {{ booking.performance.show.title }}
 Date : {{ booking.performance.date }}
 Salle : {{ booking.performance.place.title }}

Information sur la réservation :

 nom : {{ booking.firstname }} {{ booking.name }}
 nb de places : {{ booking.nb_places_str }}
 prix total   : {{ booking.price_total|floatformat:2 }} euros TTC
 numero de resa : {{ booking.uuid }} 

Si vous souhaitez réserver une place à un autre horaire ou pour un
autre spectacle :

  http://{{ site.domain }}/representation/{{ booking.performance.id }}


En espérant vous compter parmi nos spectateurs une prochaine fois.


PS : il n'est pas possible de répondre directement à cet email qui a
été envoyé automatiquement, si vous désirez nous contacter vous
trouverez toutes nos coordonnées sur notre site à l'adresse 
http://{{ site.domain }}/contact/
