# -*- coding: utf-8 -*-
#
# Copyright (c) 2012 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from django import forms
from django.forms.widgets import Select, TextInput, Textarea
from django.forms.widgets import CheckboxInput


class EventForm(forms.Form):

    firstname = forms.CharField(
        required=True,
        max_length=25,
        widget=TextInput(attrs={'class': 'input-large focused'}))

    name = forms.CharField(
        required=False,
        max_length=40,
        widget=TextInput(attrs={'class': 'input-large'}))

    nb_place_a = forms.ChoiceField(
        required=False,
        widget=Select(attrs={'class': 'span1'}))

    email = forms.EmailField(
        required=False,
        max_length=150,
        widget=TextInput(attrs={'class': 'input-xlarge'}))

    handicap = forms.BooleanField(
        required=False,
        widget=CheckboxInput())

    handicap_type = forms.BooleanField(
        required=False,
        widget=CheckboxInput())

    handicap_comment = forms.CharField(
        required=False,
        max_length=40,
        widget=TextInput(attrs={'class': 'input-large'}))

    handicap_contact = forms.BooleanField(
        required=False,
        widget=CheckboxInput())

    def __init__(self, *args, **kwargs):
        self.maxplaces = kwargs.pop('maxplaces', None)
        email_requ = kwargs.pop('email_required', None)
        super(EventForm, self).__init__(*args, **kwargs)
        if email_requ:
            self.fields['email'].required = email_requ

    def clean(self):
        cleaned_data = self.cleaned_data

        total = 1

        text = """
Pas assez de place disponible, il ne reste que %s places,
vous en avez demandé %s.
"""

        if self.maxplaces is None:
            msg = "Impossible d'enregistrer votre réservation"
            raise forms.ValidationError(msg)
        else:
            if (total > int(self.maxplaces)):
                msg = text % (self.maxplaces, total)
                raise forms.ValidationError(msg)
            if (total == 0):
                raise forms.ValidationError(msg_tot0)

        return cleaned_data
