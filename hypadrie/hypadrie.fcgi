#!/usr/bin/python
import sys, os

# change this to suits your needsOB
INSTALL_DIR = "/home/rodo/dev/public/hypadrie"
 
# Add a custom Python path.
sys.path.insert(0, INSTALL_DIR)
 
# Switch to the directory of your project. (Optional.)
# os.chdir(INSTALL_DIR)
 
# Set the DJANGO_SETTINGS_MODULE environment variable.
os.environ['DJANGO_SETTINGS_MODULE'] = "hypadrie.settings"
 
from django.core.servers.fastcgi import runfastcgi
runfastcgi(method="threaded", daemonize="false")
