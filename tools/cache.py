# -*- coding: utf-8 -*-
"""
Used to debug cache

"""
import memcache

mc = memcache.Client(['127.0.0.1:11211'], debug=1)

mc.set('foo', 'bar')
print mc.get('foo')

prefix = "hpdr"

sizes = mc.get('%s:1:hypdarie_infobox_size' % prefix)
last_index = mc.get('%s:1:hypdarie_infobox_last_index' % prefix)

print "sizes : %s lastindex : %s"% (sizes,last_index)

if sizes == None or last_index == None:
    print "One is None"
