BEGIN;
--
--
DELETE FROM resa_festival;
--
INSERT INTO "resa_festival" (id,title,url,published,date_begin,date_end,description)
VALUES (1,'Les fous du clavier','fous-du-clavier','0','2012-03-03','2012-03-09','Rencontres de geeks aguerris');

INSERT INTO "resa_festival" (id,title,url,published,date_begin,date_end,description, media_url, external_url)
VALUES (2,'Clown hors-piste 2012','festipsum','1','2012-11-02','2012-11-04','Cirque burlesque','/static/samples/animals.jpeg','http://beta.hypadrie.eu/festipsum/');

INSERT INTO "resa_festival" (id,title,url,published,date_begin,date_end,description)
VALUES (3,'Festi''Clown 2011','festiclown2011','1','2011-11-12','2011-11-13','Cirque burlesque');

INSERT INTO "resa_festival" (id,title,url,published,date_begin,date_end,description)
VALUES (4,'Geek clown','geek-clowns','1','2012-09-01','2012-09-01','Rencontre avec les clown');

INSERT INTO "resa_festival_owner" (id,festival_id, user_id) VALUES (1,1,1);
INSERT INTO "resa_festival_owner" (id,festival_id, user_id) VALUES (2,1,2);
INSERT INTO "resa_festival_owner" (id,festival_id, user_id) VALUES (3,2,1);
INSERT INTO "resa_festival_owner" (id,festival_id, user_id) VALUES (4,3,1);
INSERT INTO "resa_festival_owner" (id,festival_id, user_id) VALUES (5,4,1);

INSERT INTO "resa_festival_site" (id, festival_id, site_id) VALUES (1,1,3);
INSERT INTO "resa_festival_site" (id, festival_id, site_id) VALUES (2,2,2);
INSERT INTO "resa_festival_site" (id, festival_id, site_id) VALUES (3,3,2);
INSERT INTO "resa_festival_site" (id, festival_id, site_id) VALUES (4,4,2);
INSERT INTO "resa_festival_site" (id, festival_id, site_id) VALUES (5,4,3);

--
-- syncd shoul create an site id 1 named "example.com"
--
INSERT INTO "django_site" (id, "domain", "name") VALUES (2,"hypadrie.localdomain","hypadrie");
INSERT INTO "django_site" (id, "domain", "name") VALUES (3,"hypadrole.localdomain","hypadrole");
--
DELETE FROM resa_information;

INSERT INTO "resa_information" ("id","title","information","published","url") 
VALUES (1, 'Festipsum bientôt','Le prochain festival de clowns est déjà programmé. Il se déroulera fin novembre 2012 à Saint-Avé','1','http://beta.hypadrie.eu/festipsum/');

INSERT INTO "resa_information" ("id","title","information","published","url") 
VALUES (2, 'Hypadrole','Lorem ipsum','1','http://hypadrole.localdomain');

INSERT INTO "resa_information" ("id","title","information","published","url") 
VALUES (3, 'Hypadrie','Lorem ipsum','1','http://hypadrie.localdomain');


INSERT INTO "resa_information_site" (id, information_id, site_id) VALUES (1,1,2);
INSERT INTO "resa_information_site" (id, information_id, site_id) VALUES (2,1,3);
INSERT INTO "resa_information_site" (id, information_id, site_id) VALUES (3,2,2);
INSERT INTO "resa_information_site" (id, information_id, site_id) VALUES (4,3,3);

--
--
--
INSERT INTO "resa_show_owner" (id, show_id, user_id) VALUES (6,1,1);
INSERT INTO "resa_show_owner" (id, show_id, user_id) VALUES (7,2,1);
INSERT INTO "resa_show_owner" (id, show_id, user_id) VALUES (8,3,1);
INSERT INTO "resa_show_owner" (id, show_id, user_id) VALUES (9,4,1);
INSERT INTO "resa_show_owner" (id, show_id, user_id) VALUES (10,5,1);
INSERT INTO "resa_show_owner" (id, show_id, user_id) VALUES (11,6,1);
INSERT INTO "resa_show_owner" (id, show_id, user_id) VALUES (12,7,1);
INSERT INTO "resa_show_owner" (id, show_id, user_id) VALUES (13,10,2);
INSERT INTO "resa_show_owner" (id, show_id, user_id) VALUES (14,10,1);
--
INSERT INTO "resa_show_site" (id, show_id, site_id) VALUES (1,1,1);
INSERT INTO "resa_show_site" (id, show_id, site_id) VALUES (2,2,2);
INSERT INTO "resa_show_site" (id, show_id, site_id) VALUES (3,3,2);
INSERT INTO "resa_show_site" (id, show_id, site_id) VALUES (4,4,2);
INSERT INTO "resa_show_site" (id, show_id, site_id) VALUES (5,5,2);
INSERT INTO "resa_show_site" (id, show_id, site_id) VALUES (6,6,1);
INSERT INTO "resa_show_site" (id, show_id, site_id) VALUES (7,7,1);
INSERT INTO "resa_show_site" (id, show_id, site_id) VALUES (8,7,2);
INSERT INTO "resa_show_site" (id, show_id, site_id) VALUES (9,8,2);
INSERT INTO "resa_show_site" (id, show_id, site_id) VALUES (10,9,1);
INSERT INTO "resa_show_site" (id, show_id, site_id) VALUES (11,10,2);

--
DELETE FROM resa_show;

INSERT INTO "resa_show" ("id","events","title","company" ,"published","contact_name" ,"contact_tel" ,"contact_email","description","company_url" ,"show_url", "created_by_id")
VALUES (1, 0,'Pilouface','Ile logique','1','Cedric Aubouy','0606060606','email@email.com','Super spectacle','http://ilelogique.fr','http://ilelogique.fr', 1);

INSERT INTO "resa_show" ("id","events","title","company" ,"published","contact_name" ,"contact_tel" ,"contact_email","description","company_url" ,"show_url", "created_by_id")
VALUES (2, 0,'Pi 3.14','Ile logique','1','Cedric Aubouy','0606060606','email@email.com','Super spectacle autour du nombre Pi','http://ilelogique.fr','http://ilelogique.fr',1);

INSERT INTO "resa_show" ("id","events","title","company" ,"published","contact_name" ,"contact_tel" ,"contact_email","description","company_url" ,"show_url", "created_by_id")
VALUES (3, 0,'Dé-pensons','Ile logique','1','Cedric Aubouy','0606060606','email@email.com','Super spectacle autour du nombre Pi de la compagnie Ile Logique.  Logique absurde','http://ilelogique.fr','http://ilelogique.fr',1);

INSERT INTO "resa_show" ("id","events","title","company" ,"published","contact_name" ,"contact_tel" ,"contact_email","description","company_url" ,"show_url", "created_by_id")
VALUES (4, 0,'hidden','non showed show','0','Rodo','06','email@email.com','Ce spectacle est cache et ne doit pas sortir dans les recherches','http://non.null.fr','http://null.fr', 1);

INSERT INTO "resa_show" ("id","events","title","company" ,"published","contact_name" ,"contact_tel" ,"contact_email","description","company_url" ,"show_url", "created_by_id")
VALUES (5, 0,'lorem','lorem ipsum','1','Rodo','06','email@email.com','Ce spectacle est un test pour la recherche sur lorem','http://non.null.fr','http://null.fr', 1);

INSERT INTO "resa_show" ("id","events","title","company" ,"published","contact_name" ,"contact_tel" ,"contact_email","description","company_url" ,"show_url", "created_by_id")
VALUES (6, 0,'super other site','super lorem ipsum','1','Rodo','06','email@email.com','Ce spectacle est un test pour la recherche sur lorem','http://non.null.fr','http://null.fr', 1);

INSERT INTO "resa_show" ("id","events","title","company" ,"published","contact_name" ,"contact_tel" ,"contact_email","description","company_url" ,"show_url", "created_by_id", "family")
VALUES (7, 1,'The seven french FLOSS meeting','super seven show','1','Rodo','06','email@email.com','Ce spectacle est un test pour la recherche sur lorem','http://non.null.fr','http://null.fr', 1,'event');

INSERT INTO "resa_show" ("id","events","title","company" ,"published","contact_name" ,"contact_tel" ,"contact_email","description","company_url" ,"show_url", "created_by_id", "family")
VALUES (8, 1,'Spanish FLOSS meeting 2012','Spanish meeting in spain','1','Rodo','06','email@email.com','Spanish meeting in madrid with special guest start from free software projects','http://non.null.fr','http://null.fr', 1, 'event');

INSERT INTO "resa_show" ("id","events","title","company" ,"published","contact_name" ,"contact_tel" ,"contact_email","description","company_url" ,"show_url", "created_by_id", "family")
VALUES (9, 1,'Belgium FLOSS meeting 2012','Spanish meeting in spain','1','Rodo','06','email@email.com','Spanish meeting in madrid with special guest start from free software projects','http://non.null.fr','http://null.fr', 1, 'event');

INSERT INTO "resa_show" ("id","events","title","company" ,"published","contact_name" ,"contact_tel" ,"contact_email","description","company_url" ,"show_url", "created_by_id", "family")
VALUES (10, 1,'Germany FLOSS meeting 2012','Spanish meeting in spain','1','Rodo','06','email@email.com','Spanish meeting in madrid with special guest start from free software projects','http://non.null.fr','http://null.fr', 1, 'event');


DELETE FROM resa_place;

INSERT INTO "resa_place" (id,title,address,town,lon,lat)
VALUES (1,'Théatre Anne de Bretagne','','Vannes', -2.76121,47.66178);

INSERT INTO "resa_place" (id,title,address,town)
VALUES (2,'Dôme','','Saint-Avé');

INSERT INTO "resa_place" (id,title,address,town,lon,lat)
VALUES (3,'Fil','','Saint-Étienne',4.39074,45.44995);
--
--
DELETE FROM resa_performance;

INSERT INTO resa_performance (require_confirmation, id,show_id,place_id,date,published,open_booking,nb_places_available,price_a,label_a,price_b,information,bkng_send_email,festival_id, nb_places_onspot,public_booking)
VALUES (0,1,1,1,'2012-11-14 17:00:00','1','1','25','3.5','PrixA','2.3','','0',2,10,0);
INSERT INTO resa_performance (require_confirmation, id,show_id,place_id,date,published,open_booking,nb_places_available,price_a,label_a,price_b,price_c,information,bkng_send_email, nb_places_onspot,public_booking)
VALUES (0,2,2,2,'2012-06-01 17:00:00','1','1','120','3.5','PrixA','2.3','1.2','','0',0,1);
INSERT INTO resa_performance (require_confirmation, id,show_id,place_id,date,published,open_booking,nb_places_available,price_a,label_a,price_b,information,bkng_send_email, nb_places_onspot,public_booking)
VALUES (0,3,1,2,'2012-06-01 18:00:00','1','1','120','3.5','PrixA','2.3','','0',2,1);
INSERT INTO resa_performance (require_confirmation, id,show_id,place_id,date,published,open_booking,nb_places_available,price_a,label_a,price_b,information,bkng_send_email, nb_places_onspot,public_booking)
VALUES (0,4,1,1,'2012-11-14 16:00:00','1','1','25','3.5','PrixA','2.3','','0',0,1);
INSERT INTO resa_performance (require_confirmation, id,show_id,place_id,date,published,open_booking,nb_places_available,price_a,label_a,price_b,information,bkng_send_email,festival_id, nb_places_onspot,public_booking)
VALUES (0,5,1,1,'2012-11-14 18:00:00','1','1','25','3.5','PrixA','2.3','',0,2,0,1);
INSERT INTO resa_performance (require_confirmation, id,show_id,place_id,date,published,open_booking,nb_places_available,price_a,label_a,price_b,information,bkng_send_email,festival_id, nb_places_onspot,public_booking)
VALUES (0,6,2,1,'2012-11-14 19:00:00','1','1','120','3.5','PrixA','2.3','',0,2,0,1);
INSERT INTO resa_performance (require_confirmation, id,show_id,place_id,date,published,open_booking,nb_places_available,price_a,label_a,price_b,information,bkng_send_email, nb_places_onspot,public_booking)
VALUES (0,7,1,1,'2011-11-14 16:00:00','1','1','120','3.5','PrixA','2.3','',0,0,1);
INSERT INTO resa_performance (require_confirmation, id,show_id,place_id,date,published,open_booking,nb_places_available,price_a,label_a,price_b,information,bkng_send_email, nb_places_onspot,public_booking)
VALUES (0,8,3,1,'2012-07-14 18:00:00','1','1','120','3.5','PrixA','2.3','',0,0,1);
INSERT INTO resa_performance (require_confirmation, id,show_id,place_id,date,published,open_booking,nb_places_available,price_a,label_a,price_b,price_c,price_d,information,bkng_send_email, nb_places_onspot,public_booking)
VALUES (0,9,3,1,datetime('now','+1 day'),'1','1','120','3.5','PrixA','2.3','2','1.2','','0','0',0);
INSERT INTO resa_performance (require_confirmation, id,show_id,place_id,date,published,open_booking,nb_places_available,price_a,label_a,information,bkng_send_email, nb_places_onspot,public_booking)
VALUES (0,10,3,1,datetime('now','-5 day'),'1','1','120','3.5','PrixA','','0','0',1);
INSERT INTO resa_performance (require_confirmation, id,show_id,place_id,date,published,open_booking,nb_places_available,label_a,label_b,label_c,label_d,label_e,price_a,price_b,price_c,price_d,price_e,information,bkng_send_email, nb_places_onspot,public_booking)
VALUES (0,11,3,1,datetime('now','+2 day'),'1','1','20','Plein tarif','Tarif enfants','Tarifs Chômeurs','Tarifs étudiants','Tarifs pauvres','7.5','5','4','3','2','','0','4',0);
INSERT INTO resa_performance (require_confirmation, id,show_id,place_id,date,published,open_booking,nb_places_available,label_a,label_b,label_c,label_d,label_e,price_a,price_b,price_c,price_d,price_e,information,bkng_send_email, nb_places_onspot,public_booking)
VALUES (0,12,7,1,datetime('now','+2 day'),'1','1','20','Plein tarif','Tarif enfants','Tarifs Chômeurs','Tarifs étudiants','Tarifs pauvres','7.5','5','4','3','2','','0','4',0);
INSERT INTO resa_performance (require_confirmation, id,show_id,place_id,date,published,open_booking,nb_places_available,label_a,label_b,label_c,label_d,label_e,price_a,price_b,price_c,price_d,price_e,information,bkng_send_email, nb_places_onspot,public_booking)
VALUES (1,13,8,1,datetime('now','+3 day'),'1','1','20','Plein tarif','Tarif enfants','Tarifs Chômeurs','Tarifs étudiants','Tarifs pauvres','7.5','5','4','3','2','','0','0',1);
INSERT INTO resa_performance (require_confirmation, id,show_id,place_id,date,published,open_booking,nb_places_available,label_a,label_b,label_c,label_d,label_e,price_a,price_b,price_c,price_d,price_e,information,bkng_send_email, nb_places_onspot,public_booking)
VALUES (1,14,9,1,datetime('now','+3 day'),'1','1','20','Plein tarif','Tarif enfants','Tarifs Chômeurs','Tarifs étudiants','Tarifs pauvres','7.5','5','4','3','2','','0','0',1);
INSERT INTO resa_performance (require_confirmation, id,show_id,place_id,date,published,open_booking,nb_places_available,label_a,label_b,label_c,label_d,label_e,price_a,price_b,price_c,price_d,price_e,information,bkng_send_email, nb_places_onspot,public_booking)
VALUES (1,15,10,1,datetime('now','+3 day'),'1','1','20','Plein tarif','Tarif enfants','Tarifs Chômeurs','Tarifs étudiants','Tarifs pauvres','7.5','5','4','3','2','','0','0',1);

--
--
DELETE FROM resa_booking;

INSERT INTO resa_booking (confirmed, id,performance_id,name,firstname,email,places_a,places_b,message,uuid,booking_date,guest)
VALUES (0,1,3,"Quiédeville","Rodolphe","rodolphe@quiedeville.org",78,32,"","d38c91f1-4045-44f7-bcb9-9c2f9edd5a4e",'2012-03-30 02:32:42','0');

INSERT INTO resa_booking (confirmed, id,performance_id,name,firstname,email,places_a,places_b,message,uuid,booking_date,guest)
VALUES (0,33,3,"Fooluc","Banjo","banjo@guiter.fr",10,0,"","d38c9f11-4045-44f7-bcb9-9c2f9edd5a4e",'2012-05-30 02:32:42','0');

INSERT INTO resa_booking (confirmed, id,performance_id,name,firstname,email,places_a,places_b,message,uuid,booking_date,guest)
VALUES (0,2,6,'totor','ultrices','tortor@ultrices.com','1','0','','d38c91f1-4045-44f7-bcb9-9c2f9edd5a4e','2012-03-30 02:32:42','0');

INSERT INTO resa_booking (confirmed, id,performance_id,name,firstname,email,places_a,places_b,message,uuid,booking_date,guest)
VALUES (0,3,6,'tristique','diam','foo@example.com','1','0','','d38c91f1-4045-44f7-bcb9-9c2f9edd5a5e','2012-03-30 02:32:42','0');

INSERT INTO resa_booking (confirmed, id,performance_id,name,firstname,email,places_a,places_b,message,uuid,booking_date,guest)
VALUES (0,4,6,'vitae','ultricies','foo@example.com','1','0','','d38c91f1-4045-44f7-bcb9-9c2f9edd5a6e','2012-03-30 02:32:42','0');

INSERT INTO resa_booking (confirmed, id,performance_id,name,firstname,email,places_a,places_b,message,uuid,booking_date,guest)
VALUES (0,5,6,'Pellentesque','scelerisque','foo@example.com','1','0','','d38c91f1-4045-44f7-bcb9-9c3f9edd5a4e','2012-03-30 02:32:42','0');

INSERT INTO resa_booking (confirmed, id,performance_id,name,firstname,email,places_a,places_b,message,uuid,booking_date,guest)
VALUES (0,6,6,'massa','vel','foo@example.com','1','0','','d38c91f1-4045-44f7-bcb9-9c2f9edd5a7e','2012-03-30 02:32:42','0');

INSERT INTO resa_booking (confirmed, id,performance_id,name,firstname,email,places_a,places_b,message,uuid,booking_date,guest)
VALUES (0,7,6,'tortor','ultrices','tortor@ultrices.fr','1','1','','d38c91f1-4045-44f7-bcb9-9c2fe9dd5a4e','2012-03-30 02:32:42','0');

INSERT INTO resa_booking (confirmed, id,performance_id,name,firstname,email,places_a,places_b,message,uuid,booking_date,guest)
VALUES (0,8,6,'bibendum','Vestibulum','foo@example.com','1','0','','d38c91f1-4045-44f7-bcb9-9cf29edd5a4e','2012-03-30 02:32:42','0');

INSERT INTO resa_booking (confirmed, id,performance_id,name,firstname,email,places_a,places_b,message,uuid,booking_date,guest)
VALUES (0,9,6,'in','arcu','foo@example.com','1','0','','d38c91f1-4045-44f7-bcb9-9c29fedd5a4e','2012-03-30 02:32:42','0');

INSERT INTO resa_booking (confirmed, id,performance_id,name,firstname,email,places_a,places_b,message,uuid,booking_date,guest)
VALUES (0,10,6,'vitae','odio','foo@example.com','1','0','','d38c91f1-4045-44f7-bcb9-9c3f9edd5a4e','2012-03-30 02:32:42','0');

INSERT INTO resa_booking (confirmed, id,performance_id,name,firstname,email,places_a,places_b,message,uuid,booking_date,guest)
VALUES (0,11,6,'tincidunt','aliquet','foo@example.com','1','0','','d38c91f1-4045-44f2-bcb9-9c2f9edd5a4e','2012-03-30 02:32:42','0');

INSERT INTO resa_booking (confirmed, id,performance_id,name,firstname,email,places_a,places_b,message,uuid,booking_date,guest)
VALUES (0,12,6,'sit','amet','foo@example.com','1','0','','d38c91f1-4045-44f7-bcb9-9c2f9ed5da4e','2012-03-30 02:32:42','0');

INSERT INTO resa_booking (confirmed, id,performance_id,name,firstname,email,places_a,places_b,message,uuid,booking_date,guest)
VALUES (0,13,6,'ac','leo.','foo@example.com','1','0','','d38c91f1-4045-45f7-bcb9-9c2f9edd5a4e','2012-03-30 02:32:42','0');

INSERT INTO resa_booking (confirmed, id,performance_id,name,firstname,email,places_a,places_b,message,uuid,booking_date,guest)
VALUES (0,14,6,'Etiam','nisl','foo@example.com','1','0','','d38c91f1-4054-44f7-bcb9-9c2f9edd5a4e','2012-03-30 02:32:42','0');

INSERT INTO resa_booking (confirmed, id,performance_id,name,firstname,email,places_a,places_b,message,uuid,booking_date,guest)
VALUES (0,15,6,'diam','feugiat','foo@example.com','1','0','','d3c891f1-4045-44f7-bcb9-9c2f9edd5a4e','2012-03-30 02:32:42','0');

INSERT INTO resa_booking (confirmed, id,performance_id,name,firstname,email,places_a,places_b,message,uuid,booking_date,guest)
VALUES (0,16,6,'non','viverra','foo@example.com','1','0','','d38c91e1-4045-44f7-bcb9-9c2f9edd5a4e','2012-03-30 02:32:42','0');

INSERT INTO resa_booking (confirmed, id,performance_id,name,firstname,email,places_a,places_b,message,uuid,booking_date,guest)
VALUES (0,17,6,'eu','fringilla','foo@example.com','1','0','','d38c96f1-4045-44f7-bcb9-9c2f9edd5a4e','2012-03-30 02:32:42','0');

INSERT INTO resa_booking (confirmed, id,performance_id,name,firstname,email,places_a,places_b,message,uuid,booking_date,guest)
VALUES (0,18,6,'eget','sem','foo@example.com','1','0','','d38c91f1-4005-44f7-bcb9-9c2f9edd5a4e','2012-03-30 02:32:42','0');

INSERT INTO resa_booking (confirmed, id,performance_id,name,firstname,email,places_a,places_b,message,uuid,booking_date,guest)
VALUES (0,19,6,'Aenean','mattis','foo@example.com','2','5','','d38c91f2-4045-44f7-bcb9-9c2f9edd5a4e','2012-03-30 02:32:42','0');

INSERT INTO resa_booking (confirmed, id,performance_id,name,firstname,email,places_a,places_b,message,uuid,booking_date,guest)
VALUES (0,20,6,'arcu','nunc.','foo@example.com','1','4','','d39c91f1-4045-44f7-bcb9-9c2f9edd5a4e','2012-03-30 02:32:42','0');

INSERT INTO resa_booking (confirmed, id,performance_id,name,firstname,email,places_a,places_b,message,uuid,booking_date,guest)
VALUES (0,21,6,'Ut','tincidunt','foo@example.com','1','0','','e38c91f1-4045-44f7-bcb9-9c2f9edd5a4e','2012-03-30 02:32:42','0');

INSERT INTO resa_booking (confirmed, id,performance_id,name,firstname,email,places_a,places_b,message,uuid,booking_date,guest)
VALUES (0,22,6,'nisl','eget','foo@example.com','1','0','','d38c9df1-4045-44f7-bcb9-9c2f9edd5a4e','2012-03-30 02:32:42','0');

INSERT INTO resa_booking (confirmed, id,performance_id,name,firstname,email,places_a,places_b,message,uuid,booking_date,guest)
VALUES (0,23,6,'ultrices','placerat,','foo@example.com','1','0','','a38c91f1-4045-44f7-bcb9-9c2f9edd5a4e','2012-03-30 02:32:42','0');

INSERT INTO resa_booking (confirmed, id,performance_id,name,firstname,email,places_a,places_b,message,uuid,booking_date,guest)
VALUES (0,24,6,'nibh','urna','foo@example.com','2','0','','d38c91f1-4b45-44f7-bcb9-9c2f9edd5a4e','2012-03-30 02:32:42','0');

INSERT INTO resa_booking (confirmed, id,performance_id,name,firstname,email,places_a,places_b,message,uuid,booking_date,guest)
VALUES (0,25,6,'mattis','elit','foo@example.com','3','4','','d38c91f1-6045-44f7-bcb9-9c2f9edd5a4e','2012-03-30 02:32:42','0');

INSERT INTO resa_booking (confirmed, id,performance_id,name,firstname,email,places_a,places_b,message,uuid,booking_date,guest)
VALUES (0,26,6,'vitae','pretium','foo@example.com','2','2','','d38c91f1-7045-44f7-bcb9-9c2f9edd5a4e','2012-03-30 02:32:42','0');

INSERT INTO resa_booking (confirmed, id,performance_id,name,firstname,email,places_a,places_b,message,uuid,booking_date,guest)
VALUES (0,27,6,'diam','nunc','foo@example.com','1','0','','d38c91f1-4048-44f7-bcb9-9c2f9edd5a4e','2012-03-30 02:32:42','0');

INSERT INTO resa_booking (confirmed, id,performance_id,name,firstname,email,places_a,places_b,message,uuid,booking_date,guest)
VALUES (0,28,6,'id','ligula','cras@example.com','1','3','','d38c91f1-4145-44f7-bcb9-9c2f9edd5a4e','2012-03-30 22:02:42','0');

INSERT INTO resa_booking (confirmed, id,performance_id,name,firstname,email,places_a,places_b,message,uuid,booking_date,guest)
VALUES (0,29,6,'diam','ligula','cras@example.com','1','3','','d38c91f1-4145-44f7-bcb9-9c2f9edd5a4f','2012-02-29 02:32:42','0');

INSERT INTO resa_booking (confirmed, id,performance_id,name,firstname,email,places_a,places_b,message,uuid,booking_date,guest)
VALUES (0,30,6,'ligula','diam','bar@example.com','4','3','','a38c91f1-4145-44f7-bcb9-9c2f9edd5a4f','2012-03-3 12:32:42','0');

INSERT INTO resa_booking (confirmed, id,performance_id,name,firstname,email,places_a,places_b,message,uuid,booking_date,guest)
VALUES (0,31,6,'ligula','placerat','bar@example.com','1','3','','a38c91f1-4145-44a7-bcb9-9c2f9edd5a4f', datetime('now'),'0');

INSERT INTO resa_booking (confirmed, id,performance_id,name,firstname,email,places_a,places_b,message,uuid,booking_date,guest)
VALUES (0,32,6,'ligula','semerat','bar@example.com','1','3','','a38d91f1-4145-44a7-bcb9-9c2f9edd5a4f',datetime('now'),'0');

INSERT INTO resa_booking (confirmed, id,performance_id,name,firstname,email,places_a,places_b,message,uuid,booking_date,guest)
VALUES (0,34,7,'tortor','ultrices','tortor@ultrices.fr','1','3','','a38d91f1-4145-44a7-bcb9-9c2f9edd5a4f',datetime('now'),'0');


--
--

COMMIT ;
