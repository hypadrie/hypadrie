BEGIN;
--
--
DELETE FROM resa_festival;
--
INSERT INTO "resa_festival" (id,title,url,published,date_begin,date_end,description)
VALUES (1,'Les fous du clavier','fous-du-clavier','0','2012-03-03','2012-03-09','Rencontres de geeks aguerris');

INSERT INTO "resa_festival" (id,title,url,published,date_begin,date_end,description)
VALUES (2,'Festi''Clown 2012','festiclown2012','1','2012-11-14','2012-11-11','Cirque burlesque');

INSERT INTO "resa_festival" (id,title,url,published,date_begin,date_end,description)
VALUES (3,'Festi''Clown 2011','festiclown2011','1','2011-11-12','2011-11-13','Cirque burlesque');

INSERT INTO "resa_festival" (id,title,url,published,date_begin,date_end,description)
VALUES (4,'Rencontres clownesque','rencontres-clownesques','1','2012-09-01','2012-09-01','Rencontre avec les clown');

--
--
DELETE FROM resa_information;

INSERT INTO "resa_information" ("id","title","information","published","url") 
VALUES (1, 'Festiclown bientôt','Le prochain festival de clowns est déjà programmé. Il se déroulera fin novembre 2012 à Saint-Avé','1','http://beta.hypadrie.eu/festiclown2012/');

INSERT INTO "resa_information" ("id","title","information","published","url") 
VALUES (2, 'Foobar','Lorem ipsum','0','http://blog.rodolphe.quiedeville.org');

INSERT INTO "resa_information" ("id","title","information","published","url") 
VALUES (3, 'Rencontrez les clown','Les clowns de la compagnie Clown & Co dédicaceront leur dernier ouvrage à la Libriaire Des Lives en Stocks à Molène le 15 août 2012','1','http://beta.hypadrie.eu/festiclown2012/');

--
--
DELETE FROM resa_show;

INSERT INTO "resa_show" ("id","title","company" ,"published","contact_name" ,"contact_tel" ,"contact_email","description","company_url" ,"show_url")
VALUES (1, 'Pilouface','Ile logique','1','Cedric Aubouy','0606060606','email@email.com','Super spectacle','http://ilelogique.fr','http://ilelogique.fr');

INSERT INTO "resa_show" ("id","title","company" ,"published","contact_name" ,"contact_tel" ,"contact_email","description","company_url" ,"show_url")
VALUES (2, 'Pi 3.14','Ile logique','1','Cedric Aubouy','0606060606','email@email.com','Super spectacle autour du nombre Pi','http://ilelogique.fr','http://ilelogique.fr');

INSERT INTO "resa_show" ("id","title","company" ,"published","contact_name" ,"contact_tel" ,"contact_email","description","company_url" ,"show_url")
VALUES (3, 'Dé-pensons','Ile logique','1','Cedric Aubouy','0606060606','email@email.com','Super spectacle autour du nombre Pi','http://ilelogique.fr','http://ilelogique.fr');


DELETE FROM resa_place;

INSERT INTO "resa_place" (id,title,address,town)
VALUES (1,'Théatre Anne de Bretagne','','Vannes');

INSERT INTO "resa_place" (id,title,address,town)
VALUES (2,'Dôme','','Saint-Avé');

INSERT INTO "resa_place" (id,title,address,town)
VALUES (3,'Fil','','Saint-Étienne');

COMMIT;