.. _running:

lighttpd with fastcgi
=====================

To run hypadrie under lighhtp with fcgi

The file lighttpd.conf in samples/ dir can be used to run lighhtpd
with fastcgi.

Launch fastcgi server

 ./manage.py runfcgi method=threaded host=127.0.0.1 port=3033

.. seealso::

   * `Django's documentation
   <https://docs.djangoproject.com/en/1.3/howto/deployment/fastcgi/>`_; Django's official online documentation.

