.. _faq:

Frequently-asked questions
==========================

The following are miscellaneous common questions and answers related
to installing/using hypadrie


General
-------

**What license is hypadrie under ?**
    hypadrie is licensed under GPLv3 license; this is `an OSI-approved
    open-source license
    <http://www.opensource.org/licenses/bsd-license.php>`_, and allows
    you a large degree of freedom in modifiying and redistributing the
    code. For the full terms, see the file ``LICENSE`` which came with
    your copy of hypadrie; if you did not receive a copy of
    this file, you can view it online at
    <https://gitorious.org/hypadrie/hypadrie/blobs/master/LICENSE>.

**Is the source code published somewhere ?**
    The source code is published at `Gitorious <https://gitorious.org/hypadrie/hypadrie/blobs/master/LICENSE>`_


Installation and setup
----------------------

**How do I install hypadrie ?**
    Full instructions are available in :ref:`the quick start guide <quickstart>`.

