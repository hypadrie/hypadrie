.. _quickstart:

Quick start guide
=================

Before installing hypadrie, you'll need to have a copy of
`Django <http://www.djangoproject.com>`_ already installed. For the
|version| release, Django 1.1 or newer is required.

For further information, consult the `Django download page
<http://www.djangoproject.com/download/>`_, which offers convenient
packaged downloads and installation instructions.



Manual installation from a Gitorious checkout
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you'd like to try out the latest in-development code, you can
obtain it from the hypadrie, which is hosted at
`Gitorious <http://gitorious.org/>`_ and uses `git 
<http://git-scm.com/>`_ for version control. To
obtain the latest code and documentation, you'll need to have
git installed, at which point you can type::
	  
    git clone https://git.gitorious.org/hypadrie/hypadrie.git


