.. _requirements:

Requirements
=================

Before installing hypadrie install the following packages


Debian packages
=================

All of these packages are available under Debian Stable, hypadrie was
created, used and tested under Debian Squeeze ::

 - python-reportlab 

 - python-django

 - python-docutils

 - python-django-registration

 - libapache2-mod-wsgi

