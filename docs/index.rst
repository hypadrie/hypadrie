.. hypadrie documentation master file


hypadrie 0.1 documentation
=====================================

This documentation covers the 0.1 release of hypadrie, a
simple online booking service.

Contents:

.. toctree::
   :maxdepth: 1
   
   quickstart
   requirements
   running
   translate
   faq

.. seealso::

   * `Django's documentation
     <http://docs.djangoproject.com/en/>`_; Django's official online documentation.
